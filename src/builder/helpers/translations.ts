/** Dependencies */
import { L10n } from "@tripetto/builder";
import { getTranslation } from "./l10n";

export class Translations {
    /** Contains if the translations are loaded. */
    private static m_bLoaded = false;

    /** Retrieves if the translations are loaded. */
    public static get isLoaded(): boolean {
        return this.m_bLoaded;
    }

    /** Loads the translations. */
    public static load(baseUrl: string, ajaxUrl: string, done: () => void, language?: string): void {
        const fnLoaded = () => {
            this.m_bLoaded = true;

            done();
        };

        if (language && language === "en") {
            fnLoaded();
        }

        getTranslation(baseUrl, ajaxUrl, language).then((translation) => {
            if (translation) {
                L10n.load(translation);
            }

            fnLoaded();
        });
    }
}
