export interface IHooks {
    email?: {
        enabled: boolean;
        recipient?: string;
        includeFields?: boolean;
        allowDownloads?: boolean;
    };

    slack?: {
        enabled: boolean;
        url?: string;
        includeFields?: boolean;
        allowDownloads?: boolean;
    };

    webhook?: {
        enabled: boolean;
        url?: string;
        nvp?: boolean;
        allowDownloads?: boolean;
    };

    make?: {
        enabled: boolean;
        url?: string;
        allowDownloads?: boolean;
    };

    zapier?: {
        enabled: boolean;
        url?: string;
        allowDownloads?: boolean;
    };

    pabbly?: {
        enabled: boolean;
        url?: string;
        allowDownloads?: boolean;
    };

    /**
     * @deprecated Use `make` instead.
     */
    integromat?: {
        enabled: boolean;
        url?: string;
        allowDownloads?: boolean;
    };
}
