/** Dependencies */
import { L10n } from "@tripetto/builder";
import { getLocale } from "./l10n";

export class Locale {
    /** Contains if the locale profile is loaded. */
    private static m_bLoaded = false;

    /** Retrieves if the locale profile is loaded. */
    public static get isLoaded(): boolean {
        return this.m_bLoaded;
    }

    /** Loads the locale profile. */
    public static load(baseUrl: string, ajaxUrl: string, done: () => void): void {
        getLocale(baseUrl, ajaxUrl).then((locale) => {
            if (locale) {
                L10n.locale.load(locale);
            }

            this.m_bLoaded = true;

            done();
        });
    }
}
