/** Global package constants */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

declare const WPTripetto: {
    readonly name: string;
    readonly version: string;
};

if (typeof WPTripetto === "undefined") {
    (
        window as {} as {
            [s: string]: {};
        }
    )["WPTripetto"] = {
        name: PACKAGE_NAME,
        version: PACKAGE_VERSION,
    };
}
