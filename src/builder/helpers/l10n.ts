import { L10n } from "@tripetto/runner";
import Promise from "promise-polyfill";
import * as Superagent from "superagent";

const localeCache: {
    [locale: string]: L10n.ILocale | undefined;
} = {};

const translationCache: {
    [language: string]: L10n.TTranslation | L10n.TTranslation[] | undefined;
} = {};

let useFallback = false;

export const getLocale = (baseUrl: string, ajaxUrl: string, query?: "auto" | string) =>
    new Promise<L10n.ILocale | undefined>((resolve: (locale: L10n.ILocale | undefined) => void) => {
        const locale = query && query !== "auto" ? query : "";
        const cache = localeCache[locale];

        if (!cache) {
            const fnFallback = () => {
                useFallback = true;

                Superagent.post(ajaxUrl)
                    .type("form")
                    .send({
                        action: "tripetto_locale",
                        locale,
                    })
                    .then((response: Superagent.Response) => resolve((localeCache[locale] = (response.ok && response.body) || undefined)))
                    .catch(() => resolve(undefined));
            };

            if (useFallback) {
                return fnFallback();
            }

            Superagent.get(`${baseUrl}/tripetto.php?t=locale` + (locale ? `&l=${encodeURIComponent(locale)}` : ""))
                .send()
                .then((response: Superagent.Response) => {
                    if (response.ok) {
                        resolve((localeCache[locale] = response.body || undefined));
                    } else {
                        fnFallback();
                    }
                })
                .catch(() => fnFallback());
        } else {
            resolve(cache);
        }
    });

export const getTranslation = (baseUrl: string, ajaxUrl: string, language?: string | "auto", context?: string) =>
    new Promise<L10n.TTranslation | L10n.TTranslation[] | undefined>(
        (resolve: (translation: L10n.TTranslation | L10n.TTranslation[] | undefined) => void) => {
            const key = `${language && language !== "auto" ? language : ""}:${context || "*"}`;
            const cache = translationCache[key];

            if (!cache) {
                const fnFallback = () => {
                    useFallback = true;

                    Superagent.post(ajaxUrl)
                        .type("form")
                        .send({
                            action: "tripetto_translation",
                            language: language && language !== "auto" ? language : "",
                            context,
                        })
                        .then((response) => resolve((translationCache[key] = (response.ok && response.body) || undefined)))
                        .catch(() => resolve(undefined));
                };

                if (useFallback) {
                    return fnFallback();
                }

                Superagent.get(
                    `${baseUrl}/tripetto.php?t=translation${language && language !== "auto" ? `&l=${encodeURIComponent(language)}` : ""}${
                        context ? `&c=${encodeURIComponent(context)}` : ""
                    }`
                )
                    .send()
                    .then((response: Superagent.Response) => {
                        if (response.ok) {
                            resolve((translationCache[key] = response.body || undefined));
                        } else {
                            fnFallback();
                        }
                    })
                    .catch(() => fnFallback());
            } else {
                resolve(cache);
            }
        }
    );
