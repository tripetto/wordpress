import { DEFAULT, DOM, extendImmutable, Components } from "@tripetto/builder";
import { IBuilderStyle } from "../components/builder/style";
import { IHeaderStyle } from "../components/header/style";
import { IDialogStyle } from "../components/dialog/style";
import { IPreviewStyle } from "../components/preview/style";

const HEADER: IHeaderStyle = {
    height: 56,
    style: {
        orientation: "horizontal",
        appearance: {
            backgroundColor: "rgb(255,255,255)",
            borderBottom: "1px solid rgb(197,217,232)",
        },
        scrollable: true,
        scrollbars: {
            track: {
                size: 2,
                appearance: {
                    transition: "opacity .3s ease-out, visibility .3s ease-out",
                },
            },
            thumb: {
                size: 2,
                min: 48,
                appearance: {
                    backgroundColor: "rgba(0,0,0,0.2)",
                },
            },
        },
    },
    separator: {
        width: 1,
        height: 56,
        appearance: {
            backgroundColor: "rgb(216,229,238)",
        },
    },
    divider: {
        width: 1,
        height: 24,
        appearance: {
            backgroundColor: "rgb(216,229,238)",
            marginLeft: 16,
            marginTop: 16,
        },
    },
    spacer: {
        width: 16,
    },
    button: {
        withLabel: {
            width: "auto",
            height: 56,
            appearance: {
                fontFamily: "Roboto Medium",
                fontSize: 16,
                color: "rgb(44,64,90)",
                textAlign: "left",
                padding: "18px 22px",
                transition: "color .1s ease-in-out",
                "> i": {
                    float: "left",
                    fontSize: 20,
                    marginRight: 16,
                    color: "rgb(0,147,238)",
                },
                [DOM.Stylesheet.selector("small")]: {
                    fontFamily: "Roboto Regular",
                    fontSize: 12,
                    color: "rgb(0,147,238)",
                    padding: "21px 10px",
                    "> i": {
                        fontSize: 16,
                        marginRight: 8,
                        color: "rgb(168,198,223)",
                        position: "relative",
                        top: -2,
                    },
                },
                [DOM.Stylesheet.selector("customize")]: {
                    "> i": {
                        color: "#2bcdfd",
                    },
                },
                [DOM.Stylesheet.selector("share")]: {
                    "> i": {
                        color: "#feca2f",
                    },
                },
                [DOM.Stylesheet.selector("automate")]: {
                    "> i": {
                        color: "#30ca89",
                    },
                },
                [DOM.Stylesheet.selector("results")]: {
                    "> i": {
                        color: "#fa3a69",
                    },
                },
            },
            hover: {
                color: "rgb(0,147,238)",
                [DOM.Stylesheet.selector("small")]: {
                    "> i": {
                        color: "rgb(0,147,238)",
                    },
                },
                [DOM.Stylesheet.selector("customize")]: {
                    color: "#2bcdfd",
                },
                [DOM.Stylesheet.selector("share")]: {
                    color: "#feca2f",
                },
                [DOM.Stylesheet.selector("automate")]: {
                    color: "#30ca89",
                },
                [DOM.Stylesheet.selector("results")]: {
                    color: "#fa3a69",
                },
            },
            tap: {
                color: "rgb(0,147,238)",
                transition: "none",
            },
            opened: {
                transition: "none",
                [DOM.Stylesheet.selector("customize")]: {
                    color: "#2bcdfd",
                },
            },
            menu: DEFAULT.menu,
        },
        withoutLabel: {
            width: 64,
            height: 56,
            appearance: {
                color: "rgb(168,198,223)",
                transition: "color .1s ease-in-out",
                "> i": {
                    float: "left",
                    fontSize: 20,
                    margin: "18px 23px",
                },

                [DOM.Stylesheet.selector("small")]: {
                    "> i": {
                        fontSize: 16,
                        margin: "20px 24px",
                    },
                },
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
                transition: "none",
            },
        },
    },
    toggle: {
        width: 40,
        height: 40,
        appearance: {
            color: "rgb(168,198,223)",
            marginTop: 8,
            border: "1px solid transparent",
            borderRadius: 8,
            transition: "color .1s ease-in-out, background-color .2s ease-in-out, border-color .2s ease-in-out",
            "> i": {
                float: "left",
                fontSize: 20,
                margin: "9px 9px",
            },
        },
        hover: {
            color: "rgb(0,147,238)",
        },
        tap: {
            color: "rgb(0,147,238)",
            transition: "none",
        },
        selected: {
            backgroundColor: "rgb(250,251,252)",
            borderColor: "rgb(197,217,232)",
            color: "rgb(0,147,238)",
        },
    },
    title: {
        width: "fill",
        height: 56,
        appearance: {
            textAlign: "left",
            padding: "18px 21px",
            "> i": {
                float: "left",
                fontSize: 20,
                marginRight: 14,
                color: "#855ffb",
            },
            "> span:first-of-type": {
                fontFamily: "Roboto Medium",
                fontSize: 16,
                color: "rgb(44,64,90)",
                textAlign: "left",
                borderRight: "1px solid rgb(216,229,238)",
                padding: "2px 18px 3px 0",
            },
            "> span:last-child": {
                fontFamily: "Roboto Regular",
                fontSize: 14,
                color: "rgb(141,171,196)",
                transition: "color .1s ease-in-out",
                textAlign: "left",
                paddingLeft: 15,
            },
            [DOM.Stylesheet.selector("compact")]: {
                "> span:first-of-type": {
                    display: "none",
                },
                "> span:last-child": {
                    paddingLeft: 4,
                },
            },
        },
        hover: {
            "> span:last-child": {
                color: "rgb(0,147,238)",
            },
        },
    },
    menu: {
        width: 64,
        height: 56,
        appearance: {
            color: "rgb(168,198,223)",
            transition: "color .1s ease-in-out",
            "> i": {
                float: "left",
                fontSize: 20,
                margin: "18px 23px",
            },

            [DOM.Stylesheet.selector("authenticated")]: {
                color: "rgb(78,206,61)",
            },
        },
        hover: {
            color: "rgb(0,147,238)",
        },
        tap: {
            color: "rgb(0,147,238)",
            transition: "none",
        },
        opened: {
            color: "rgb(0,147,238)",
            transition: "none",
        },
        menu: DEFAULT.menu,
    },
};

const DIALOG: IDialogStyle = {
    width: 480,
    appearance: {
        borderRadius: 8,
        backgroundColor: "rgb(255,255,255)",
    },
    overlay: {
        backgroundColor: "rgba(0,0,0,0.3)",
    },
    title: {
        fontFamily: "Roboto Regular",
        fontSize: 24,
        color: "rgb(44,64,90)",
        position: "absolute",
        left: 40,
        top: 40,
        width: 376,
        height: 40,
    },
    message: {
        fontFamily: "Roboto Regular",
        fontSize: 16,
        color: "rgb(141,171,196)",
        position: "absolute",
        left: 40,
        right: 40,
        top: 80,
        height: 72,
    },
    confirm: {
        height: 276,
        buttons: {
            container: {
                position: "absolute",
                left: 40,
                right: 40,
                bottom: 32,
                height: 48,
            },
            ok: {
                appearance: {
                    fontFamily: "Roboto Regular",
                    fontSize: 16,
                    color: "rgb(255,255,255)",
                    backgroundColor: "rgb(0,147,238)",
                    borderRadius: 4,
                    display: "inline-block",
                    padding: "16px 24px",
                },
                hover: {
                    backgroundColor: "rgb(2,133,215)",
                },
                tap: {
                    backgroundColor: "rgb(0,121,196)",
                },
            },
            okDanger: {
                appearance: {
                    fontFamily: "Roboto Regular",
                    fontSize: 16,
                    color: "rgb(255,255,255)",
                    backgroundColor: "rgb(255,21,31)",
                    borderRadius: 4,
                    display: "inline-block",
                    padding: "16px 24px",
                },
                hover: {
                    backgroundColor: "rgb(215,7,16)",
                },
                tap: {
                    backgroundColor: "rgb(194,4,12)",
                },
            },
            cancel: {
                appearance: {
                    fontFamily: "Roboto Regular",
                    fontSize: 16,
                    color: "rgb(44,64,90)",
                    display: "inline-block",
                    padding: "16px 24px",
                },
                hover: {
                    textDecoration: "underline",
                },
                tap: {
                    textDecoration: "underline",
                },
            },
        },
    },
    alert: {
        height: 184,
        close: {
            appearance: {
                width: 48,
                height: 48,
                position: "absolute",
                right: 0,
                top: 0,
                color: "rgb(168,198,223)",
                transition: "color .1s ease-in-out",
                "> i": {
                    float: "left",
                    fontSize: 20,
                    margin: 14,
                },
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
                transition: "none",
            },
        },
    },
};

const PREVIEW: IPreviewStyle = {
    appearance: {
        borderLeft: "1px solid rgb(197,217,232)",
        backgroundColor: "rgb(255,255,255)",
    },
    header: {
        height: 64,
        appearance: {
            position: "absolute",
            left: 0,
            right: 0,
            height: 64,
            top: 0,
            backgroundColor: "rgb(250,251,252)",
            borderBottom: "1px solid rgb(216,229,238)",
            padding: "12px 240px 0px 12px",
        },
        runner: {
            appearance: {
                display: "inline-block",
                position: "relative",
                height: 40,
                fontFamily: "Roboto Light",
                fontSize: 15,
                color: "rgb(0,147,238)",
                textAlign: "left",
                backgroundImage: `url("data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCI+PHBhdGggZD0iTTAgNmEuNS41IDAgMDEuODUzLS4zNTRsOC42NDYgOC42NDYgOC42NDYtOC42NDZhLjUuNSAwIDAxLjcwNy43MDdsLTkgOWEuNS41IDAgMDEtLjcwNyAwbC05LTlhLjQ5OC40OTggMCAwMS0uMTQ2LS4zNTR6IiBmaWxsPSIjMDA5M0VFIi8+PC9zdmc+")`,
                backgroundSize: "16px 16px",
                backgroundPosition: "right 16px center",
                backgroundColor: "rgb(255,255,255)",
                backgroundRepeat: "no-repeat",
                border: "1px solid rgb(197,217,232)",
                borderRadius: 4,
                padding: "10px 40px 0px 36px",
                transition: "background .1s ease-in-out, opacity .1s ease-in-out",
                /** Icon */
                "> span:first-child": {
                    position: "absolute",
                    fontSize: 20,
                    left: 8,
                    top: 8,
                },
                /** Label */
                "> span:last-child": {
                    whiteSpace: "nowrap",
                },
            },
            hover: {
                backgroundColor: "rgb(225,236,245)",
            },
            tap: {
                backgroundColor: "rgb(197,217,232)",
            },
            opened: {
                backgroundColor: "rgb(197,217,232)",
            },
        },
        styles: {
            appearance: {
                position: "absolute",
                top: 12,
                right: 64 + 54 + 54,
                width: 54,
                height: 40,
                fontSize: 18,
                color: "rgb(168,198,223)",
                padding: 11,
            },
            disabled: {
                pointerEvents: "none",
                opacity: 0.3,
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
            },
        },
        restart: {
            appearance: {
                position: "absolute",
                top: 12,
                right: 64 + 54,
                width: 54,
                height: 40,
                fontSize: 18,
                color: "rgb(168,198,223)",
                padding: 11,
            },
            disabled: {
                pointerEvents: "none",
                opacity: 0.3,
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
            },
        },
        help: {
            appearance: {
                position: "absolute",
                top: 12,
                right: 64,
                width: 54,
                height: 40,
                fontSize: 18,
                color: "rgb(168,198,223)",
                padding: 11,
                borderRight: "1px solid rgb(216,229,238)",
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
            },
        },
        close: {
            appearance: {
                position: "absolute",
                top: 12,
                right: 12,
                width: 40,
                height: 40,
                fontSize: 18,
                color: "rgb(168,198,223)",
                padding: 11,
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
            },
        },
    },
    buttons: {
        height: 48,
        appearance: {
            position: "absolute",
            left: 0,
            right: 0,
            height: 48,
            top: 64,
        },
        button: {
            appearance: {
                backgroundColor: "rgb(255,255,255)",
                borderBottom: "1px solid rgb(216,229,238)",
                position: "absolute",
                top: 0,
                height: 48,
                width: "50%",
                fontFamily: "Roboto Regular",
                fontSize: 13,
                color: "rgb(141,171,196)",
                textAlign: "center",
                paddingTop: 16,
                transition: "color .1s ease-in-out, border .2s ease-in-out",
                "&:first-child": {
                    left: 0,
                },
                "&:last-child": {
                    right: 0,
                    borderLeft: "1px solid rgb(216,229,238)",
                },
            },
            hover: {
                color: "rgb(0,147,238)",
            },
            tap: {
                color: "rgb(0,147,238)",
            },
            selected: {
                color: "rgb(0,147,238)",
                borderBottom: "2px solid rgb(0,147,238)",
            },
        },
    },
    runner: {
        appearance: {
            position: "absolute",
            left: 0,
            right: 0,
            top: 64 + 48,
            bottom: 0,
            opacity: 0,
            pointerEvents: "none",
            transition: "opacity .2s ease-in-out",

            "> iframe": {
                pointerEvents: "none",
            },
        },
        active: {
            pointerEvents: "auto",
            opacity: 1,

            "> iframe": {
                pointerEvents: "auto",
            },
        },
    },
};

const HELP: Components.IToolbarItemStyle = {
    height: 40,
    width: 40,
    appearance: {
        backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTkuNSAxN2MtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXYtM2MwLTAuMjc2IDAuMjI0LTAuNSAwLjUtMC41IDMuMDMzIDAgNS41LTIuNDY3IDUuNS01LjVzLTIuNDY3LTUuNS01LjUtNS41LTUuNSAyLjQ2Ny01LjUgNS41YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjVzLTAuNS0wLjIyNC0wLjUtMC41YzAtMy41ODQgMi45MTYtNi41IDYuNS02LjVzNi41IDIuOTE2IDYuNSA2LjVjMCAzLjQxNi0yLjY0OSA2LjIyNS02IDYuNDgxdjIuNTE5YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSJyZ2IoMTY4LDE5OCwyMjMpIj48L3BhdGg+CjxwYXRoIGQ9Ik05LjUgMjBjLTAuMjc2IDAtMC41LTAuMjI0LTAuNS0wLjV2LTFjMC0wLjI3NiAwLjIyNC0wLjUgMC41LTAuNXMwLjUgMC4yMjQgMC41IDAuNXYxYzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSJyZ2IoMTY4LDE5OCwyMjMpIj48L3BhdGg+Cjwvc3ZnPgo=")`,
        backgroundSize: "16px 16px",
        backgroundPosition: "center center",
        backgroundColor: "rgb(255,255,255)",
        border: `1px solid rgb(197,217,232)`,
        borderRadius: 4,
        marginLeft: 12,
        marginRight: 4,
    },
    hover: {
        backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTkuNSAxN2MtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXYtM2MwLTAuMjc2IDAuMjI0LTAuNSAwLjUtMC41IDMuMDMzIDAgNS41LTIuNDY3IDUuNS01LjVzLTIuNDY3LTUuNS01LjUtNS41LTUuNSAyLjQ2Ny01LjUgNS41YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjVzLTAuNS0wLjIyNC0wLjUtMC41YzAtMy41ODQgMi45MTYtNi41IDYuNS02LjVzNi41IDIuOTE2IDYuNSA2LjVjMCAzLjQxNi0yLjY0OSA2LjIyNS02IDYuNDgxdjIuNTE5YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSJyZ2IoMCwxNDcsMjM4KSI+PC9wYXRoPgo8cGF0aCBkPSJNOS41IDIwYy0wLjI3NiAwLTAuNS0wLjIyNC0wLjUtMC41di0xYzAtMC4yNzYgMC4yMjQtMC41IDAuNS0wLjVzMC41IDAuMjI0IDAuNSAwLjV2MWMwIDAuMjc2LTAuMjI0IDAuNS0wLjUgMC41eiIgZmlsbD0icmdiKDAsMTQ3LDIzOCkiPjwvcGF0aD4KPC9zdmc+Cg==")`,
        backgroundColor: "rgb(225,236,245)",
    },
    tap: {
        backgroundImage: `url("data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4KPCFET0NUWVBFIHN2ZyBQVUJMSUMgIi0vL1czQy8vRFREIFNWRyAxLjEvL0VOIiAiaHR0cDovL3d3dy53My5vcmcvR3JhcGhpY3MvU1ZHLzEuMS9EVEQvc3ZnMTEuZHRkIj4KPHN2ZyB2ZXJzaW9uPSIxLjEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgeG1sbnM6eGxpbms9Imh0dHA6Ly93d3cudzMub3JnLzE5OTkveGxpbmsiIHdpZHRoPSIyMCIgaGVpZ2h0PSIyMCIgdmlld0JveD0iMCAwIDIwIDIwIj4KPHBhdGggZD0iTTkuNSAxN2MtMC4yNzYgMC0wLjUtMC4yMjQtMC41LTAuNXYtM2MwLTAuMjc2IDAuMjI0LTAuNSAwLjUtMC41IDMuMDMzIDAgNS41LTIuNDY3IDUuNS01LjVzLTIuNDY3LTUuNS01LjUtNS41LTUuNSAyLjQ2Ny01LjUgNS41YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjVzLTAuNS0wLjIyNC0wLjUtMC41YzAtMy41ODQgMi45MTYtNi41IDYuNS02LjVzNi41IDIuOTE2IDYuNSA2LjVjMCAzLjQxNi0yLjY0OSA2LjIyNS02IDYuNDgxdjIuNTE5YzAgMC4yNzYtMC4yMjQgMC41LTAuNSAwLjV6IiBmaWxsPSJyZ2IoMCwxNDcsMjM4KSI+PC9wYXRoPgo8cGF0aCBkPSJNOS41IDIwYy0wLjI3NiAwLTAuNS0wLjIyNC0wLjUtMC41di0xYzAtMC4yNzYgMC4yMjQtMC41IDAuNS0wLjVzMC41IDAuMjI0IDAuNSAwLjV2MWMwIDAuMjc2LTAuMjI0IDAuNS0wLjUgMC41eiIgZmlsbD0icmdiKDAsMTQ3LDIzOCkiPjwvcGF0aD4KPC9zdmc+Cg==")`,
        backgroundColor: "rgb(197,217,232)",
    },
};

// We take the default skin of the builder and extend it to a studio skin.
export const SKIN = extendImmutable<Partial<IBuilderStyle>>(DEFAULT, {
    header: HEADER,
    preview: PREVIEW,
    dialog: DIALOG,
    help: HELP,
}) as IBuilderStyle;
