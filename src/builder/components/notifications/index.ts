import Promise from "promise-polyfill";
import superagent from "superagent";
import { Components, Debounce, Forms, Layers, _, REGEX_IS_URL, compare } from "@tripetto/builder";
import { BuilderComponent } from "../builder";
import { IHooks } from "../../helpers/hooks";
import { REGEX_IS_EMAIL } from "../../helpers/regex";
import { HELP_NOTIFICATION, HELP_SLACK } from "../../urls";

export class NotificationsComponent extends Components.Controller<{
    readonly id: number;
    readonly tier: "default" | "pro" | "legacy";
    readonly hooks: IHooks;
}> {
    private readonly builder: BuilderComponent;
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(builder: BuilderComponent, hooks: IHooks, id: number, tier: "default" | "pro" | "legacy"): NotificationsComponent {
        return builder.openPanel(
            (panel: Layers.Layer) => new NotificationsComponent(panel, builder, hooks, id, tier),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(layer: Layers.Layer, builder: BuilderComponent, hooks: IHooks, id: number, tier: "default" | "pro" | "legacy") {
        super(
            layer,
            {
                id,
                hooks,
                tier,
            },
            _("Notifications"),
            "compact",
            builder.style,
            "right",
            "on-when-validated",
            _("Close")
        );

        this.builder = builder;

        layer.hook("OnShow", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });
    }

    private testSlack(url: string, includeFields: boolean): Promise<boolean> {
        return new Promise((resolve: (succeeded: boolean) => void) => {
            superagent
                .post(this.builder.ajaxUrl)
                .type("form")
                .send({
                    action: "tripetto_test_slack",
                    url,
                    includeFields,
                    name: this.builder.name,
                    mockup: JSON.stringify(this.builder.mockup),
                })
                .then((res) => {
                    resolve(res.status === 200);
                })
                .catch(() => {
                    resolve(false);
                });
        });
    }

    onCards(cards: Components.Cards): void {
        const email = this.ref.hooks.email || (this.ref.hooks.email = { enabled: false, recipient: "" });
        const slack = this.ref.hooks.slack || (this.ref.hooks.slack = { enabled: false });

        let currentHooks = JSON.parse(JSON.stringify({ ...this.ref.hooks, email, slack }));
        const updateHooks = new Debounce(() => {
            const hooks = { ...this.ref.hooks, email, slack };

            if (!compare(hooks, currentHooks, true)) {
                currentHooks = JSON.parse(JSON.stringify(hooks));

                this.builder.post({
                    action: "tripetto_hooks",
                    id: this.ref.id,
                    hooks: JSON.stringify(hooks),
                });
            }
        }, 300);

        if (this.ref.tier !== "pro") {
            cards.add(
                new Forms.Form({
                    title: "🥇 " + _("Pro license required"),
                    controls: [
                        new Forms.Static(
                            "You need a pro license to use all the notification features. Please click the button below for more information."
                        ),
                        new Forms.Button("🛒 " + _("Buy a pro license now"), "normal")
                            .url(
                                "https://tripetto.com/wordpress/pricing/?utm_source=wp_plugin&utm_medium=tripetto_platforms&utm_campaign=pro_upgrade&utm_content=builder_notifications"
                            )
                            .width(250),
                    ],
                })
            );
        }

        const emailRecipient = new Forms.Email((email && email.recipient) || "")
            .autoValidate((recipient: Forms.Text) =>
                recipient.value === "" || recipient.isDisabled || !recipient.isObservable
                    ? "unknown"
                    : REGEX_IS_EMAIL.test(recipient.value)
                    ? "pass"
                    : "fail"
            )
            .on((recipient: Forms.Email) => {
                if (email && (!recipient.value || REGEX_IS_EMAIL.test(recipient.value))) {
                    email.recipient = recipient.value;

                    updateHooks.invoke();
                }
            });
        const emailIncludeFields = new Forms.Checkbox(_("Include response data in the message"), email.includeFields).on(
            (checkbox: Forms.Checkbox) => {
                email.includeFields = checkbox.isChecked;
                emailAllowDownloads.isDisabled = !checkbox.isChecked;
                updateHooks.invoke();
            }
        );
        const emailAllowDownloads = new Forms.Checkbox(
            _("Allow direct access to uploaded files without WP admin login"),
            email.allowDownloads
        )
            .description(
                _(
                    "When checked uploaded files can be directly downloaded from the email without the need of logging in to your WordPress admin environment. Enable this if you want to allow email recipients (that don't have WordPress admin access) to access uploaded files for this form."
                )
            )
            .disabled(!email.includeFields)
            .on((checkbox: Forms.Checkbox) => {
                email.allowDownloads = checkbox.isChecked;
                updateHooks.invoke();
            });
        const emailGroup = new Forms.Group([
            new Forms.Static(_("Send a notification to:")),
            emailRecipient,
            emailIncludeFields,
            emailAllowDownloads,
        ]).visible(email.enabled);
        const emailEnabled = new Forms.Checkbox(
            _("Send an email when someone completes your form ([learn more](%1))", HELP_NOTIFICATION),
            email.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                email.enabled = checkbox.isChecked;
                updateHooks.invoke();

                emailGroup.visible(checkbox.isChecked);
            });

        cards.add(
            new Forms.Form({
                title: "📧 " + _("Email notification"),
                controls: [emailEnabled, emailGroup],
            })
        ).isDisabled = this.ref.tier === "default";

        const slackUrl = new Forms.Text("singleline", slack.url)
            .inputMode("url")
            .placeholder("https://")
            .autoValidate((url) =>
                url.value === "" || url.isDisabled || !url.isObservable ? "unknown" : REGEX_IS_URL.test(url.value) ? "pass" : "fail"
            )
            .on((url) => {
                slack.url = url.value;
                updateHooks.invoke();

                slackTestButton.buttonType = "normal";
                slackTestButton.disabled(!slackUrl.value || !REGEX_IS_URL.test(slackUrl.value));
                slackTestButton.label("🩺 " + _("Test"));
            });
        const slackIncludeFields = new Forms.Checkbox(_("Include response data in the message"), slack.includeFields).on(
            (checkbox: Forms.Checkbox) => {
                slack.includeFields = checkbox.isChecked;
                slackAllowDownloads.isDisabled = !checkbox.isChecked;
                updateHooks.invoke();
            }
        );
        const slackAllowDownloads = new Forms.Checkbox(
            _("Allow direct access to uploaded files without WP admin login"),
            slack.allowDownloads
        )
            .description(
                _(
                    "When checked uploaded files can be directly downloaded from Slack without the need of logging in to your WordPress admin environment. Enable this if you want to allow Slack users (that don't have WordPress admin access) to access uploaded files for this form."
                )
            )
            .disabled(!slack.includeFields)
            .on((checkbox: Forms.Checkbox) => {
                slack.allowDownloads = checkbox.isChecked;
                updateHooks.invoke();
            });
        const slackTestButton = new Forms.Button("🩺 " + _("Test")).on((testButton) => {
            slackUrl.disable();
            testButton.disable();
            testButton.buttonType = "normal";
            testButton.label("⏳ " + _("Testing..."));

            this.testSlack(slackUrl.value, slackIncludeFields.isChecked).then((succeeded) => {
                slackUrl.enable();
                testButton.enable();
                testButton.buttonType = succeeded ? "accept" : "warning";
                testButton.label(succeeded ? "✔ " + _("All good!") : "❌ " + _("Something's wrong"));
            });
        });
        const slackGroup = new Forms.Group([
            new Forms.Static(
                _(
                    "Please create an incoming [Slack webhook](https://api.slack.com/incoming-webhooks) and enter the URL of the webhook here:"
                )
            ).markdown(),
            slackUrl,
            slackIncludeFields,
            slackAllowDownloads,
            slackTestButton,
        ]).visible(slack.enabled);

        const slackEnabled = new Forms.Checkbox(
            _("Send a Slack message when someone completes your form ([learn more](%1))", HELP_SLACK),
            slack.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                const checked = checkbox.isChecked;

                slack.enabled = checked;
                updateHooks.invoke();

                slackGroup.visible(checked);
            });

        cards.add(
            new Forms.Form({
                title: "📣 " + _("Slack notification"),
                controls: [slackEnabled, slackGroup],
            })
        ).isDisabled = this.ref.tier !== "pro";
    }
}
