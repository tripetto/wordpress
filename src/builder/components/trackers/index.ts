import { Components, Debounce, Forms, Layers, _, compare } from "@tripetto/builder";
import { BuilderComponent } from "../builder";
import { ITrackers, ITracker } from "./trackers";
import { TRACKER_CODE_TEMPLATE } from "./template";
import { HELP_TRACKING, HELP_TRACKING_CUSTOM, HELP_TRACKING_FB, HELP_TRACKING_GA, HELP_TRACKING_GTM } from "../../urls";

export class TrackersComponent extends Components.Controller<{
    readonly id: number;
    readonly tier: "default" | "pro" | "legacy";
    readonly trackers: ITrackers;
}> {
    private readonly builder: BuilderComponent;
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(builder: BuilderComponent, trackers: ITrackers, id: number, tier: "default" | "pro" | "legacy"): TrackersComponent {
        return builder.openPanel(
            (panel: Layers.Layer) => new TrackersComponent(panel, builder, trackers, id, tier),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(
        layer: Layers.Layer,
        builder: BuilderComponent,
        trackers: ITrackers,
        id: number,
        tier: "default" | "pro" | "legacy"
    ) {
        super(
            layer,
            {
                id,
                trackers,
                tier,
            },
            _("Tracking"),
            "compact",
            builder.style,
            "right",
            "on-when-validated",
            _("Close"),
            [new Components.ToolbarLink(builder.style.help, HELP_TRACKING)]
        );

        this.builder = builder;

        layer.hook("OnShow", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });
    }

    onCards(cards: Components.Cards): void {
        const ga =
            this.ref.trackers.ga ||
            (this.ref.trackers.ga = {
                enabled: false,
                id: "",
                useGlobal: false,
                trackStart: true,
                trackStage: false,
                trackUnstage: false,
                trackFocus: false,
                trackBlur: false,
                trackPause: false,
                trackComplete: true,
            });

        const fb =
            this.ref.trackers.fb ||
            (this.ref.trackers.fb = {
                enabled: false,
                id: "",
                useGlobal: false,
                trackStart: true,
                trackStage: false,
                trackUnstage: false,
                trackFocus: false,
                trackBlur: false,
                trackPause: false,
                trackComplete: true,
            });
        const custom =
            this.ref.trackers.custom ||
            (this.ref.trackers.custom = {
                enabled: false,
                code: "",
            });

        if (!custom.code) {
            custom.code = TRACKER_CODE_TEMPLATE;
        }

        let currentTrackers = JSON.parse(JSON.stringify({ ...this.ref.trackers, ga, fb, custom }));
        const updateTrackers = new Debounce(() => {
            const trackers = { ...this.ref.trackers, ga, fb, custom };

            if (!compare(trackers, currentTrackers, true)) {
                currentTrackers = JSON.parse(JSON.stringify(trackers));

                this.builder.post({
                    action: "tripetto_trackers",
                    id: this.ref.id,
                    trackers: JSON.stringify(trackers),
                });
            }
        }, 300);

        if (this.ref.tier !== "pro") {
            cards.add(
                new Forms.Form({
                    title: "🥇 " + _("Pro license required"),
                    controls: [
                        new Forms.Static("You need a pro license to use tracking. Please click the button below for more information."),
                        new Forms.Button("🛒 " + _("Buy a pro license now"), "normal")
                            .url(
                                "https://tripetto.com/wordpress/pricing/?utm_source=wp_plugin&utm_medium=tripetto_platforms&utm_campaign=pro_upgrade&utm_content=builder_tracking"
                            )
                            .width(250),
                    ],
                })
            );
        }

        const trackerService = (
            placeholder: string,
            description: string,
            global: string,
            tracker: ITracker,
            fnUpdate: () => void,
            events: {
                readonly start: string;
                readonly stage: string;
                readonly unstage: string;
                readonly focus: string;
                readonly blur: string;
                readonly pause: string;
                readonly complete: string;
            }
        ) => {
            const notification = new Forms.Notification(
                _(
                    "If you also share your form using the shareable link, make sure to supply an ID. If you only embed this form, you can omit the ID in case you are using GTM."
                ),
                "info"
            ).visible(tracker.useGlobal && !tracker.id);

            return [
                new Forms.Static(description).markdown(),
                new Forms.Text("singleline", tracker.id)
                    .placeholder(placeholder)
                    .maxLength(24)
                    .on((id) => {
                        tracker.id = id.value;

                        notification.visible(tracker.useGlobal && !tracker.id);

                        fnUpdate();
                    }),
                notification,
                new Forms.Checkbox(global, tracker.useGlobal || false)
                    .description(
                        _(
                            "Use the tracking code already available on the page hosting the Tripetto form. This prevents Tripetto placing another tracking code on the page."
                        )
                    )
                    .on((useGlobal) => {
                        tracker.useGlobal = useGlobal.isChecked;

                        notification.visible(tracker.useGlobal && !tracker.id);

                        fnUpdate();
                    }),
                new Forms.Static("**" + _("Events to track:") + "**").markdown(),
                new Forms.Checkbox(_("Track form starting"), tracker.trackStart)
                    .markdown()
                    .description(_("Tracks when a form is started (event name: %1).", events.start))
                    .on((trackStart) => {
                        tracker.trackStart = trackStart.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(_("Track form completion"), tracker.trackComplete)
                    .markdown()
                    .description(_("Tracks when a form is completed (event name: %1).", events.complete))
                    .on((trackComplete) => {
                        tracker.trackComplete = trackComplete.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(_("Track staged blocks"), tracker.trackStage)
                    .markdown()
                    .description(_("Tracks when a block becomes available (event name: %1).", events.stage))
                    .on((trackStage) => {
                        tracker.trackStage = trackStage.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(_("Track unstaged blocks"), tracker.trackUnstage)
                    .markdown()
                    .description(_("Tracks when a block becomes unavailable (event name: %1).", events.unstage))
                    .on((trackUnstage) => {
                        tracker.trackUnstage = trackUnstage.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(_("Track focus"), tracker.trackFocus)
                    .markdown()
                    .description(_("Tracks when an input element gains focus (event name: %1).", events.focus))
                    .on((trackFocus) => {
                        tracker.trackFocus = trackFocus.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(_("Track blur"), tracker.trackBlur)
                    .markdown()
                    .description(_("Tracks when an input element loses focus (event name: %1).", events.blur))
                    .on((trackBlur) => {
                        tracker.trackBlur = trackBlur.isChecked;

                        fnUpdate();
                    }),
                new Forms.Checkbox(_("Track form pausing"), tracker.trackPause)
                    .markdown()
                    .description(_("Tracks when a form is paused (event name: %1).", events.pause))
                    .on((trackPause) => {
                        tracker.trackPause = trackPause.isChecked;

                        fnUpdate();
                    }),
            ];
        };

        /**
         * Google Universal Analytics
         */
        const gaUpdate = () => {
            ga.enabled = gaEnabled.isChecked;

            updateTrackers.invoke();
        };
        const gaEnabled = new Forms.Checkbox(
            _(
                "Track form activity with Google Analytics ([learn more](%1)) or Google Tag Manager ([learn more](%2))",
                HELP_TRACKING_GA,
                HELP_TRACKING_GTM
            ),
            ga.enabled
        )
            .markdown()
            .on((checkbox) => {
                gaGroup.visible(checkbox.isChecked);
                gaUpdate();
            });
        const gaGroup = new Forms.Group(
            trackerService(
                "UA-XXXXXX-X, GTM-XXXXXXX " + _("or") + " G-XXXXXXXXXX",
                _(
                    "Configure your [Google Analytics](%1) or [Google Tag Manager](%2) setup and copy-paste your Measurement, Tracking or Container ID here:",
                    "https://analytics.google.com/",
                    "https://tagmanager.google.com/"
                ),
                _("Google Analytics or Google Tag Manager is already installed on my website"),
                ga,
                gaUpdate,
                {
                    start: "tripetto_start",
                    stage: "tripetto_stage",
                    unstage: "tripetto_unstage",
                    focus: "tripetto_focus",
                    blur: "tripetto_blur",
                    pause: "tripetto_pause",
                    complete: "tripetto_complete",
                }
            )
        ).visible(ga.enabled);

        cards.add(
            new Forms.Form({
                title: "📈 " + _("Google Analytics / Google Tag Manager"),
                controls: [gaEnabled, gaGroup],
            })
        ).isDisabled = this.ref.tier !== "pro";

        /**
         * Facebook Pixel
         */
        const fbUpdate = () => {
            fb.enabled = fbEnabled.isChecked;

            updateTrackers.invoke();
        };
        const fbEnabled = new Forms.Checkbox(_("Track form activity with Facebook Pixel ([learn more](%1))", HELP_TRACKING_FB), fb.enabled)
            .markdown()
            .on((checkbox) => {
                fbGroup.visible(checkbox.isChecked);
                fbUpdate();
            });
        const fbGroup = new Forms.Group(
            trackerService(
                "000000000000000",
                _(
                    "Configure your [Facebook Pixel](%1) setup and copy-paste your Pixel ID here:",
                    "https://www.facebook.com/business/learn/facebook-ads-pixel"
                ),
                _("Facebook Pixel is already installed on my website"),
                fb,
                fbUpdate,
                {
                    start: "TripettoStart",
                    stage: "TripettoStage",
                    unstage: "TripettoUnstage",
                    focus: "TripettoFocus",
                    blur: "TripettoBlur",
                    pause: "TripettoPause",
                    complete: "TripettoComplete",
                }
            )
        ).visible(fb.enabled);

        cards.add(
            new Forms.Form({
                title: "📈 " + _("Facebook Pixel"),
                controls: [fbEnabled, fbGroup],
            })
        ).isDisabled = this.ref.tier !== "pro";

        /**
         * Custom code
         */
        const customCodeUpdate = () => {
            custom.enabled = customCodeEnabled.isChecked;
            custom.code = customCode.value;

            updateTrackers.invoke();
        };
        const customCodeEnabled = new Forms.Checkbox(
            _("Track form activity with custom tracking code ([learn more](%1))", HELP_TRACKING_CUSTOM),
            custom.enabled
        )
            .markdown()
            .on((checkbox) => {
                customCodeGroup.visible(checkbox.isChecked);
                customCodeUpdate();
            });
        const customCode = new Forms.Text("multiline", this.ref.trackers.custom.code)
            .sanitize(false)
            .trim(false)
            .on(() => customCodeUpdate());
        const customCodeGroup = new Forms.Group([
            new Forms.Static(
                _("Implement your custom JavaScript code for event tracking and connect Tripetto to other tracking services.")
            ),
            new Forms.Notification(_("Custom code might break your form!"), "warning"),
            new Forms.Static(
                _("Make sure the provided code is valid and **always test your form** after submitting or changing the custom code!")
            ).markdown(),
            customCode,
        ]).visible(custom.enabled);

        cards.add(
            new Forms.Form({
                title: "👩‍💻 " + _("Custom tracking code"),
                controls: [customCodeEnabled, customCodeGroup],
            })
        ).isDisabled = this.ref.tier !== "pro";
    }
}
