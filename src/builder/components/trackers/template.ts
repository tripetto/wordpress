export const TRACKER_CODE_TEMPLATE = `function() {
  /* Place your tracker initialization code here. */

  /**
   * The function returned here is invoked by Tripetto
   * each time a trackable event occurs.
   */
  return function(event, form, block) {
    /* Place your event tracking code here. */

  }
}`;
