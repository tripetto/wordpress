export interface ITracker {
    enabled: boolean;
    id: string;
    useGlobal?: boolean;
    trackStart: boolean;
    trackStage: boolean;
    trackUnstage: boolean;
    trackFocus: boolean;
    trackBlur: boolean;
    trackPause: boolean;
    trackComplete: boolean;
}

export interface ITrackers {
    ga?: ITracker;
    fb?: ITracker;
    custom?: {
        enabled: boolean;
        code: string;
    };
}
