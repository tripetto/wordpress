import { Components, DOM, linearicon, _ } from "@tripetto/builder";
import { IHeaderStyle } from "./style";
import { DeviceSize } from "../../helpers/device";

export class Title extends Components.ToolbarStatic<DeviceSize> {
    private icon?: DOM.Element;
    private prefix?: DOM.Element;

    constructor(style: IHeaderStyle, title: string, fnTapped?: () => void) {
        super(style.title, title, undefined, fnTapped);
    }

    protected onViewChange(): boolean {
        if (this.element) {
            this.element.selectorSafe("compact", this.view === "xs");

            return true;
        }

        return false;
    }

    onDraw(toolbar: Components.Toolbar<DeviceSize>, element: DOM.Element): void {
        this.icon = element.create("i", (icon: DOM.Element) => linearicon(0xe62d, icon, true));
        this.prefix = element.create("span", (prefix: DOM.Element) => (prefix.text = _("Build")));

        super.onDraw(toolbar, element, element.create("span"));
    }

    change(title: string, subform: boolean): void {
        this.label = title || (subform ? _("Unnamed subform") : _("Unnamed"));

        if (this.icon) {
            linearicon(subform ? 0xe6d8 : 0xe62d, this.icon, true);
        }

        if (this.prefix) {
            this.prefix.text = subform ? _("Subform") : _("Build");
        }
    }
}
