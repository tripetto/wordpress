import { Components, Layers, _ } from "@tripetto/builder";
import { Title } from "./title";
import { Menu, learnMenu, supportMenu } from "./menu";
import { Toggle } from "./toggle";
import { Separator } from "./separator";
import { Spacer } from "./spacer";
import { Button, ButtonWithMenu } from "./button";
import { BuilderComponent } from "../builder";
import { DeviceSize, windowSize } from "../../helpers/device";
import { HELP_RUNNERS } from "../../urls";

export class HeaderComponent {
    private readonly parent: BuilderComponent;
    private readonly support: [Menu<DeviceSize>, Separator<DeviceSize>];
    private readonly builder: [
        ButtonWithMenu<DeviceSize>,
        Separator<DeviceSize>,
        Button<DeviceSize>,
        Separator<DeviceSize>,
        ButtonWithMenu<DeviceSize>,
        Separator<DeviceSize>,
        Button<DeviceSize>,
        Separator<DeviceSize>,
        Spacer<DeviceSize>,
        Toggle<DeviceSize>,
        Toggle<DeviceSize>,
        Toggle<DeviceSize>,
        Spacer<DeviceSize>,
        Separator<DeviceSize>
    ];
    private readonly preview: {
        desktop: Toggle<DeviceSize>;
        tablet: Toggle<DeviceSize>;
        phone: Toggle<DeviceSize>;
    };
    private readonly title: Title;
    private edit: (() => void) | undefined;
    private back: (() => void) | undefined;

    constructor(parent: BuilderComponent) {
        const style = parent.style.header;
        const badge = parent.tier !== "pro" ? "Pro" : "";

        this.title = new Title(style, parent.builder.name || _("Unnamed"), () => this.doEdit());

        this.parent = parent;
        this.preview = {
            desktop: new Toggle<DeviceSize>({
                style,
                icon: 0xe7af,
                onToggle: () => this.toggleDevice("desktop"),
            })
                .excludeInViews("xs", "s", "m")
                .selected(this.parent.device === "desktop"),
            tablet: new Toggle<DeviceSize>({
                style,
                icon: 0xe7ab,
                onToggle: () => this.toggleDevice("tablet"),
            })
                .excludeInViews("xs", "s")
                .selected(this.parent.device === "tablet"),
            phone: new Toggle<DeviceSize>({
                style,
                icon: 0xe7a5,
                onToggle: () => this.toggleDevice("phone"),
            }).selected(this.parent.device === "phone"),
        };

        this.builder = [
            new ButtonWithMenu<DeviceSize>({
                style,
                mode: "customize",
                icon: 0xe62b,
                label: _("Customize"),
                options: () => [
                    ...this.getRunnersMenu(),
                    new Components.MenuSeparator(),
                    new Components.MenuLabel(_("Form appearance")),
                    new Components.MenuItemWithIcon(0xe756, _("Properties"), () => this.doEdit()),
                    new Components.MenuItemWithIcon(0xe61f, _("Styles"), () => this.doStyles()).badge(badge),
                    new Components.MenuItemWithIcon(0xe886, _("Translations"), () => this.doL10n()),
                ],
            }).excludeInViews("xs", "s", "m"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
            new Button<DeviceSize>({
                style,
                mode: "share",
                icon: 0xe8c1,
                label: _("Share"),
                onTap: () => this.doShareOrEmbed(),
            })
                .excludeInViews("xs", "s", "m")
                .visible(!this.parent.sideBySide),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m").visible(!this.parent.sideBySide),
            new ButtonWithMenu<DeviceSize>({
                style,
                mode: "automate",
                icon: 0xe920,
                label: _("Automate"),
                options: () => [
                    new Components.MenuItemWithIcon(0xe8aa, _("Notifications"), () => this.doNotifications()).badge(badge),
                    new Components.MenuItemWithIcon(0xe920, _("Connections"), () => this.doConnections()).badge(badge),
                    new Components.MenuItemWithIcon(0xe77f, _("Tracking"), () => this.doTrackers()).badge(badge),
                ],
            }).excludeInViews("xs", "s", "m"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
            new Button<DeviceSize>({
                style,
                mode: "results",
                icon: 0xe6ac,
                label: _("Results"),
                onTap: () => this.doResults(),
            })
                .excludeInViews("xs", "s", "m")
                .visible(this.parent.results),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m").visible(this.parent.results),
            new Spacer(style),
            this.preview.desktop,
            this.preview.tablet,
            this.preview.phone,
            new Spacer(style),
            new Separator<DeviceSize>(style),
        ];

        this.support = [
            new Menu<DeviceSize>(parent, 0xe7ed, () => this.getSupport()).excludeInViews("xs", "s"),
            new Separator<DeviceSize>(style).excludeInViews("xs", "s"),
        ];

        const toolbarComponent = parent.layer.component(
            new Components.ToolbarComponent<DeviceSize>(
                {
                    style: style.style,
                    view: windowSize(),
                    left: [
                        new Menu<DeviceSize>(parent, 0xe92b, () => this.getMenu())
                            .includeInViews("xs", "s", "m")
                            .visible(this.parent.allowFullScreen),
                        new Separator<DeviceSize>(style).includeInViews("xs", "s", "m").visible(this.parent.allowFullScreen),
                        new Button<DeviceSize>({ style, icon: 0xe93b, onTap: () => this.goBack() }).excludeInViews("xs", "s", "m"),
                        new Separator<DeviceSize>(style).excludeInViews("xs", "s", "m"),
                        this.title,
                        new Button<DeviceSize>({ style, mode: "small", icon: 0xe672, onTap: () => this.doEdit() })
                            .excludeInViews("xs")
                            .visible(this.parent.allowFullScreen),
                    ],
                    right: [
                        new Separator(style),
                        ...(this.parent.allowFullScreen ? this.builder : []),
                        ...(this.parent.allowFullScreen ? this.support : []),
                        new Button<DeviceSize>({ style, icon: 0xe94c, onTap: () => this.doFullscreen() }).visible(
                            this.parent.allowFullScreen
                        ),
                        new Separator<DeviceSize>(style).visible(this.parent.sideBySide && this.parent.allowFullScreen),
                        new Button<DeviceSize>({ style, icon: 0xe935, onTap: () => this.goBack() }).visible(
                            this.parent.sideBySide && this.parent.allowFullScreen
                        ),
                    ],
                },
                Layers.Layer.configuration.height(style.height).alignTop()
            )
        );

        toolbarComponent.layer.hook("OnResize", "synchronous", () => {
            toolbarComponent.toolbar.view = windowSize();
        });
    }

    private getRunnersMenu(): Components.MenuOption[] {
        return [
            new Components.MenuLabel(_("Form face")),
            new Components.MenuItemWithIcon(
                this.parent.runner === "autoscroll" ? 0xe999 : 0xe98d,
                _("Autoscroll"),
                () => (this.parent.runner = "autoscroll")
            ),
            new Components.MenuItemWithIcon(
                this.parent.runner === "chat" ? 0xe999 : 0xe98d,
                _("Chat"),
                () => (this.parent.runner = "chat")
            ),
            new Components.MenuItemWithIcon(
                this.parent.runner === "classic" ? 0xe999 : 0xe98d,
                _("Classic"),
                () => (this.parent.runner = "classic")
            ),
            new Components.MenuSeparator(),
            new Components.MenuLinkWithIcon(0xe933, _("Help with form faces"), HELP_RUNNERS),
        ];
    }

    private getMenu(): Components.MenuOption[] {
        const options: Components.MenuOption[] = [];

        options.push(new Components.MenuLabel(_("Navigation")), new Components.MenuItemWithIcon(0xe93b, _("Back"), () => this.goBack()));

        options.push(
            new Components.MenuSeparator(),
            new Components.MenuLabel(_("Design")),
            new Components.MenuSubmenuWithIcon(0xe6f3, _("Form face"), this.getRunnersMenu()),
            new Components.MenuItemWithIcon(0xe672, _("Properties"), () => this.doEdit()),
            new Components.MenuItemWithIcon(0xe61f, _("Styles"), () => this.doStyles()),
            new Components.MenuItemWithIcon(0xe886, _("Translations"), () => this.doL10n()),
            new Components.MenuSeparator(),
            new Components.MenuLabel(_("Automate")),
            new Components.MenuItemWithIcon(0xe8aa, _("Notifications"), () => this.doNotifications()),
            new Components.MenuItemWithIcon(0xe920, _("Connections"), () => this.doConnections()),
            new Components.MenuItemWithIcon(0xe77f, _("Tracking"), () => this.doTrackers()),
            ...(!this.parent.sideBySide || this.parent.results
                ? [
                      new Components.MenuSeparator(),
                      new Components.MenuLabel(_("Run")),
                      ...(this.parent.sideBySide ? [] : [new Components.MenuItemWithIcon(0xe8c1, _("Share"), () => this.doShareOrEmbed())]),
                      ...(this.parent.results ? [new Components.MenuItemWithIcon(0xe6ac, _("Results"), () => this.doResults())] : []),
                  ]
                : [])
        );

        if (this.support[0].isExcluded) {
            options.push(
                new Components.MenuSeparator(),
                new Components.MenuLabel(_("More...")),
                new Components.MenuSubmenuWithIcon(0xe7ed, _("Support"), learnMenu(this.parent)),
                new Components.MenuSubmenuWithIcon(0xe699, _("Contact"), supportMenu())
            );
        }

        return options;
    }

    private getSupport(): Components.MenuOption[] {
        return [
            new Components.MenuSeparator(),
            new Components.MenuLabel(_("Learn")),
            ...learnMenu(this.parent),
            new Components.MenuSeparator(),
            new Components.MenuLabel(_("Support")),
            ...supportMenu(),
        ];
    }

    private goBack(): void {
        if (this.back) {
            this.back();
        } else {
            this.parent.close();
        }
    }

    private doEdit(): void {
        if (this.edit) {
            this.edit();
        }
    }

    private doStyles(): void {
        this.parent.stylesEditor();
    }

    private doL10n(): void {
        this.parent.l10nEditor();
    }

    private doTrackers(): void {
        this.parent.tracking();
    }

    private doShareOrEmbed(): void {
        const shareButton = this.builder[2];

        shareButton.disable();

        this.parent.shareOrEmbed().whenClosed = () => shareButton.enable();
    }

    private doNotifications(): void {
        this.parent.notifications();
    }

    private doConnections(): void {
        this.parent.connections();
    }

    private doResults(): void {
        window.parent.postMessage(
            {
                tripetto: true,
                target: this.parent.id,
                type: "results",
            },
            window.location.origin
        );
    }

    private doFullscreen(): void {
        if (this.parent.sideBySide) {
            this.parent.closePreview();
        }

        window.parent.postMessage(
            {
                tripetto: true,
                target: this.parent.id,
                type: "fullscreen",
            },
            window.location.origin
        );
    }

    private toggleDevice(device: "phone" | "tablet" | "desktop"): void {
        this.parent.toggleDevice(device);
    }

    update(): void {
        this.preview.desktop.isSelected = this.parent.device === "desktop";
        this.preview.tablet.isSelected = this.parent.device === "tablet";
        this.preview.phone.isSelected = this.parent.device === "phone";
    }

    breadcrumb(
        forms: {
            readonly name: string;
            readonly edit: () => void;
            readonly activate: () => void;
            readonly close: () => void;
        }[],
        back: (() => void) | undefined
    ): void {
        if (forms.length > 0) {
            const currentForm = forms[forms.length - 1];

            this.title.change(currentForm.name, forms.length > 1);
            this.edit =
                forms.length > 1
                    ? () => currentForm.edit()
                    : !this.parent.sideBySide || this.parent.allowFullScreen
                    ? () => this.parent.edit()
                    : undefined;
        }

        this.back = back;
    }
}
