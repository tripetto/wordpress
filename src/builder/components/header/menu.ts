import { Components, DOM, linearicon, _ } from "@tripetto/builder";
import { BuilderComponent } from "../builder";
import * as URLS from "../../urls";

export class Menu<T> extends Components.ToolbarMenu<T> {
    private icon: number;

    constructor(builder: BuilderComponent, icon: number, fnMenu: () => Components.MenuOption[]) {
        super(builder.style.header.menu, undefined, fnMenu);

        this.icon = icon;
    }

    onDraw(toolbar: Components.Toolbar<T>, element: DOM.Element): void {
        super.onDraw(toolbar, element);

        element.create("i", (icon: DOM.Element) => linearicon(this.icon, icon));
    }
}

export function learnMenu(builder: BuilderComponent): Components.MenuOption[] {
    return [
        new Components.MenuLinkWithIcon(0xe7ec, _("Help center"), URLS.HELP),
        new Components.MenuItemWithIcon(0xe6a5, _("Cheatsheet"), () => builder.openCheatsheet()),
    ];
}

export function supportMenu(): Components.MenuOption[] {
    return [new Components.MenuLinkWithIcon(0xe7ed, _("Ask support"), URLS.SUPPORT)];
}
