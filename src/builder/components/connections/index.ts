import Promise from "promise-polyfill";
import superagent from "superagent";
import { Components, Debounce, Forms, Layers, _, REGEX_IS_URL, compare, findFirst } from "@tripetto/builder";
import { BuilderComponent } from "../builder";
import { IHooks } from "../../helpers/hooks";
import {
    HELP_WEBHOOK,
    HELP_WEBHOOK_MAKE,
    HELP_WEBHOOK_PABBLY,
    HELP_WEBHOOK_ZAPIER,
    HELP_WEBHOOK_CUSTOM,
    HELP_WEBHOOK_CUSTOM_RAW,
} from "../../urls";

export class ConnectionsComponent extends Components.Controller<{
    readonly id: number;
    readonly tier: "default" | "pro" | "legacy";
    readonly hooks: IHooks;
}> {
    private readonly builder: BuilderComponent;
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(builder: BuilderComponent, hooks: IHooks, id: number, tier: "default" | "pro" | "legacy"): ConnectionsComponent {
        return builder.openPanel(
            (panel: Layers.Layer) => new ConnectionsComponent(panel, builder, hooks, id, tier),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(layer: Layers.Layer, builder: BuilderComponent, hooks: IHooks, id: number, tier: "default" | "pro" | "legacy") {
        super(
            layer,
            {
                id,
                hooks,
                tier,
            },
            _("Connections"),
            "compact",
            builder.style,
            "right",
            "on-when-validated",
            _("Close"),
            [new Components.ToolbarLink(builder.style.help, HELP_WEBHOOK)]
        );

        this.builder = builder;

        layer.hook("OnShow", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });
    }

    private testWebhook(url: string, nvp: boolean): Promise<boolean> {
        return new Promise((resolve: (succeeded: boolean) => void) => {
            superagent
                .post(this.builder.ajaxUrl)
                .type("form")
                .send({ action: "tripetto_test_webhook", url, nvp, mockup: JSON.stringify(this.builder.mockup) })
                .then((res) => {
                    resolve(res.status === 200);
                })
                .catch(() => {
                    resolve(false);
                });
        });
    }

    onCards(cards: Components.Cards): void {
        const webhook = this.ref.hooks.webhook || (this.ref.hooks.webhook = { enabled: false, nvp: true });
        const make = this.ref.hooks.make || this.ref.hooks.integromat || (this.ref.hooks.make = { enabled: false });
        const zapier = this.ref.hooks.zapier || (this.ref.hooks.zapier = { enabled: false });
        const pabbly = this.ref.hooks.pabbly || (this.ref.hooks.pabbly = { enabled: false });

        let currentHooks = JSON.parse(JSON.stringify({ ...this.ref.hooks, webhook, make, zapier, pabbly }));
        const updateHooks = new Debounce(() => {
            const hooks = { ...this.ref.hooks, webhook, make, zapier, pabbly };

            delete hooks.integromat;

            if (!compare(hooks, currentHooks, true)) {
                currentHooks = JSON.parse(JSON.stringify(hooks));

                this.builder.post({
                    action: "tripetto_hooks",
                    id: this.ref.id,
                    hooks: JSON.stringify(hooks),
                });
            }
        }, 300);

        if (this.ref.tier !== "pro") {
            cards.add(
                new Forms.Form({
                    title: "🥇 " + _("Pro license required"),
                    controls: [
                        new Forms.Static(
                            "You need a pro license to use all the connections features. Please click the button below for more information."
                        ),
                        new Forms.Button("🛒 " + _("Buy a pro license now"), "normal")
                            .url(
                                "https://tripetto.com/wordpress/pricing/?utm_source=wp_plugin&utm_medium=tripetto_platforms&utm_campaign=pro_upgrade&utm_content=builder_connections"
                            )
                            .width(250),
                    ],
                })
            );
        }

        const fnService = (
            title: string,
            domains: string[],
            help: string,
            description: string,
            service: {
                enabled: boolean;
                url?: string;
                allowDownloads?: boolean;
            },
            serviceName?: string
        ) => {
            const validateUrl = (url: string) =>
                findFirst(domains, (domain) => {
                    if (domain.indexOf("*") !== -1) {
                        const a = domain.substring(0, domain.indexOf("*"));
                        const b = domain.substring(domain.indexOf("*") + 1);

                        return url.indexOf(a) === 0 && url.indexOf(b) > a.length;
                    } else {
                        return url.indexOf(domain) === 0;
                    }
                })
                    ? true
                    : false;
            const serviceUrl = new Forms.Text("singleline", service.url)
                .inputMode("url")
                .placeholder("https://")
                .autoValidate((url) =>
                    url.value === "" || url.isDisabled || !url.isObservable
                        ? "unknown"
                        : REGEX_IS_URL.test(url.value) && validateUrl(url.value)
                        ? "pass"
                        : "fail"
                )
                .on((url) => {
                    service.url = url.value;
                    updateHooks.invoke();

                    serviceTestButton.buttonType = "normal";
                    serviceTestButton.disabled(!serviceUrl.value || !REGEX_IS_URL.test(serviceUrl.value) || !validateUrl(serviceUrl.value));
                    serviceTestButton.label("🩺 " + _("Test"));
                });
            const serviceAllowDownloads = new Forms.Checkbox(_("Allow access to uploaded files"), service.allowDownloads)
                .description(
                    _(
                        "This will allow %1 access to any uploaded files (only applies for forms that use the file upload block).",
                        serviceName || title
                    )
                )
                .on((checkbox: Forms.Checkbox) => {
                    service.allowDownloads = checkbox.isChecked;
                    updateHooks.invoke();
                });
            const serviceTestButton = new Forms.Button("🩺 " + _("Test")).disable().on((testButton) => {
                serviceUrl.disable();
                testButton.disable();
                testButton.buttonType = "normal";
                testButton.label("⏳ " + _("Testing..."));

                this.testWebhook(serviceUrl.value, true).then((succeeded) => {
                    serviceUrl.enable();
                    testButton.enable();
                    testButton.buttonType = succeeded ? "accept" : "warning";
                    testButton.label(succeeded ? "✔ " + _("All good!") : "❌ " + _("Something's wrong"));
                });
            });

            const serviceGroup = new Forms.Group([
                new Forms.Static(description).markdown(),
                serviceUrl,
                serviceAllowDownloads,
                serviceTestButton,
            ]).visible(service.enabled);

            const serviceEnabled = new Forms.Checkbox(
                _("Submit completed forms to %1 ([learn more](%2))", serviceName || title, help),
                service.enabled
            )
                .markdown()
                .on((checkbox: Forms.Checkbox) => {
                    const checked = checkbox.isChecked;

                    service.enabled = checked;
                    updateHooks.invoke();

                    serviceGroup.visible(checked);
                });

            cards.add(
                new Forms.Form({
                    title: "🌐 " + title,
                    controls: [serviceEnabled, serviceGroup],
                })
            ).isDisabled = this.ref.tier !== "pro";
        };

        fnService(
            _("Make"),
            ["https://hook.*.make.com/", "https://hook.integromat.com/"],
            HELP_WEBHOOK_MAKE,
            _(
                "Configure [Make](%1) and paste their webhook URL here:",
                "https://www.make.com/en/integrations/tripetto?utm_source=tripetto&utm_medium=partner&utm_campaign=tripetto-partner-program"
            ),
            make,
            "Make"
        );
        fnService(
            "Zapier",
            ["https://hooks.zapier.com/"],
            HELP_WEBHOOK_ZAPIER,
            _("Configure [Zapier](%1) and paste their webhook URL here:", "https://zapier.com/apps/webhook/integrations"),
            zapier
        );
        fnService(
            "Pabbly Connect",
            ["https://connect.pabbly.com/"],
            HELP_WEBHOOK_PABBLY,
            _("Configure [Pabbly Connect](%1) and paste their webhook URL here:", "https://www.pabbly.com/connect/integrations/Tripetto/"),
            pabbly
        );

        const webhookUrl = new Forms.Text("singleline", webhook.url)
            .inputMode("url")
            .placeholder("https://")
            .autoValidate((url) =>
                url.value === "" || url.isDisabled || !url.isObservable ? "unknown" : REGEX_IS_URL.test(url.value) ? "pass" : "fail"
            )
            .on((url) => {
                webhook.url = url.value;
                updateHooks.invoke();

                webhookTestButton.buttonType = "normal";
                webhookTestButton.disabled(!webhookUrl.value || !REGEX_IS_URL.test(webhookUrl.value));
                webhookTestButton.label("🩺 " + _("Test"));
            });
        const webhookAllowDownloads = new Forms.Checkbox(_("Allow webhook to access uploaded files"), webhook.allowDownloads)
            .description(
                _(
                    "This will give the webhook access to the uploaded files for this form. If left unchecked uploaded files are only downloadable from within your WordPress admin environment. So a webhook endpoint cannot access those files. Only activate this checkbox if your webhook needs access to the uploaded files."
                )
            )
            .on((checkbox: Forms.Checkbox) => {
                webhook.allowDownloads = checkbox.isChecked;
                updateHooks.invoke();
            });
        const webhookRaw = new Forms.Checkbox(
            _("Send raw response data to webhook ([learn more](%1))", HELP_WEBHOOK_CUSTOM_RAW),
            !webhook.nvp
        )
            .markdown()
            .description(
                _(
                    "Please only use this option if you're an expert on webhooks. When enabled this option sends way more data to your webhook and in such a format your automation tool cannot process this by default. By leaving this option disabled, you can just use your response data easily."
                )
            )
            .on((checkbox: Forms.Checkbox) => {
                webhook.nvp = !checkbox.isChecked;
                updateHooks.invoke();
            });
        const webhookTestButton = new Forms.Button("🩺 " + _("Test")).disable().on((testButton) => {
            webhookUrl.disable();
            testButton.disable();
            testButton.buttonType = "normal";
            testButton.label("⏳ " + _("Testing..."));

            this.testWebhook(webhookUrl.value, !webhookRaw.isChecked).then((succeeded) => {
                webhookUrl.enable();
                testButton.enable();
                testButton.buttonType = succeeded ? "accept" : "warning";
                testButton.label(succeeded ? "✔ " + _("All good!") : "❌ " + _("Something's wrong"));
            });
        });

        const webhookGroup = new Forms.Group([
            new Forms.Static(_("Post the data to the following URL:")),
            webhookUrl,
            webhookAllowDownloads,
            webhookRaw,
            webhookTestButton,
        ]).visible(webhook.enabled);

        const webhookEnabled = new Forms.Checkbox(
            _("Submit completed forms to a custom webhook ([learn more](%1))", HELP_WEBHOOK_CUSTOM),
            webhook.enabled
        )
            .markdown()
            .on((checkbox: Forms.Checkbox) => {
                const checked = checkbox.isChecked;

                webhook.enabled = checked;
                updateHooks.invoke();

                webhookGroup.visible(checked);
            });

        cards.add(
            new Forms.Form({
                title: "🔗 " + _("Custom webhook"),
                controls: [webhookEnabled, webhookGroup],
            })
        ).isDisabled = this.ref.tier !== "pro";
    }
}
