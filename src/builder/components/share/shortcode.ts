export interface IShortcode {
    readonly pausable?: boolean;
    readonly persistent?: boolean;
    readonly async?: boolean;
    readonly fullPage?: boolean;
    readonly width?: string;
    readonly height?: string;
    readonly placeholder?: string;
    readonly css?: string;
}
