import { Components, Debounce, Forms, Layers, Str, castToBoolean, set, _ } from "@tripetto/builder";
import { BuilderComponent } from "../builder";
import { IShortcode } from "./shortcode";
import { HELP_GDPR, HELP_SHARE, HELP_LINK, HELP_SHORTCODE } from "../../urls";

export class ShareComponent extends Components.Controller<{
    readonly id: number;
    readonly url: string;
    readonly shortcode: IShortcode;
}> {
    private readonly builder: BuilderComponent;
    whenReady?: () => void;
    whenClosed?: () => void;

    static open(builder: BuilderComponent, id: number, url: string, shortcode: IShortcode): ShareComponent {
        return builder.openPanel(
            (panel: Layers.Layer) => new ShareComponent(panel, builder, id, url, shortcode),
            Layers.Layer.configuration.width(640).animation(Layers.LayerAnimations.Zoom)
        );
    }

    private constructor(layer: Layers.Layer, builder: BuilderComponent, id: number, url: string, shortcode: IShortcode) {
        super(
            layer,
            {
                id,
                url,
                shortcode,
            },
            _("Share"),
            "compact",
            builder.style,
            "right",
            "always-on",
            _("Close"),
            [new Components.ToolbarLink(builder.style.help, HELP_SHARE)]
        );

        this.builder = builder;

        layer.hook("OnShow", "framed", () => {
            if (this.whenReady) {
                this.whenReady();
            }
        });

        layer.hook("OnClose", "framed", () => {
            if (this.whenClosed) {
                this.whenClosed();
            }
        });
    }

    private get shortcode(): IShortcode {
        return this.ref.shortcode;
    }

    private copyToClipboard(source: Forms.Text): Forms.Button {
        const label = _("Copy to clipboard");

        return new Forms.Button(label).width(200).onClick((button: Forms.Button) => {
            button.disable();
            button.type("accept");
            button.label("✔ " + _("Copied!"));

            source.copyToClipboard();

            setTimeout(() => {
                button.enable();
                button.type("normal");
                button.label(label);
            }, 1000);
        });
    }

    private generateShortcode(): string {
        let shortcode = `[tripetto id='${this.ref.id}'`;

        if (this.shortcode.pausable) {
            shortcode += " pausable='yes'";
        }

        if (this.shortcode.persistent) {
            shortcode += " persistent='yes'";
        }

        if (!castToBoolean(this.shortcode.async, true)) {
            shortcode += " async='no'";
        }

        if (this.shortcode.fullPage) {
            shortcode += " mode='page'";
        }

        if (this.shortcode.width) {
            shortcode += " width='" + Str.replace(this.shortcode.width, "'", "") + "'";
        }

        if (this.shortcode.height) {
            shortcode += " height='" + Str.replace(this.shortcode.height, "'", "") + "'";
        }

        if (this.shortcode.placeholder) {
            shortcode += " placeholder='" + encodeURIComponent(this.shortcode.placeholder).replace(/[']/g, escape) + "'";
        }

        if (this.shortcode.css) {
            shortcode += " css='" + encodeURIComponent(this.shortcode.css).replace(/[']/g, escape) + "'";
        }

        return shortcode + "]";
    }

    onCards(cards: Components.Cards): void {
        const updateSettings = new Debounce(() => {
            this.builder.post({
                action: "tripetto_shortcode",
                id: this.ref.id,
                shortcode: JSON.stringify(this.shortcode),
            });
        }, 300);

        const urlText = new Forms.Text("singleline", this.ref.url).readonly();
        const shortcodeText = new Forms.Text("singleline", this.generateShortcode()).fixedLines(2).sanitize(false).trim(false).readonly();

        const fnUpdate = (action: () => void) => {
            action();

            shortcodeText.value = this.generateShortcode();
            updateSettings.invoke();
        };

        const widthCtrl = new Forms.Text("singleline", this.shortcode.width || "")
            .width(100)
            .indent(32)
            .maxLength(50)
            .align("center")
            .placeholder(_("100%"))
            .on((width) => fnUpdate(() => set(this.shortcode, "width", (width.isObservable && width.value) || undefined)))
            .visible(this.shortcode.width ? true : false);

        const heightCtrl = new Forms.Text("singleline", this.shortcode.height || "")
            .width(100)
            .indent(32)
            .maxLength(50)
            .align("center")
            .placeholder(_("auto"))
            .on((height) => fnUpdate(() => set(this.shortcode, "height", (height.isObservable && height.value) || undefined)))
            .visible(this.shortcode.height ? true : false);

        const placeholderCtrl = new Forms.Text("multiline", this.shortcode.placeholder || "")
            .indent(32)
            .on((placeholder) =>
                fnUpdate(() => set(this.shortcode, "placeholder", (placeholder.isObservable && placeholder.value) || undefined))
            )
            .visible(this.shortcode.placeholder ? true : false);

        const cssCtrl = new Forms.Text("multiline", this.shortcode.css || "")
            .indent(32)
            .placeholder(_("To specify rules for a specific block, use this selector: %1", `[data-block="<block identifier>"] { ... }`))
            .on((css) => fnUpdate(() => set(this.shortcode, "css", (css.isObservable && css.value) || undefined)))
            .visible(this.shortcode.css ? true : false);

        cards.add(
            new Forms.Form({
                title: "🧭 " + _("Shareable link"),
                controls: [
                    new Forms.Static(
                        _("Your form is automatically available through a dedicated link ([learn more](%1)).", HELP_LINK)
                    ).markdown(),
                    new Forms.Button(_("Open in browser"), "accept").width(200).url(this.ref.url),
                    this.copyToClipboard(urlText),
                    urlText,
                ],
            })
        );

        cards.add(
            new Forms.Form({
                title: "👨‍💻 " + _("WordPress shortcode"),
                controls: [
                    new Forms.Static(
                        _("Use the shortcode below to embed the form on any page on your website ([learn more](%1)).", HELP_SHORTCODE)
                    ).markdown(),
                    this.copyToClipboard(shortcodeText),
                    shortcodeText,
                    new Forms.Checkbox(_("Allow pausing and resuming"), this.shortcode.pausable)
                        .description(
                            _(
                                "Allows users to pause the form and continue with it later by sending a resume link to the user's email address."
                            )
                        )
                        .on((checkbox) => fnUpdate(() => set(this.shortcode, "pausable", checkbox.isChecked))),
                    new Forms.Checkbox(_("Save and restore uncompleted forms"), this.shortcode.persistent)
                        .description(
                            _(
                                "Saves uncompleted forms in the local storage of the browser. Next time the user visits the form it is restored so the user can continue."
                            )
                        )
                        .on((checkbox) => fnUpdate(() => set(this.shortcode, "persistent", checkbox.isChecked))),
                    new Forms.Checkbox(_("Display form as full page overlay"), this.shortcode.fullPage).on((checkbox) =>
                        fnUpdate(() => set(this.shortcode, "fullPage", checkbox.isChecked))
                    ),
                    new Forms.Checkbox(_("Specify a fixed width for the form"), this.shortcode.width ? true : false).on((width) =>
                        widthCtrl.visible(width.isChecked)
                    ),
                    widthCtrl,
                    new Forms.Checkbox(_("Specify a fixed height for the form"), this.shortcode.height ? true : false).on((height) =>
                        heightCtrl.visible(height.isChecked)
                    ),
                    heightCtrl,
                    new Forms.Checkbox(_("Specify a loader placeholder message"), this.shortcode.placeholder ? true : false)
                        .description(_("This message is shown while the form is loading. You can specify text or HTML."))
                        .on((placeholder) => placeholderCtrl.visible(placeholder.isChecked)),
                    placeholderCtrl,
                    new Forms.Checkbox(_("Disable asynchronous loading"), this.shortcode.async === false)
                        .description(
                            _(
                                "Asynchronous loading helps avoiding caching issues, but sometimes results in longer form loading times because the page has already been rendered before the form is loaded. Enabling this option will load the form together with the page itself. Some cache plugins cache the page including the form. If this option is checked and your form doesn't update when you make a change, you probably need to clear the cache of your cache plugin."
                            )
                        )
                        .on((checkbox) => fnUpdate(() => set(this.shortcode, "async", !checkbox.isChecked))),
                    new Forms.Checkbox(_("Specify custom CSS styles"), this.shortcode.css ? true : false)
                        .description(
                            _(
                                "Don't use this, unless you know what you are doing. We don't give support on forms with custom CSS! If you have a problem with a form that uses custom CSS, then first disable the custom CSS and check if the problem persists."
                            )
                        )
                        .on((css) => cssCtrl.visible(css.isChecked)),
                    cssCtrl,
                ],
            })
        );

        cards.add(
            new Forms.Form({
                title: "✌ " + _("Our promise on data privacy"),
                controls: [
                    new Forms.Static(
                        _(
                            "Both the form and collected data are hosted and stored in _your own WordPress instance_. Not a single connection related to Tripetto is ever made with an external host other than yours. It's all in your own instance, under your control. Hello [GDPR](%1)!",
                            HELP_GDPR
                        )
                    ).markdown(),
                ],
            })
        );
    }
}
