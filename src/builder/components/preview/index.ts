import { DOM, IDefinition, Layers, Touch, linearicon, _ } from "@tripetto/builder";
import { TL10n, TStyles } from "@tripetto/runner";
import { BuilderComponent } from "../builder";
import { Runners, TRunners } from "../../helpers/runners";
import { HELP_PREVIEW, HELP_RUNNERS } from "../../urls";
import { Components } from "@tripetto/builder";
import { IBuilderStyle } from "../builder/style";

export type TPreviewDevice = "phone" | "tablet" | "desktop" | "off";

export class PreviewComponent extends Layers.LayerComponent {
    private readonly style: IBuilderStyle;
    private readonly builder: BuilderComponent;
    private runners: Runners;
    private runnerIcon?: DOM.Element;
    private runnerLabel?: DOM.Element;
    private stylesButton?: DOM.Element;
    private restartButton?: DOM.Element;
    private viewButtons?: [DOM.Element, DOM.Element];
    whenChanged?: () => void;

    static getWidth(device: TPreviewDevice): number {
        return device === "desktop" ? 1140 : device === "tablet" ? 640 : window.screen.width < 576 ? window.screen.width : 400;
    }

    constructor(builder: BuilderComponent, view: "preview" | "test") {
        super(
            Layers.Layer.configuration
                .width(PreviewComponent.getWidth(builder.device))
                .visible(builder.device !== "off")
                .alignRight()
                .animation(Layers.LayerAnimations.Zoom)
                .style({
                    layer: {
                        appearance: builder.style.preview.appearance,
                    },
                    applyToChildren: false,
                })
        );

        this.style = builder.style;
        this.builder = builder;
        this.runners = new Runners(builder, view);
    }

    private updateRunnerSwitcher(): void {
        if (this.runnerIcon && this.runnerLabel) {
            switch (this.builder.runner) {
                case "autoscroll":
                    linearicon(0xe711, this.runnerIcon, true);

                    this.runnerLabel.text = _("Autoscroll");
                    break;
                case "chat":
                    linearicon(0xe7d7, this.runnerIcon, true);

                    this.runnerLabel.text = _("Chat");
                    break;
                case "classic":
                    linearicon(0xe92f, this.runnerIcon, true);

                    this.runnerLabel.text = _("Classic");
                    break;
            }
        }
    }

    onRender(): void {
        this.layer.context.create(
            "div",
            (header: DOM.Element) => {
                header.create(
                    "div",
                    (runner: DOM.Element) => {
                        this.runnerIcon = runner.create("span");
                        this.runnerLabel = runner.create("span");

                        this.updateRunnerSwitcher();

                        Touch.Tap.single(
                            runner,
                            () =>
                                new Components.Menu(
                                    [
                                        new Components.MenuLabel(_("Form face")),
                                        new Components.MenuItemWithIcon(
                                            0xe711,
                                            _("Autoscroll"),
                                            () => (this.builder.runner = "autoscroll"),
                                            this.builder.runner === "autoscroll"
                                        ),
                                        new Components.MenuItemWithIcon(
                                            0xe7d7,
                                            _("Chat"),
                                            () => (this.builder.runner = "chat"),
                                            this.builder.runner === "chat"
                                        ),
                                        new Components.MenuItemWithIcon(
                                            0xe92f,
                                            _("Classic"),
                                            () => (this.builder.runner = "classic"),
                                            this.builder.runner === "classic"
                                        ),
                                        new Components.MenuSeparator(),
                                        new Components.MenuLinkWithIcon(0xe933, _("Help with form faces"), HELP_RUNNERS),
                                    ],
                                    {
                                        style: this.style.menu,
                                        onOpen: () => runner.addSelectorSafe("opened"),
                                        onClose: () => runner.removeSelectorSafe("opened"),
                                    }
                                ).openAtElement(runner, "bottom"),
                            () => runner.addSelectorSafe("tap"),
                            () => runner.removeSelectorSafe("tap")
                        );

                        Touch.Hover.pointer(runner, (pHover: Touch.IHoverEvent) => runner.selectorSafe("hover", pHover.isHovered));
                    },
                    [
                        this.style.preview.header.runner.appearance,
                        {
                            [DOM.Stylesheet.selector("hover")]: this.style.preview.header.runner.hover,
                            [DOM.Stylesheet.selector("tap")]: this.style.preview.header.runner.tap,
                            [DOM.Stylesheet.selector("opened")]: this.style.preview.header.runner.opened,
                        },
                    ]
                );

                this.stylesButton = header.create(
                    "div",
                    (styles: DOM.Element) => {
                        linearicon(0xe61f, styles);

                        Touch.Tap.single(
                            styles,
                            () => this.builder.stylesEditor(),
                            () => styles.addSelectorSafe("tap"),
                            () => styles.removeSelectorSafe("tap")
                        );

                        Touch.Hover.pointer(styles, (pHover: Touch.IHoverEvent) => styles.selectorSafe("hover", pHover.isHovered));
                    },
                    [
                        this.style.preview.header.styles.appearance,
                        {
                            [DOM.Stylesheet.selector("hover")]: this.style.preview.header.styles.hover,
                            [DOM.Stylesheet.selector("tap")]: this.style.preview.header.styles.tap,
                            [DOM.Stylesheet.selector("disabled")]: this.style.preview.header.styles.disabled,
                        },
                    ]
                );

                this.restartButton = header.create(
                    "div",
                    (restart: DOM.Element) => {
                        linearicon(0xe8da, restart);

                        restart.selector("disabled", this.runners.view === "preview");

                        Touch.Tap.single(
                            restart,
                            () => this.restart(),
                            () => restart.addSelectorSafe("tap"),
                            () => restart.removeSelectorSafe("tap")
                        );

                        Touch.Hover.pointer(restart, (pHover: Touch.IHoverEvent) => restart.selectorSafe("hover", pHover.isHovered));
                    },
                    [
                        this.style.preview.header.restart.appearance,
                        {
                            [DOM.Stylesheet.selector("hover")]: this.style.preview.header.restart.hover,
                            [DOM.Stylesheet.selector("tap")]: this.style.preview.header.restart.tap,
                            [DOM.Stylesheet.selector("disabled")]: this.style.preview.header.restart.disabled,
                        },
                    ]
                );

                header.create(
                    "div",
                    (help: DOM.Element) => {
                        linearicon(0xe933, help);

                        Touch.Tap.single(
                            help,
                            () => window.open(HELP_PREVIEW, "_blank"),
                            () => help.addSelectorSafe("tap"),
                            () => help.removeSelectorSafe("tap")
                        );

                        Touch.Hover.pointer(help, (pHover: Touch.IHoverEvent) => help.selectorSafe("hover", pHover.isHovered));
                    },
                    [
                        this.style.preview.header.help.appearance,
                        {
                            [DOM.Stylesheet.selector("hover")]: this.style.preview.header.help.hover,
                            [DOM.Stylesheet.selector("tap")]: this.style.preview.header.help.tap,
                        },
                    ]
                );

                header.create(
                    "div",
                    (close: DOM.Element) => {
                        linearicon(0xe92a, close);

                        Touch.Tap.single(
                            close,
                            () => this.builder.closePreview(),
                            () => close.addSelectorSafe("tap"),
                            () => close.removeSelectorSafe("tap")
                        );

                        Touch.Hover.pointer(close, (pHover: Touch.IHoverEvent) => close.selectorSafe("hover", pHover.isHovered));
                    },
                    [
                        this.style.preview.header.close.appearance,
                        {
                            [DOM.Stylesheet.selector("hover")]: this.style.preview.header.close.hover,
                            [DOM.Stylesheet.selector("tap")]: this.style.preview.header.close.tap,
                        },
                    ]
                );
            },
            this.style.preview.header.appearance
        );

        this.layer.context.create(
            "div",
            (buttons: DOM.Element) => {
                this.viewButtons = [
                    buttons.create(
                        "div",
                        (editView: DOM.Element) => {
                            editView.text = _("All without logic");
                            editView.selector("selected", this.runners.view === "preview");

                            Touch.Tap.single(
                                editView,
                                () => this.setView("preview"),
                                () => editView.addSelectorSafe("tap"),
                                () => editView.removeSelectorSafe("tap")
                            );

                            Touch.Hover.pointer(editView, (pHover: Touch.IHoverEvent) => editView.selectorSafe("hover", pHover.isHovered));
                        },
                        [
                            this.style.preview.buttons.button.appearance,
                            {
                                [DOM.Stylesheet.selector("hover")]: this.style.preview.buttons.button.hover,
                                [DOM.Stylesheet.selector("tap")]: this.style.preview.buttons.button.tap,
                                [DOM.Stylesheet.selector("selected")]: this.style.preview.buttons.button.selected,
                            },
                        ]
                    ),
                    buttons.create(
                        "div",
                        (testView: DOM.Element) => {
                            testView.text = _("Test with logic");
                            testView.selector("selected", this.runners.view === "test");

                            Touch.Tap.single(
                                testView,
                                () => this.setView("test"),
                                () => testView.addSelectorSafe("tap"),
                                () => testView.removeSelectorSafe("tap")
                            );

                            Touch.Hover.pointer(testView, (pHover: Touch.IHoverEvent) => testView.selectorSafe("hover", pHover.isHovered));
                        },
                        [
                            this.style.preview.buttons.button.appearance,
                            {
                                [DOM.Stylesheet.selector("hover")]: this.style.preview.buttons.button.hover,
                                [DOM.Stylesheet.selector("tap")]: this.style.preview.buttons.button.tap,
                                [DOM.Stylesheet.selector("selected")]: this.style.preview.buttons.button.selected,
                            },
                        ]
                    ),
                ];
            },
            this.style.preview.buttons.appearance
        );

        this.layer.wait();

        this.runners.render(
            () =>
                this.layer.context.create("div", undefined, [
                    this.style.preview.runner.appearance,
                    {
                        [DOM.Stylesheet.selector("active")]: this.style.preview.runner.active,
                    },
                ]),
            () => this.layer.done()
        );
    }

    changeRunner(runner: TRunners): void {
        this.runners.changeRunner(runner);

        this.updateRunnerSwitcher();
    }

    closeOnRunnerChange<
        T extends {
            readonly close: () => void;
            whenClosed?: () => void;
        }
    >(ref: T): T {
        if (ref instanceof Components.StylesEditor && this.stylesButton) {
            this.stylesButton.addSelectorSafe("disabled");
        }

        this.runners.onChangeRunner = () => ref.close();

        ref.whenClosed = () => {
            this.runners.onChangeRunner = undefined;

            if (this.builder.sideBySide && !this.builder.allowFullScreen) {
                this.builder.close();
            }

            if (ref instanceof Components.StylesEditor && this.stylesButton) {
                this.stylesButton.removeSelectorSafe("disabled");
            }
        };

        return ref;
    }

    setDefinition(definition: IDefinition): void {
        this.runners.setDefinition(definition);
    }

    setStyles(styles: TStyles): void {
        this.runners.setStyles(styles);
    }

    setL10n(l10n: TL10n) {
        this.runners.setL10n(l10n);
    }

    setDevice(device: TPreviewDevice): void {
        this.runners.setState(device !== "off");

        this.layer.configuration.width(PreviewComponent.getWidth(device));
        this.layer.isVisible = device !== "off";
    }

    setView(view: "preview" | "test"): void {
        this.runners.setView(view);

        if (this.viewButtons) {
            this.viewButtons[0].selectorSafe("selected", view === "preview");
            this.viewButtons[1].selectorSafe("selected", view === "test");
        }

        if (this.restartButton) {
            this.restartButton.selectorSafe("disabled", view === "preview");
        }
    }

    restart(): void {
        this.runners.restart();
    }

    @Layers.component("OnDestroy")
    destroy(): void {
        this.runners.destroy();
    }
}
