import { DOM } from "@tripetto/builder";

export interface IDialogStyle {
    width: number;
    appearance: DOM.IStyles;
    overlay: DOM.IStyles;
    title: DOM.IStyles;
    message: DOM.IStyles;
    confirm: {
        height: number;
        buttons: {
            container: DOM.IStyles;
            ok: {
                appearance: DOM.IStyles;
                hover: DOM.IStyles;
                tap: DOM.IStyles;
            };
            okDanger: {
                appearance: DOM.IStyles;
                hover: DOM.IStyles;
                tap: DOM.IStyles;
            };
            cancel: {
                appearance: DOM.IStyles;
                hover: DOM.IStyles;
                tap: DOM.IStyles;
            };
        };
    };
    alert: {
        height: number;
        close: {
            appearance: DOM.IStyles;
            hover: DOM.IStyles;
            tap: DOM.IStyles;
        };
    };
}
