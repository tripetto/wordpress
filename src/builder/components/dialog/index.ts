import { DOM, Layers, Touch, _, castToBoolean, linearicon } from "@tripetto/builder";
import { IDialogProperties } from "./properties";
import { BuilderComponent } from "../builder";

export class DialogComponent extends Layers.LayerComponent {
    static builder: BuilderComponent;
    private readonly props: IDialogProperties;
    private confirmed = false;

    static confirm(
        title: string,
        message: string,
        okText: string,
        cancelText: string,
        danger: boolean,
        confirm: () => void,
        cancel?: () => void
    ): DialogComponent {
        return DialogComponent.builder.layer.component(
            new DialogComponent({
                style: DialogComponent.builder.style.dialog,
                title,
                message,
                type: "confirm",
                okText,
                cancelText,
                danger,
                confirm,
                cancel,
            }),
            "popup"
        );
    }

    static alert(title: string, message: string): DialogComponent {
        return DialogComponent.builder.layer.component(
            new DialogComponent({ style: DialogComponent.builder.style.dialog, title, message, type: "alert" }),
            "popup"
        );
    }

    private constructor(props: IDialogProperties) {
        super(
            Layers.Layer.configuration
                .width(props.style.width)
                .height(props.type === "confirm" ? props.style.confirm.height : props.style.alert.height)
                .alignCenterHorizontal()
                .alignCenterVertical()
                .overlay()
                .capture()
                .global()
                .style({
                    layer: {
                        appearance: props.style.appearance,
                    },
                    overlay: {
                        appearance: props.style.overlay,
                    },
                })
                .animation(Layers.LayerAnimations.Zoom)
        );

        this.props = props;
    }

    private confirm(): void {
        this.confirmed = true;

        if (this.props.confirm) {
            this.props.confirm();
        }

        this.layer.close();
    }

    private cancel(): void {
        this.layer.close();
    }

    private createConfirmButtons(): void {
        this.layer.context.create(
            "div",
            (buttonContainer: DOM.Element) => {
                const okButtonStyle = this.props.danger ? this.props.style.confirm.buttons.okDanger : this.props.style.confirm.buttons.ok;

                buttonContainer.create(
                    "div",
                    (okButton: DOM.Element) => {
                        okButton.text = this.props.okText!;

                        Touch.Tap.single(
                            okButton,
                            () => this.confirm(),
                            () => okButton.addSelectorSafe("tap"),
                            () => okButton.removeSelectorSafe("tap")
                        );

                        Touch.Hover.pointer(okButton, (pHover: Touch.IHoverEvent) => okButton.selectorSafe("hover", pHover.isHovered));
                    },
                    [
                        okButtonStyle.appearance,
                        {
                            [DOM.Stylesheet.selector("hover")]: okButtonStyle.hover,
                            [DOM.Stylesheet.selector("tap")]: okButtonStyle.tap,
                        },
                    ]
                );

                buttonContainer.create(
                    "div",
                    (cancelButton: DOM.Element) => {
                        cancelButton.text = this.props.cancelText!;

                        Touch.Tap.single(
                            cancelButton,
                            () => this.cancel(),
                            () => cancelButton.addSelectorSafe("tap"),
                            () => cancelButton.removeSelectorSafe("tap")
                        );

                        Touch.Hover.pointer(cancelButton, (pHover: Touch.IHoverEvent) =>
                            cancelButton.selectorSafe("hover", pHover.isHovered)
                        );
                    },
                    [
                        this.props.style.confirm.buttons.cancel.appearance,
                        {
                            [DOM.Stylesheet.selector("hover")]: this.props.style.confirm.buttons.cancel.hover,
                            [DOM.Stylesheet.selector("tap")]: this.props.style.confirm.buttons.cancel.tap,
                        },
                    ]
                );
            },
            this.props.style.confirm.buttons.container
        );
    }

    onRender(): void {
        this.layer.context.create(
            "div",
            (title: DOM.Element) => {
                title.text = this.props.title;
            },
            this.props.style.title
        );

        this.layer.context.create(
            "div",
            (message: DOM.Element) => {
                message.text = this.props.message;
            },
            this.props.style.message
        );

        if (this.props.type === "confirm") {
            this.createConfirmButtons();

            if (this.props.cancel) {
                this.layer.onClose = () => {
                    if (!this.confirmed) {
                        this.props.cancel!();
                    }
                };
            }
        }

        if (this.props.type === "alert") {
            this.layer.context.create(
                "div",
                (cancelButton: DOM.Element) => {
                    cancelButton.create("i", (icon: DOM.Element) => linearicon(0xe935, icon));

                    Touch.Tap.single(
                        cancelButton,
                        () => this.cancel(),
                        () => cancelButton.addSelectorSafe("tap"),
                        () => cancelButton.removeSelectorSafe("tap")
                    );

                    Touch.Hover.pointer(cancelButton, (pHover: Touch.IHoverEvent) => cancelButton.selectorSafe("hover", pHover.isHovered));
                },
                [
                    this.props.style.alert.close.appearance,
                    {
                        [DOM.Stylesheet.selector("hover")]: this.props.style.alert.close.hover,
                        [DOM.Stylesheet.selector("tap")]: this.props.style.alert.close.tap,
                    },
                ]
            );
        }

        Touch.Keyboard.global(
            this.layer.context,
            (event: Touch.IKeyboardEvent) => {
                if (event.key === "Escape") {
                    this.cancel();
                }
            },
            "keydown"
        );
    }
}
