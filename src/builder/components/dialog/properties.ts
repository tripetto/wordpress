import { IDialogStyle } from "./style";

export interface IDialogProperties {
    readonly style: IDialogStyle;
    readonly title: string;
    readonly message: string;
    readonly type: "alert" | "confirm";
    readonly danger?: boolean;
    readonly okText?: string;
    readonly cancelText?: string;
    readonly confirm?: () => void;
    readonly cancel?: () => void;
}
