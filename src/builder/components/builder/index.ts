import { DialogComponent } from "../dialog";
import {
    Builder,
    Components,
    Debounce,
    Forms,
    IDefinition,
    Layers,
    _,
    assert,
    each,
    set,
    setAny,
    scheduleTimeout,
} from "@tripetto/builder";
import { TL10n, TStyles, calculateFingerprintAndStencil, Runner, Export } from "@tripetto/runner";
import { IBuilderProps } from "../../props";
import { IBuilderStyle } from "./style";
import { Runners, TRunners } from "../../helpers/runners";
import { getTranslation } from "../../helpers/l10n";
import { windowSize } from "../../helpers/device";
import { HeaderComponent } from "../header";
import { PreviewComponent, TPreviewDevice } from "../preview";
import { ShareComponent } from "../share";
import { NotificationsComponent } from "../notifications";
import { ConnectionsComponent } from "../connections";
import { TrackersComponent } from "../trackers";
import { Loader } from "../loader";
import { IHooks } from "../../helpers/hooks";
import { ITrackers } from "../trackers/trackers";
import { IShortcode } from "../share/shortcode";
import {
    HELP_L10N,
    HELP_STYLES,
    HELP_PROLOGUE,
    HELP_EPILOGUE,
    HELP_BRANCHES,
    HELP_CULLING,
    HELP_TERMINATORS,
    HELP_CONDITIONS,
    HELP_BLOCK_ERROR,
    HELP_BLOCK_HIDDEN_FIELD,
    HELP_BLOCK_CALCULATOR,
    HELP_BLOCK_MAILER,
    HELP_BLOCK_MAILER_SENDER,
} from "../../urls";
import * as Superagent from "superagent";

declare const PACKAGE_LICENSE: string;

export class BuilderComponent extends Layers.LayerComponent {
    private header!: HeaderComponent;
    private currentRunner: TRunners;
    private readonly updateDefinition: Debounce;
    private readonly updateStyles: Debounce;
    private readonly updateL10n: Debounce;
    private readonly queue: {}[] = [];
    private posting = false;
    private closing = false;
    private retry = 0;
    private token: string;
    readonly baseUrl: string;
    readonly ajaxUrl: string;
    readonly shareUrl: string;
    readonly style: IBuilderStyle;
    builder!: Builder;
    builderLayer: Layers.Layer | undefined;
    preview!: PreviewComponent;
    readonly language: string;
    readonly id: number;
    readonly styles: TStyles;
    readonly l10n: TL10n;
    readonly tier: "default" | "pro" | "legacy";
    readonly definition: IDefinition | undefined;
    readonly hooks: IHooks;
    readonly trackers: ITrackers;
    readonly shortcode: IShortcode;
    readonly results: boolean;
    readonly sideBySide: boolean;
    readonly allowFullScreen: boolean;
    readonly open?: "share" | "styles" | "l10n" | "notifications" | "connections" | "tracking";
    readonly data?: string;
    device: TPreviewDevice;
    whenReady?: () => void;
    whenClosed?: () => void;

    constructor(style: IBuilderStyle, props: IBuilderProps) {
        super(Layers.Layer.configuration.applicationRole());

        const size = windowSize();

        this.style = style;
        this.ajaxUrl = props.ajaxUrl;
        this.baseUrl = props.baseUrl;
        this.shareUrl = props.shareUrl;
        this.language = props.language;
        this.id = props.id;
        this.token = props.token;
        this.definition = props.definition;
        this.currentRunner = this.validateRunner(props.runner);
        this.styles = props.styles || {};
        this.l10n = props.l10n || {};
        this.hooks = props.hooks || {};
        this.trackers = props.trackers || {};
        this.shortcode = props.shortcode || {};
        this.tier = props.tier || "default";
        this.open = props.open;
        this.data = props.data;
        this.results = (!props.sideBySide && props.results) || false;
        this.sideBySide = props.sideBySide || false;
        this.allowFullScreen = !this.sideBySide || !props.open;
        this.device = size === "xs" || size === "s" ? "off" : size === "xl" ? "tablet" : "phone";

        this.updateDefinition = new Debounce((id: number, name: string, definition: IDefinition) => {
            const hashes = calculateFingerprintAndStencil(definition);

            this.post({
                action: "tripetto_definition",
                id,
                name,
                definition: JSON.stringify(definition),
                fingerprint: hashes.fingerprint,
                stencil: hashes.stencil("exportables"),
                actionables: hashes.stencil("actionables"),
            });

            if (this.sideBySide) {
                window.parent.postMessage(
                    {
                        tripetto: true,
                        target: this.id,
                        type: "definition",
                        definition,
                    },
                    window.location.origin
                );
            }
        }, 300);

        this.updateStyles = new Debounce((id: number, styles: TStyles) => {
            this.post({
                action: "tripetto_styles",
                id,
                styles: JSON.stringify(styles),
            });

            if (this.sideBySide) {
                window.parent.postMessage(
                    {
                        tripetto: true,
                        target: this.id,
                        type: "styles",
                        styles,
                    },
                    window.location.origin
                );
            }
        }, 300);

        this.updateL10n = new Debounce((id: string, l10n: TL10n) => {
            this.post({
                action: "tripetto_l10n",
                id,
                l10n: JSON.stringify(l10n),
            });

            if (this.sideBySide) {
                window.parent.postMessage(
                    {
                        tripetto: true,
                        target: this.id,
                        type: "l10n",
                        l10n,
                    },
                    window.location.origin
                );
            }
        }, 300);

        Loader.builder = this;
        DialogComponent.builder = this;

        window.addEventListener(
            "beforeunload",
            (event) => {
                if (!this.closing) {
                    this.closing = true;

                    window.parent.postMessage(
                        {
                            tripetto: true,
                            target: this.id,
                            type: "closing",
                        },
                        window.location.origin
                    );
                }

                if (this.posting || this.queue.length > 0) {
                    event.preventDefault();

                    return (event.returnValue = _("Not all data is saved! Please wait a moment before navigating away from this page."));
                }
            },
            {
                capture: true,
            }
        );

        window.addEventListener("unload", () => {
            this.updateDefinition.flush();
            this.updateStyles.flush();
            this.updateL10n.flush();
        });

        window.addEventListener("message", (msg: MessageEvent) => {
            if (msg.data && msg.data.tripetto && msg.data.type === "terminate") {
                this.close();
            }
        });
    }

    get runner(): TRunners {
        return this.currentRunner;
    }

    set runner(runner: TRunners) {
        Loader.show();

        this.builder
            .useNamespace(runner, Runners.builderBundle(this, runner), "url")
            .then(() => {
                this.preview.changeRunner((this.currentRunner = runner));

                this.post({
                    action: "tripetto_runner",
                    id: this.id,
                    runner,
                });

                if (this.sideBySide) {
                    window.parent.postMessage(
                        {
                            tripetto: true,
                            target: this.id,
                            type: "runner",
                            runner,
                        },
                        window.location.origin
                    );
                }

                Loader.hide();
            })
            .catch(() => Loader.hide());
    }

    get mockup() {
        let exportables: Export.IExportables | undefined;

        if (this.builder.definition) {
            const runner = new Runner({
                definition: this.builder.definition,
                mode: "paginated",
                start: true,
            });

            if (runner.instance) {
                exportables = Export.exportables(runner.instance);
            }
        }

        return {
            id: "0000000000",
            index: 1,
            created: new Date().toISOString(),
            fingerprint: exportables?.fingerprint || undefined,
            fields: exportables?.fields || [],
        };
    }

    get name() {
        return this.builder.definition?.name || "";
    }

    private handlePost(retry?: boolean) {
        if ((!this.retry || retry) && !this.posting && this.queue.length > 0) {
            const data = this.queue.splice(0, 1)[0];

            this.posting = true;

            Superagent.post(this.ajaxUrl)
                .type("form")
                .send({
                    ...data,
                    token: this.token,
                })
                .then((res) => {
                    if (!res.ok) {
                        this.showError();
                    } else {
                        this.token = res.body?.token || "";
                    }

                    this.posting = false;
                    this.retry = 0;

                    this.handlePost();
                })
                .catch((err) => {
                    if (err.status === 409) {
                        this.handleConflict(() => {
                            this.queue.splice(0, 0, {
                                ...data,
                                overwrite: true,
                            });

                            this.posting = false;
                            this.retry = 0;

                            this.handlePost();
                        });
                    } else if (err.status === 410) {
                        this.formDeleted();
                    } else {
                        if (this.retry < 5) {
                            this.queue.splice(0, 0, data);

                            this.posting = false;
                            this.retry++;

                            scheduleTimeout(() => this.handlePost(true), 1000 * this.retry);
                        } else {
                            this.showError(
                                () => {
                                    this.queue.splice(0, 0, data);

                                    this.posting = false;
                                    this.retry = 0;

                                    this.handlePost();
                                },
                                () => {
                                    this.posting = false;
                                    this.retry = 0;
                                }
                            );
                        }
                    }
                });
        } else if (this.closing) {
            this.close();
        }
    }

    private showError(retry?: () => void, cancel?: () => void): void {
        const title = "🤯 " + _("Something's cracking");
        const message = _(
            "Your actions aren't being processed properly and probably not saved to the server. We apologize for the inconvenience. Please try again and drop a line if troubles persist."
        );

        if (retry) {
            DialogComponent.confirm(title, message, _("Retry"), _("Cancel"), false, retry, cancel);
        } else {
            DialogComponent.alert(title, message);
        }
    }

    private handleConflict(overwrite: () => void): void {
        DialogComponent.confirm(
            "💥 " + _("Form is outdated!"),
            _(
                "Your changes cannot be saved because your version of the form is outdated (so the version saved on the server is newer). The form is changed by someone else or by you in another browser window/tab. What do you want to do with the form?"
            ),
            _("Reload from server"),
            _("Overwrite"),
            false,
            () => {
                window.parent.postMessage(
                    {
                        tripetto: true,
                        target: this.id,
                        type: "refresh",
                    },
                    window.location.origin
                );
            },
            () => overwrite()
        );
    }

    private formDeleted(): void {
        DialogComponent.alert(
            "💣 " + _("Form is gone!"),
            _(
                "Your actions aren't being processed properly because the form seems to be deleted. You cannot make changes to this form anymore."
            )
        );
    }

    private validateRunner(runner: string): TRunners {
        switch (runner) {
            case "chat":
                return "chat";
            case "classic":
            case "standard-bootstrap":
                return "classic";
            default:
                return "autoscroll";
        }
    }

    onRender(): void {
        this.openPanel((layer: Layers.Layer) => {
            this.builderLayer = layer;
            this.builder = Builder.open(this.definition, {
                license: PACKAGE_LICENSE,
                layer,
                layerConfiguration: Layers.Layer.configuration.right(this.device === "off" ? 0 : PreviewComponent.getWidth(this.device)),
                style: this.style,
                controls: "right",
                fonts: this.baseUrl + "/fonts/",
                zoom: "fit-horizontal",
                disableLogo: true,
                disableSaveButton: true,
                disableEditButton: true,
                disableCloseButton: true,
                disableTutorialButton: true,
                disableOpenCloseAnimation: true,
                disableNesting: this.tier !== "pro",
                disableBookmarks: true,
                supportURL: false,
                helpTopics: {
                    prologue: HELP_PROLOGUE,
                    epilogue: HELP_EPILOGUE,
                    branches: HELP_BRANCHES,
                    culling: HELP_CULLING,
                    terminators: HELP_TERMINATORS,
                    conditions: HELP_CONDITIONS,
                    "block:error": HELP_BLOCK_ERROR,
                    "block:hidden-field": HELP_BLOCK_HIDDEN_FIELD,
                    "block:calculator": HELP_BLOCK_CALCULATOR,
                    "block:mailer": HELP_BLOCK_MAILER,
                    "block:mailer:sender": HELP_BLOCK_MAILER_SENDER,
                },
                namespace: {
                    identifier: this.currentRunner,
                    url: Runners.builderBundle(this, this.currentRunner),
                    onLoad: (namespace) => {
                        try {
                            if (this.data) {
                                const variables: {
                                    name: string;
                                    description: string;
                                }[] = [];
                                const values: {
                                    [variable: string]: string;
                                } = {};

                                each(
                                    JSON.parse(atob(this.data)),
                                    (
                                        variable: {
                                            readonly description: string;
                                            readonly value: string;
                                        },
                                        name
                                    ) => {
                                        variables.push({
                                            name,
                                            description: variable.description,
                                        });

                                        values[name] = variable.value;
                                    },
                                    { keys: true }
                                );

                                setAny(namespace.nodeBlocks.ofType("@tripetto/block-hidden-field"), "customVariables", {
                                    name: _("WordPress"),
                                    variables,
                                });

                                setAny(window, "TRIPETTO_CUSTOM_VARIABLES", values);
                            }
                        } catch {}
                    },
                },
                tier:
                    (this.tier !== "pro" && {
                        name: "Pro",
                        blocks: [
                            "@tripetto/block-calculator",
                            "@tripetto/block-error",
                            "@tripetto/block-hidden-field",
                            "@tripetto/block-mailer",
                            "@tripetto/block-setter",
                            "@tripetto/block-stop",
                            "@tripetto/block-variable",
                            "@tripetto/block-signature",
                        ],
                        mode: this.tier === "legacy" ? "enabled" : "disabled",
                        message: (ref) => {
                            ref.form({
                                title: "🥇 " + _("Pro license required"),
                                controls: [
                                    new Forms.Static(
                                        _(
                                            "You need a pro license to unlock the full potential of this block. Please click the button below for more information."
                                        )
                                    ),
                                    new Forms.Button("🛒 " + _("Buy a pro license now"), "normal")
                                        .url(
                                            "https://tripetto.com/wordpress/pricing/?utm_source=wp_plugin&utm_medium=tripetto_platforms&utm_campaign=pro_upgrade&utm_content=builder_blocks"
                                        )
                                        .width(250),
                                ],
                            });
                        },
                    }) ||
                    undefined,
                onChange: (changedDefinition: IDefinition) => this.updateDefinition.invoke(this.id, this.builder.name, changedDefinition),
                onPreview: (previewDefinition: IDefinition) => {
                    this.preview.setDefinition(previewDefinition);
                },
                onBreadcrumb: (forms, back) => this.header.breadcrumb(forms, back),
                onClose: () => {
                    if (this.whenClosed) {
                        this.whenClosed();
                    }
                },
            });

            this.header = new HeaderComponent(this);
            this.preview = layer.component(new PreviewComponent(this, "preview"));

            layer.hook("OnShow", "framed", () => {
                if (this.whenReady) {
                    this.whenReady();
                }

                let autoCloser:
                    | {
                          whenReady?: () => void;
                          whenClosed?: () => void;
                      }
                    | undefined;

                const onReady = () => {
                    window.parent.postMessage(
                        {
                            tripetto: true,
                            target: this.id,
                            type: "ready",
                        },
                        window.location.origin
                    );
                };

                switch (this.open) {
                    case "share":
                        autoCloser = this.shareOrEmbed();
                        break;
                    case "styles":
                        this.stylesEditor(onReady);
                        break;
                    case "l10n":
                        this.l10nEditor(onReady);
                        break;
                    case "notifications":
                        autoCloser = this.notifications();
                        break;
                    case "connections":
                        autoCloser = this.connections();
                        break;
                    case "tracking":
                        autoCloser = this.tracking();
                        break;
                }

                if (autoCloser) {
                    autoCloser.whenReady = onReady;

                    if (this.sideBySide) {
                        autoCloser.whenClosed = () => this.close();
                    }
                }
            });

            this.builder.hook("OnRename", "framed", () => {
                parent.document.title = (this.builder.name || _("Unnamed")) + " - Tripetto";
            });
        });
    }

    openPanel<T>(render: (layer: Layers.Layer) => T, config?: Layers.LayerConfiguration): T {
        let ref!: T;

        (
            this.builderLayer ||
            assert(
                this.layer.createChain(
                    Layers.Layer.configuration
                        .top(this.style.header.height)
                        .layout("habc")
                        .style({
                            layer: {
                                appearance: {
                                    backgroundColor: this.style.background,
                                },
                            },
                            applyToChildren: false,
                        })
                )
            )
        ).createPanel((panel: Layers.Layer) => {
            ref = render(panel);
        }, config);

        return ref;
    }

    post(data: {}): void {
        this.queue.push(data);

        this.handlePost();
    }

    edit(): void {
        this.builder.edit("properties");
    }

    close(): void {
        if (!this.closing) {
            if (this.sideBySide) {
                this.updateDefinition.flush();
                this.updateStyles.flush();
                this.updateL10n.flush();

                this.builder.close();
            }

            this.closing = true;
        }

        window.parent.postMessage(
            {
                tripetto: true,
                target: this.id,
                type: this.posting || this.queue.length > 0 ? "closing" : "close",
            },
            window.location.origin
        );
    }

    stylesEditor(fnReady?: () => void): Components.StylesEditor | undefined {
        const contract = Runners.stylesContract(this.runner);

        return (
            contract &&
            this.preview.closeOnRunnerChange(
                this.openPanel((panel: Layers.Layer) => {
                    if (fnReady) {
                        panel.hook("OnShow", "framed", fnReady);
                    }

                    return new Components.StylesEditor<void>(
                        panel,
                        contract,
                        this.styles,
                        this.tier === "pro" ? "licensed" : "unlicensed",
                        (styles) => {
                            set(this, "styles", styles);

                            this.preview.setStyles(styles);
                            this.updateStyles.invoke(this.id, styles);
                        },
                        false,
                        true,
                        (done: (bReset: boolean) => void) => {
                            DialogComponent.confirm(
                                _("Reset styles"),
                                _("Are you sure you want to reset the styles for this form?"),
                                _("Yes, reset it!"),
                                _("No"),
                                true,
                                () => done(true),
                                () => done(false)
                            );
                        },
                        undefined,
                        this.style,
                        "right",
                        [new Components.ToolbarLink(this.style.help, HELP_STYLES)]
                    );
                }, Layers.Layer.configuration.width(400).animation(Layers.LayerAnimations.Zoom))
            )
        );
    }

    l10nEditor(fnReady?: () => void): Components.L10nEditor | undefined {
        const contract = Runners.l10nContract(this.runner);

        return (
            contract &&
            this.preview.closeOnRunnerChange(
                this.openPanel((panel: Layers.Layer) => {
                    if (fnReady) {
                        panel.hook("OnShow", "framed", fnReady);
                    }

                    return new Components.L10nEditor<void>(
                        panel,
                        contract,
                        this.builder,
                        this.l10n,
                        (l10n, current) => {
                            set(this, "l10n", l10n);

                            this.preview.setL10n(current);
                            this.updateL10n.invoke(this.id, l10n);
                        },
                        (language: string) => getTranslation(this.baseUrl, this.ajaxUrl, language, `@tripetto/runner-${this.runner}`),
                        undefined,
                        this.style,
                        "right",
                        [new Components.ToolbarLink(this.style.help, HELP_L10N)]
                    );
                }, Layers.Layer.configuration.width(600).animation(Layers.LayerAnimations.Zoom))
            )
        );
    }

    shareOrEmbed(): ShareComponent {
        return ShareComponent.open(this, this.id, this.shareUrl, this.shortcode);
    }

    notifications(): NotificationsComponent {
        return NotificationsComponent.open(this, this.hooks, this.id, this.tier);
    }

    connections(): ConnectionsComponent {
        return ConnectionsComponent.open(this, this.hooks, this.id, this.tier);
    }

    tracking(): TrackersComponent {
        return TrackersComponent.open(this, this.trackers, this.id, this.tier);
    }

    toggleDevice(device: "phone" | "tablet" | "desktop"): boolean {
        this.device = (this.device !== device && device) || "off";

        if (this.builder.layer) {
            this.builder.layer.configuration.right(this.device === "off" ? 0 : PreviewComponent.getWidth(this.device));
        }

        if (this.preview) {
            this.preview.setDevice(this.device);
        }

        this.header.update();

        return this.device === device;
    }

    closePreview(): void {
        if (this.device !== "off") {
            this.toggleDevice(this.device);
        }
    }

    openCheatsheet(): void {
        Components.Tutorial.open(this.layer, this.style.tutorial);
    }
}
