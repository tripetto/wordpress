import { Components, IBuilderStyle as IBuilderBaseStyle } from "@tripetto/builder";
import { IPreviewStyle } from "../preview/style";
import { IHeaderStyle } from "../header/style";
import { IDialogStyle } from "../dialog/style";

export interface IBuilderStyle extends IBuilderBaseStyle {
    header: IHeaderStyle;
    preview: IPreviewStyle;
    dialog: IDialogStyle;
    help: Components.IToolbarItemStyle;
}
