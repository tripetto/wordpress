import { IDefinition } from "@tripetto/builder";
import { TL10n, TStyles } from "@tripetto/runner";
import { IHooks } from "./helpers/hooks";
import { ITrackers } from "./components/trackers/trackers";
import { IShortcode } from "./components/share/shortcode";

export interface IBuilderProps {
    readonly id: number;
    readonly token: string;
    readonly baseUrl: string;
    readonly ajaxUrl: string;
    readonly shareUrl: string;
    readonly language: string;
    readonly runner: "autoscroll";
    readonly definition?: IDefinition;
    readonly styles?: TStyles;
    readonly l10n?: TL10n;
    readonly hooks?: IHooks;
    readonly trackers?: ITrackers;
    readonly shortcode?: IShortcode;
    readonly tier?: "pro" | "legacy";
    readonly open?: "share" | "styles" | "l10n" | "notifications" | "connections";
    readonly results?: boolean;
    readonly data?: string;
    readonly sideBySide?: boolean;
}
