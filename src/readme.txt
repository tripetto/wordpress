=== {{ TITLE }} ===
Contributors: tripetto
Tags: {{ KEYWORDS }}
Requires at least: 4.9
Tested up to: 6.7
Requires PHP: 5.6
Stable tag: {{ VERSION }}
License: GPLv2 or later

{{ DESCRIPTION }}

== Description ==

**Tired of boring and ugly forms in your WordPress site?** 😴
**Use the Tripetto form builder to make your forms conversational!** 🚀
**Your visitors will love filling out Tripetto forms!** 😍

### ⚡ Tripetto form builder plugin in a nutshell ###

- Build any form you need in our unique form builder: contact form, registration form, customer satisfaction form, reservation form, quiz form, order form, exam form, quote calculation form (<a href="https://tripetto.com/examples/" rel="friend" title="See live form examples" target="_blank">see live form examples</a>);
- Full-featured form builder plugin that helps you to create conversational forms;
- The form plugin runs a stand-alone, full-blown form builder. All storage, including all collected form data, is stored right inside your WordPress without any dependencies on outside infrastructure;
- Visually build your smart forms on the magnetic storyboard (<a href="https://tripetto.com/magnetic-form-builder/" rel="friend" title="See the magnetic form builder" target="_blank">learn more</a>);
- Build your form from scratch, or use one of our form templates to build your form even faster;
- Advanced form calculator block included for creating quiz forms, order forms, exam forms and more (<a href="https://tripetto.com/calculator/" rel="friend" title="No-code calculations with the calculator block" target="_blank">learn more</a>);
- Built-in SPAM-protection without the need for using CAPTCHAs in your forms (<a href="https://tripetto.com/help/articles/how-tripetto-prevents-spam-entries-from-your-forms/" rel="friend" title="How we fight spam" target="_blank">learn more</a>);
- Automate form data workflows with notifications (email/Slack), webhook connections to 1.000+ services and form activity tracking;
- All in a no-code form builder plugin that's right inside of your own WP Admin;
- Integrated with WordPress' most popular content editors and page builders. Easily embed your forms with the shortcode, Gutenberg block or Elementor widget.
- No third-party account needed, not even a Tripetto account;
- GDPR proof.

<a href="https://tripetto.com/wordpress/features/" rel="friend" title="See form builder features" target="_blank">See form builder features here.</a>
<a href="https://tripetto.com/wordpress/feature-list/" rel="friend" title="See full plugin feature list" target="_blank">See full plugin feature list here.</a>

### 🧐 What makes Tripetto a better form builder plugin? ###

💡 With the Tripetto form builder plugin you create stunning custom form experiences instead of boring forms. You can use all your forms in three form layouts, also named 'form faces' in Tripetto:

- **Autoscroll form face**: the autoscroll form face presents one question at a time and is akin to Typeform’s conversational forms;
- **Chat form face**: the chat form face presents all questions and answers as speech bubbles and is partly inspired by Landbot;
- **Classic form face**: the classic form face presents question fields in a traditional form as often seen in SurveyMonkey and the likes.

Just use the form face that suits your form and purpose best. <a href="https://tripetto.com/form-layouts/" rel="friend" title="More info about form faces" target="_blank">See all form layouts.</a>

💡 With the Tripetto form builder plugin you create smart forms that react to the given answers of your respondents. That’s why we call them conversational forms. To help you with this, our unique visual form builder easily lets you create the right logic, like **branch logic**, **jump logic** and **pipe logic**. <a href="https://tripetto.com/logic-types/" rel="friend" title="More info about logic" target="_blank">Learn more about form logic.</a>

💡 With the Tripetto form builder plugin you create interactive quizzes, order forms, exams and more with the **calculator block**. Use the given answers of your respondents to perform any calculation you need in your form. The calculator block can add, subtract, multiply and divide, but also supports formulas and advanced mathematical functions. The result: fully no-code calculator forms! <a href="https://tripetto.com/calculator/" rel="friend" title="More info about the calculator block" target="_blank">Learn more about calculations.</a>

💡 With the Tripetto form builder plugin you create beautiful responsive forms that match your website perfectly, with custom **welcome and end screens**, advanced **styling options** and **full translations**. Your forms are **totally responsive** for mobile, tablet, laptop and desktop usage. <a href="https://tripetto.com/form-layouts/" rel="friend" title="More info about customizations" target="_blank">Learn more about customizations.</a>

💡 With the Tripetto form builder plugin you easily automate your form data workflows. **Receive notifications**, **connect to 1.000+ services** and **track form activity**. <a href="https://tripetto.com/data-automations/" rel="friend" title="More info about automations" target="_blank">Learn more about automations.</a>

### 👷 Give shape - Easily build your forms ###
- Start a new form from scratch, or kickstart your form building with one of the form templates that are included in the form builder plugin;
- Build forms visually on the magnetic storyboard, unlike any other form builder;
- Use subforms on the form builder's storyboard to create forms-in-forms for improved organization and structuring of large forms;
- Real-time preview while building your form in the form builder;
- All question types you need are included in the form builder;
- Add a custom welcome message before the form starts;
- Add a custom closing message at the end of the form, or redirect to another page when the form is completed.

<a href="https://tripetto.com/wordpress/help/building-forms-and-surveys/" rel="friend" title="Learn more about building" target="_blank">Learn more about building your forms.</a>

### 🤯 Add brains - Make your forms smart ###
- Our form builder includes advanced logic features to make your form feel like a conversation;
- Easy to use but very powerful thanks to Tripetto's visual form builder;
- Various types of advanced form logic making it a conversational form;
- Branch logic, to only ask the right follow-up questions;
- Skip logic, to jump over unnecessary questions;
- Pipe logic, to show given answers inside your form;
- Add flexible closing messages and redirects, based on your logic;
- Perform real-time calculations inside your forms with the calculator block in the form builder (Pro);
- Use no-code action blocks in the form builder to work with variables, values and hidden fields (Pro);
- Send emails to yourself and/or respondents with the send email block in the form builder (Pro).

<a href="https://tripetto.com/wordpress/help/using-logic-features/" rel="friend" title="Learn more about using logic" target="_blank">Learn more about using logic in your forms.</a>

### 🎨 Dress up - Customize your forms to your needs ###
- Choose between three form experiences in the form builder: autoscroll, chat, or classic;
- Choose autoscroll for a conversational form, for example, a quiz form, feedback form, screening form or large survey;
- Choose chat for a chat form that feels like a chatbot, for example, a support form, evaluation form (incl. Net Promoter Score/NPS) or RSVP form;
- Choose classic for a traditional looking but smart form, for example, a contact form, reservation form or registration form;
- Style your forms in the form builder (fonts, colors, backgrounds, buttons, inputs);
- All forms are fully optimized for perfect responsiveness on mobile, tablet, laptop and desktop;
- Translate/edit all labels inside your forms in the form builder;
- Remove Tripetto branding (Pro).

<a href="https://tripetto.com/wordpress/help/styling-and-customizing/" rel="friend" title="Learn more about customization" target="_blank">Learn more about styling and customizing your forms.</a>

### ⚡ Hook up - Automate things ###
- Receive email and Slack notifications upon form completion (Pro);
- Connect your responses to 1.000+ services via Make, Zapier, Pabbly and custom webhooks (Pro);
- Track how your respondents use your forms with form activity tracking via Google Analytics, Google Tag Manager, Facebook Pixel and custom tracking codes (Pro).

<a href="https://tripetto.com/wordpress/help/automating-things/" rel="friend" title="Learn more about automating" target="_blank">Learn more about automating your form data.</a>

### 📣 Send out - Share forms how you wish ###
- Immediately share your form with the shareable link in your WP site, making your WP site a complete form tool/survey tool instantly;
- Or embed the form wherever you want with the WP shortcode. Compose your shortcode exactly like you want to embed your form with the shortcode editor in the form builder (no-code);
- Or embed Tripetto forms right inside WordPress' Gutenberg editor. Easily build, customize and automate your forms without leaving the Gutenberg editor;
- Or embed Tripetto forms into your Elementor pages and popups with our Elementor widget.

<a href="https://tripetto.com/wordpress/help/sharing-forms-and-surveys/" rel="friend" title="Learn more about sharing" target="_blank">Learn more about sharing your forms.</a>

### 🛡️ Take stock - Handle form responses safely ###
- Form responses are stored in your own WordPress install/database only (no third-party storage);
- SPAM protection is built right into your forms, so no need for CAPTCHAs etc.;
- View and manage form results inside your WP Admin;
- Export form results to CSV.

<a href="https://tripetto.com/wordpress/help/managing-data-and-results/" rel="friend" title="Learn more about data management" target="_blank">Learn more about data management of your forms.</a>

### 🥇 Pro features ###
Upgrade the Tripetto WordPress form builder plugin to push your forms to the next level and greatly enhance all your forms and surveys. Pro features are:

**Unlimited form building** - No limits in our form builder:

- Unlimited forms
- Unlimited form logic per form
- Unlimited form questions per form
- Unlimited form responses per form
- Unlimited usage of Pro form templates

**Advanced blocks** - Use advanced blocks in your form:

- Subforms
- Signature input

**All action blocks** - Perform advanced actions in your form:

- Calculator
- Custom variable
- Force stop
- Hidden field
- Raise error
- Send email
- Set value

**Tripetto unbranding** - Remove Tripetto branding:

- Remove Tripetto branding in forms
- Remove Tripetto branding in emails

**Notifications** - Receive instant notifications for new form responses:

- Email notifications, including all form data
- Slack notifications, including all form data

**Connections** - Connect your form responses to 1.000+ services using webhooks:

- Connect form data with Make
- Connect form data with Zapier
- Connect form data with Pabbly Connect
- Connect form data with custom webhooks

**Activity tracking** - Track form activity of your respondents:

- Track form activity with Google Analytics
- Track form activity with Google Tag Manager
- Track form activity with Facebook Pixel
- Track form activity with custom tracking codes

**WordPress roles management** - Configure plugin access and capabilities:

- User role access settings
- User role capabilities settings

**Priority support + updates** - Be assured of priority support and all plugin updates:

- Access to help center [24/7]
- Prio support [Mon-Fri, 9-17 CET]
- All updates and upgrades

👉 **<a href="https://tripetto.com/wordpress/pricing/?utm_source=wp_plugin&utm_medium=tripetto_platforms&utm_campaign=pro_upgrade&utm_content=readme" rel="friend" title="Get your Pro license" target="_blank">Get your Pro license today!</a>**
Available as single-site, 5-sites and unlimited sites.

### 🧰 Available as Gutenberg block ###
Tripetto's full-blown form builder is also available right inside the WordPress Gutenberg editor. The Tripetto Gutenberg block makes it easy to build, customize and automate your forms without leaving the Gutenberg editor.

- Add existing forms or build a new form right inside WordPress' Gutenberg editor;
- Build, customize and automate your forms without leaving the Gutenberg editor. All form builder features are available right inside the Gutenberg editor;
- See a live preview of your form in the Gutenberg editor, so you'll constantly see and feel how your form will blend with the rest of your content.

<a href="https://tripetto.com/help/articles/how-to-use-the-tripetto-form-builder-in-the-wordpress-gutenberg-editor/" rel="friend" title="Learn more about Tripetto in the Gutenberg editor" target="_blank">Learn more about Tripetto in the Gutenberg editor</a>

### 🧰 Available as Elementor widget ###
Tripetto forms are also available in Elementor. The Tripetto Elementor widget lets you easily embed your forms into pages and popups that you create with Elementor.

<a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/" rel="friend" title="Learn more about Tripetto in the Elementor builder" target="_blank">Learn more about Tripetto in the Elementor builder</a>

### 🕵️‍♂️ Compare Tripetto with others ###
We are the new kid on the block in the form builder world, so we understand that a comparison will help you decide if Tripetto is the right form builder for you.

- <a href="https://tripetto.com/wpforms-alternative/" rel="friend" title="Tripetto is a WPForms alternative" target="_blank">Compare Tripetto with WPForms</a>
- <a href="https://tripetto.com/contactform7-alternative/" rel="friend" title="Tripetto is a Contact Form 7 alternative" target="_blank">Compare Tripetto with Contact Form 7</a>
- <a href="https://tripetto.com/ninjaforms-alternative/" rel="friend" title="Tripetto is a Ninja Forms alternative" target="_blank">Compare Tripetto with Ninja Forms</a>
- <a href="https://tripetto.com/gravityforms-alternative/" rel="friend" title="Tripetto is a Gravity Forms alternative" target="_blank">Compare Tripetto with Gravity Forms</a>
- <a href="https://tripetto.com/typeform-alternative/" rel="friend" title="Tripetto is a Typeform alternative" target="_blank">Compare Tripetto with Typeform</a>

<a href="https://tripetto.com/compare/" rel="friend" title="Compare Tripetto" target="_blank">Compare Tripetto with other form builders.</a>

### 🔔 Stay up-to-date ###
Any questions about our form builder or your forms? We're happy to help!

- <a href="https://tripetto.com/wordpress/help/" rel="friend" title="Contact us" target="_blank">Help Center</a>
- <a href="https://tripetto.com/contact/" rel="friend" title="Contact us" target="_blank">Contact us</a>
- <a href="https://tripetto.com/subscribe/" rel="friend" title="Subscribe to Tripetto newsletter" target="_blank">Subscribe to our newsletter</a>
- <a href="https://www.twitter.com/tripetto/" rel="friend" title="Follow Tripetto on Twitter" target="_blank">Follow us on Twitter</a>

== Installation ==

👉 Install the Tripetto form builder plugin via the WordPress.org plugin repository or by uploading the files to your server;

👉 Activate the Tripetto form builder plugin through the Plugins page in your WP Admin;

👉 Navigate to the Tripetto tab in your WP Admin menu.

🥇 Get your <a href="https://tripetto.com/wordpress/pricing/?utm_source=wp_plugin&utm_medium=tripetto_platforms&utm_campaign=pro_upgrade&utm_content=readme" rel="friend" target="_blank">Pro License</a> to upgrade your form builder!

== Frequently Asked Questions ==

= Which question types are included? =

The Tripetto form builder plugin offers everything you need to build any form you need.

**Input blocks**
Our form builder naturally supports all the commonly used form question types:

- Checkbox;
- Checkboxes;
- Date (and time);
- Dropdown (single-select);
- Dropdown (multi-select) with keyword search;
- Email address;
- File upload;
- Matrix;
- Multiple choice;
- Number;
- Paragraph;
- Password;
- Phone number;
- Picture choice;
- Radio buttons;
- Ranking;
- Rating;
- Scale;
- Signature (Pro);
- Statement;
- Static text;
- Text (multiple-lines);
- Text (single-line) optionally with suggestions;
- URL;
- Yes/No.

**Action blocks**
Our form builder also offers action blocks to perform smart tasks in the background:

- Calculator (Pro) - Perform advanced calculations with given answers, functions, comparators and constants;
- Custom variable (Pro) - Store all kinds of variable values and use them in your form;
- Force stop (Pro) - Prevent a form from submitting;
- Hidden field (Pro) - Use hidden fields to enter data in your form;
- Raise error (Pro) - Prevent a form from submitting by showing an error message;
- Send email (Pro) - Send emails from the form, including form data;
- Set value (Pro) - Prefill question blocks with the right values.

**Logic blocks**
Our form builder is designed to add powerful logic to make your forms smart and conversational:

- Branch logic;
- Skip logic;
- Pipe logic;
- Flexible endings.

**Condition blocks**
Our form builder helps you to make your forms smart by intelligently and automatically directing flows with dynamic condition blocks:

- Conditions;
- Evaluate;
- Regex;
- Password match;
- Device Size.

<a href="https://tripetto.com/wordpress/help/building-forms-and-surveys/" rel="friend" title="More information about building your forms" target="_blank">More information about building your forms over here.</a>

= Can I use logic in my forms? =

Yes, the Tripetto form builder is all about using the right logic in your form! Our form builder helps you to easily add logic to make your forms smart and conversational:

- Branch logic;
- Skip logic;
- Pipe logic;
- Flexible endings.

<a href="https://tripetto.com/wordpress/help/using-logic-features/" rel="friend" title="More information about using logic features" target="_blank">More information about using logic features over here.</a>

= Can I use calculations in my forms? =

Yes, the Tripetto form builder includes the most advanced form calculator out there (Pro)! It supports the following operators:

- Add;
- Subtract;
- Multiply;
- Divide;
- Equal.

And the calculator block works with several advanced operations to enhance your form calculations:

- Blocks (given answers);
- Mathematical functions;
- Comparators;
- Constants;
- Subcalculations.

<a href="https://tripetto.com/help/articles/how-to-use-the-calculator-block/" rel="friend" title="More information about the calculator block" target="_blank">More information about the calculator block over here.</a>

= Can I receive notifications for new form submissions? =

Yes, Tripetto can keep you updated on new form completions with instant, automatic notifications straight into your Slack and inbox. Our form builder helps you to set this up in no time. <a href="https://tripetto.com/wordpress/help/automating-things/" rel="friend" title="More information about notifications" target="_blank">More information about notifications over here.</a>

= Can I connect other online services to my forms? =

Yes, let Tripetto send your data to 1.000+ connected services with Make, Zapier, Pabbly Connect and custom webhooks. Our form builder helps you to connect your form responses to your automation tool easily. <a href="https://tripetto.com/help/articles/how-to-automate-a-webhook-to-connect-to-other-services-for-each-new-result/" rel="friend" title="More information about connections" target="_blank">More information about connections over here.</a>

= Can I track how my forms perform? =

Yes, you can track form activity by connecting to Google Analytics, Google Tag Manager, Facebook Pixel and even custom tracking codes. Our form builder helps you to configure the connection instantly. <a href=" https://tripetto.com/help/articles/how-to-automate-form-activity-tracking/" rel="friend" title="More information about form activity tracking" target="_blank">More information about form activity tracking over here.</a>

= How do I publish forms in my WP site? =

You can use a shareable link or use the shortcode to embed your forms. Our form builder will help you to get both options running right away. <a href="https://tripetto.com/help/articles/how-to-run-your-form-from-the-wordpress-plugin/" rel="friend" title="More information about sharing" target="_blank">More information about sharing over here.</a>

= Can I use Tripetto in the Gutenberg editor? =

Yes, our full-blown form builder is also available right inside the WordPress Gutenberg editor, so you can easily build, customize and automate your forms without leaving the Gutenberg editor. <a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-gutenberg-block/" rel="friend" title="More information about Tripetto in Gutenberg editor" target="_blank">More information Tripetto in Gutenberg editor.</a>

= Can I use Tripetto in Elementor? =

For sure! Our plugin adds an Elementor widget to your Elementor builder, which makes it super easy to embed your forms in Elementor pages and popups. <a href="https://tripetto.com/help/articles/how-to-embed-your-form-in-your-wordpress-site-using-the-elementor-widget/" rel="friend" title="More information about Tripetto in Elementor" target="_blank">More information Tripetto in Elementor.</a>

= Can I use WordPress roles to give access to Tripetto? =

Yes, we fully support WordPress roles and capabilities! This enables you to configure which roles have access to the plugin and which capabilities each role has inside the plugin. <a href="https://tripetto.com/help/articles/how-to-configure-plugin-access-and-capabilities-with-wordpress-user-roles/" rel="friend" title="More information about WordPress roles" target="_blank">More information about WordPress roles in Tripetto.</a>

= How do I upgrade to Pro? =

There are a few scenarios to upgrade your Tripetto form builder plugin. Generally, you need to take the following steps:

- Purchase a Pro license via our <a href="https://tripetto.com/wordpress/pricing/?utm_source=wp_plugin&utm_medium=tripetto_platforms&utm_campaign=pro_upgrade&utm_content=readme" rel="friend" title="Upgrade to Pro" target="_blank">website</a>, or your WP Admin menu;
- You will receive a **zip package** with the pro version of the form builder plugin and your **personal license key**. After purchasing, you can find these in Freemius (our license partner) and/or in your email;
- Upload, install and activate the zip package in your WP Admin. Enter the license key to activate.

<a href="https://tripetto.com/help/articles/how-to-upgrade-the-wordpress-plugin-to-the-pro-plan/" rel="friend" title="More information about upgrading to pro" target="_blank">More information about upgrading to pro over here.</a>

= Can I get a free Pro trial? =

We don't offer free trials for the Pro plan, but we do offer a 100% unconditional money-back guarantee. Over the next 14 days, if Tripetto isn’t what you hoped, <a href="https://tripetto.com/support/" rel="friend" title="Tripetto Support" target="_blank">drop us a line</a>. We’ll promptly refund the full 100%. No questions asked.

== Screenshots ==

1. Example of a customer satisfaction survey in the autoscroll form face
2. Example of a wedding RSVP form in the chat form face
3. Example of a fitness registration form in the autoscroll form face
4. Example of a restaurant reservation form in the classic form face
5. Demo of the customizations in the form builder, including switching form faces
6. Demo of the visual form builder, with easy-to-use logic

== Translations ==

The following translations are included:

- English (default)
- Dutch (Nederlands)
- French (Français)
- Indonesian (Indonesia)
- Polish (Polski)

*Note:* This plugin is designed to be fully localized and translatable, but we need help to translate it to other languages. Take a look at our <a href="https://gitlab.com/tripetto/translations" rel="friend" title="Translations repository" target="_blank">translations repository</a> to see what needs to be done and how you can contribute.

== Changelog ==

{{ CHANGELOG }}
