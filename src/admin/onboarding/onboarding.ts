import { IDefinition, TL10n, Export, Import, each, set } from "@tripetto/runner";
import { IAutoscrollStyles, run } from "@tripetto/runner-autoscroll";
import { monitorPosition } from "../helpers/position";
import * as Superagent from "superagent";
import "./onboarding.scss";
import "promise-polyfill/src/polyfill";

type TData = {
    [name: string]: string | number | boolean;
};

export function onboarding(
    definition: IDefinition,
    l10n: TL10n,
    styles: IAutoscrollStyles,
    data: TData,
    ajaxUrl: string,
    nonce: string
): void {
    const container = document.createElement("div");
    const element = document.createElement("div");

    container.appendChild(element);

    monitorPosition(container, "wp-tripetto-onboarding");

    container.style.backgroundColor = styles.background?.color || "";

    set(styles, "background", undefined);

    document.body.appendChild(container);

    run({
        element,
        definition,
        l10n,
        styles,
        display: "page",
        onReady: () => setTimeout(() => element.classList.add("wp-tripetto-onboarding-show"), 500),
        onImport: (i) => {
            const fields: Import.IFieldByName[] = [];

            each(
                data,
                (p, name: string) =>
                    fields.push({
                        name,
                        value: p,
                    }),
                {
                    keys: true,
                }
            );

            Import.fields(i, fields);
        },
        onSubmit: (instance) =>
            new Promise((resolve: (id: string | undefined) => void, reject: (reason?: string) => void) => {
                Superagent.post(ajaxUrl)
                    .type("form")
                    .send({
                        action: "tripetto_onboarding",
                        nonce,
                        dataset: JSON.stringify(
                            ((fields) => {
                                const dataset: TData = {};

                                each(fields.fields, (field) => {
                                    dataset[field.name] = field.value as string | number | boolean;
                                });

                                return dataset;
                            })(Export.exportables(instance))
                        ),
                    })
                    .then((result) => {
                        if (result.status === 200 && result.text) {
                            window.location.replace(result.text);
                        } else {
                            reject();
                        }
                    })
                    .catch(() => reject());
            }),
    });
}
