import { monitorPosition } from "../helpers/position";
import "./header.scss";

function limit(s: string, max: number): string {
    if (s.length > max) {
        return s.substr(0, max) + "...";
    }

    return s;
}

export function header(props: {
    readonly back?: string;
    readonly title: string;
    readonly count?: number;
    readonly left?: string;
    readonly right?: string;
    readonly notification?: {
        readonly title: string;
        readonly description: string;
        readonly color?: string;
    };
}) {
    const header = document.createElement("div");
    const wpBody = document.getElementById("wpbody-content");
    const tripettoBody = document.getElementById("wp-tripetto-admin");

    if (wpBody) {
        wpBody.style.marginTop = `${80 + (props.notification ? 52 : 0) + 12}px`;
    }

    if (props.notification) {
        const notification = document.createElement("div");
        const free = document.createElement("div");
        const pro = document.createElement("div");
        const upgrade = document.createElement("a");

        notification.className = "wp-tripetto-header-notification";
        free.textContent = props.notification.title;
        upgrade.href =
            "https://tripetto.com/wordpress/pricing/?utm_source=wp_plugin&utm_medium=tripetto_platforms&utm_campaign=pro_upgrade&utm_content=notification";
        upgrade.target = "_blank";
        upgrade.textContent = props.notification.description;

        if (props.notification.color) {
            notification.style.backgroundColor = props.notification.color;
        }

        pro.appendChild(upgrade);
        notification.appendChild(free);
        notification.appendChild(pro);
        header.appendChild(notification);
    }

    const navigation = document.createElement("div");
    navigation.className = "wp-tripetto-header-navigation";

    const left = document.createElement("div");
    const right = document.createElement("div");
    const title = document.createElement("div");

    if (props.back) {
        const back = document.createElement("a");

        back.className = "wp-tripetto-header-back";
        back.href = props.back;

        left.appendChild(back);
    }

    title.className = "wp-tripetto-header-title";
    title.textContent = limit(props.title, 30) + (typeof props.count === "number" ? ` (${props.count})` : "");

    left.appendChild(title);

    if (props.left) {
        const buttons = document.createElement("div");

        buttons.innerHTML = props.left;

        left.appendChild(buttons);
    }

    if (props.right) {
        right.innerHTML = props.right;
    }

    navigation.appendChild(left);
    navigation.appendChild(right);

    header.appendChild(navigation);

    monitorPosition(header, "wp-tripetto-header");

    document.body.appendChild(header);

    const dropdown = document.getElementById("wp-tripetto-header-dropdown");
    const dropdownButton = document.getElementById("wp-tripetto-header-dropdown-button");

    if (dropdown && dropdownButton) {
        dropdownButton.addEventListener("click", function () {
            dropdown.classList.toggle("wp-tripetto-header-dropdown-open");
        });
    }

    if (tripettoBody) {
        tripettoBody.style.opacity = "1";
    }
}
