import * as Superagent from "superagent";
import "./templates.scss";

interface IRunner {
    run: (props: { definition: {}; styles: {}; element: HTMLDivElement; view: "test"; display: "page"; onReady: () => void }) => void;
}

declare const TripettoRunner: {} | undefined;
declare const TripettoAutoscroll: IRunner | undefined;
declare const TripettoChat: IRunner | undefined;
declare const TripettoClassic: IRunner | undefined;

export function templates() {
    const templateTiles = document.querySelectorAll(".wp-tripetto-template-tile");
    const templatesElement = document.getElementById("wp-tripetto-templates");

    window.addEventListener("load", () => {
        if (templatesElement) {
            templatesElement.style.display = "block";
        }
    });

    for (let t = 0; t < templateTiles.length; t++) {
        const templateButtons = <HTMLElement>templateTiles[t].querySelector(".wp-tripetto-template-buttons");

        if (templateButtons) {
            templateTiles[t].addEventListener("mouseover", function () {
                templateButtons.classList.add("wp-tripetto-template-buttons-active");
            });

            templateTiles[t].addEventListener("mouseout", function () {
                templateButtons.classList.remove("wp-tripetto-template-buttons-active");
            });
        }
    }
}

export function previewTemplate(runner: "autoscroll" | "chat" | "classic", url: string, id: string): void {
    const previewOverlayElement = document.createElement("div");
    const closeElement = document.createElement("div");
    const loaderElement = document.createElement("div");
    const previewElement = document.createElement("div");
    let runnerPackage: IRunner | undefined = undefined;
    let opened = true;
    const fnClose = () => {
        opened = false;

        previewOverlayElement.classList.add("close");

        setTimeout(() => previewOverlayElement.remove(), 300);
    };
    let definition: {} | undefined;
    let styles: {} | undefined;

    const getFoundation = () => {
        return (typeof TripettoRunner !== "undefined" && TripettoRunner) || undefined;
    };

    const getRunner = (runner: string) => {
        switch (runner) {
            case "chat":
                return (typeof TripettoChat !== "undefined" && TripettoChat) || undefined;
            case "classic":
                return (typeof TripettoClassic !== "undefined" && TripettoClassic) || undefined;
            default:
                return (typeof TripettoAutoscroll !== "undefined" && TripettoAutoscroll) || undefined;
        }
    };

    const showPreview = () => {
        if (opened && !runnerPackage) {
            if (definition && styles && getFoundation()) {
                runnerPackage = getRunner(runner);

                if (runnerPackage) {
                    runnerPackage.run({
                        definition,
                        styles,
                        element: previewElement,
                        display: "page",
                        view: "test",
                        onReady: () => {
                            previewElement.classList.add("show");
                        },
                    });

                    return;
                }
            }

            requestAnimationFrame(() => showPreview());
        }
    };

    const loadJS = (url: string) => {
        const scriptElement = document.createElement("script");

        scriptElement.src = url;

        document.body.appendChild(scriptElement);
    };

    previewOverlayElement.className = "wp-tripetto-preview-template";
    previewOverlayElement.onclick = () => fnClose();
    loaderElement.textContent = "Loading template preview...";

    previewOverlayElement.appendChild(loaderElement);
    previewOverlayElement.appendChild(closeElement);
    previewOverlayElement.appendChild(previewElement);

    document.body.appendChild(previewOverlayElement);

    if (!getFoundation()) {
        loadJS(`${url}/vendors/tripetto-runner.js`);
    }

    if (!getRunner(runner)) {
        loadJS(`${url}/vendors/tripetto-runner-${runner}.js`);
    }

    // Load the template definition
    Superagent.get(`${url}/admin/templates/${id}/definition.json`)
        .then((res) => {
            if (res.ok) {
                definition = res.body;

                showPreview();
            }
        })
        .catch(() => fnClose());

    // Load the template styles
    Superagent.get(`${url}/admin/templates/${id}/styles.json`)
        .then((res) => {
            if (res.ok) {
                styles = res.body;

                showPreview();
            }
        })
        .catch(() => fnClose());

    // Show the preview
    requestAnimationFrame(() => {
        previewOverlayElement.classList.add("show");

        showPreview();
    });
}
