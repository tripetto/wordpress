export * from "./header/header";
export * from "./builder/builder";
export * from "./templates/templates";
export * from "./helpers/modal";
export * from "./helpers/clipboard";
export * from "./dashboard/dashboard";
export * from "./onboarding/onboarding";

import "../fonts/fonts.scss";
import "./forms/forms.scss";
import "./results/results.scss";

/** Global package constants */
declare const PACKAGE_NAME: string;
declare const PACKAGE_VERSION: string;

/** Contains the package name. */
export const name = PACKAGE_NAME;

/** Contains the version number. */
export const version = PACKAGE_VERSION;
