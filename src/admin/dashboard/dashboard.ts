import "./dashboard.scss";

export function dashboard() {
    const dashboardElement = document.getElementById("wp-tripetto-dashboard");

    window.addEventListener("load", () => {
        if (dashboardElement) {
            dashboardElement.style.display = "flex";
        }
    });
}
