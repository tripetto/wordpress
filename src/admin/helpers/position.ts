export function monitorPosition(
    element: HTMLElement,
    className: string,
    splitScreen: boolean = false,
    width: string = "100%",
    fullscreen: boolean = false
): () => void {
    const wpAdminBar = document.getElementById("wpadminbar");
    const wpMenu = (!splitScreen && document.getElementById("adminmenuwrap")) || undefined;
    const position = {
        fullscreen,
        left: splitScreen ? width : "160px",
        top: splitScreen ? "0" : "32px",
    };
    const updatePosition = () => {
        element.style.left = position.fullscreen ? "0px" : !splitScreen ? position.left : "auto";
        element.style.width = !position.fullscreen && splitScreen ? position.left : "auto";
        element.style.top = position.fullscreen ? "0px" : position.top;
        element.style.zIndex = position.fullscreen ? "999999" : "auto";
    };

    if (wpAdminBar) {
        if (typeof ResizeObserver !== "undefined") {
            const observer = new ResizeObserver((elements) => {
                elements.forEach((element) => {
                    if (element.target.id === wpMenu?.id) {
                        position.left = `${element.contentRect.width}px`;
                    } else if (element.target.id === wpAdminBar.id) {
                        position.top = `${element.contentRect.height}px`;
                    }
                });

                updatePosition();
            });

            observer.observe(wpAdminBar);

            if (wpMenu) {
                observer.observe(wpMenu);
            }
        }

        if (wpMenu) {
            position.left = `${wpMenu.getBoundingClientRect().width}px`;
        }

        position.top = `${wpAdminBar.getBoundingClientRect().height}px`;
    }

    element.id = className;

    if (splitScreen) {
        element.style.boxShadow = "-5px 0 10px #00000047";
    }

    updatePosition();

    return () => {
        position.fullscreen = !position.fullscreen;

        updatePosition();
    };
}
