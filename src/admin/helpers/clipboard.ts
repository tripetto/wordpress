import copyTextToClipboard from "copy-text-to-clipboard";
import { Str, castToBoolean } from "@tripetto/builder";

interface IShortcode {
    readonly pausable?: boolean;
    readonly persistent?: boolean;
    readonly async?: boolean;
    readonly fullPage?: boolean;
    readonly width?: string;
    readonly height?: string;
    readonly placeholder?: string;
    readonly css?: string;
}

export function copyShortcodeToClipboard(id: number, props: string): void {
    let shortcodeProps: IShortcode | undefined;
    let shortcode = `[tripetto id='${id}'`;

    try {
        shortcodeProps = JSON.parse(atob(props));
    } catch {
        shortcodeProps = undefined;
    }

    if (shortcodeProps) {
        if (shortcodeProps.pausable) {
            shortcode += " pausable='yes'";
        }

        if (shortcodeProps.persistent) {
            shortcode += " persistent='yes'";
        }

        if (!castToBoolean(shortcodeProps.async, true)) {
            shortcode += " async='no'";
        }

        if (shortcodeProps.fullPage) {
            shortcode += " mode='page'";
        }

        if (shortcodeProps.width) {
            shortcode += " width='" + Str.replace(shortcodeProps.width, "'", "") + "'";
        }

        if (shortcodeProps.height) {
            shortcode += " height='" + Str.replace(shortcodeProps.height, "'", "") + "'";
        }

        if (shortcodeProps.placeholder) {
            shortcode += " placeholder='" + encodeURIComponent(shortcodeProps.placeholder).replace(/[']/g, escape) + "'";
        }

        if (shortcodeProps.css) {
            shortcode += " css='" + encodeURIComponent(shortcodeProps.css).replace(/[']/g, escape) + "'";
        }
    }

    shortcode += "]";

    copyTextToClipboard(shortcode);
}
