import "./modal.scss";

/**
 * Shows a modal dialog.
 * @param title Title for the modal.
 * @param message The message displayed in the modal.
 * @param accept The label for the accept button.
 * @param cancel The label for the cancel button.
 * @param action The action that is performed when clicked on the accept button. Can be a function or a URL.
 * @param width Width of the modal.
 * @param height Height of the modal.
 */
export function showModal(
    title: string,
    message: string,
    accept: string,
    cancel: string,
    action: (() => void) | string,
    width: number,
    height: number
): void {
    const overlayElement = document.createElement("div");
    const dialogElement = document.createElement("div");
    const titleElement = document.createElement("span");
    const messageElement = document.createElement("p");
    const buttonsElement = document.createElement("div");
    const cancelButton = document.createElement("a");
    const acceptButton = document.createElement("a");
    const fnClose = () => {
        overlayElement.classList.add("close");

        setTimeout(() => overlayElement.remove(), 300);
    };

    overlayElement.className = "wp-tripetto-modal";

    dialogElement.style.width = `${width}px`;
    dialogElement.style.height = `${height}px`;
    dialogElement.tabIndex = 1;
    cancelButton.tabIndex = 2;
    acceptButton.tabIndex = 3;

    titleElement.textContent = title;
    messageElement.textContent = message;
    acceptButton.textContent = accept;
    cancelButton.textContent = cancel;

    if (typeof action === "string") {
        acceptButton.href = action;
    } else {
        acceptButton.onclick = () => {
            action();
            fnClose();
        };
    }

    cancelButton.onclick = () => fnClose();
    overlayElement.onclick = () => fnClose();

    dialogElement.onkeydown = (e: KeyboardEvent) => {
        if (e.key === "Escape") {
            fnClose();
        }
    };

    buttonsElement.appendChild(cancelButton);
    buttonsElement.appendChild(acceptButton);

    // Add the elements to the DOM
    dialogElement.appendChild(titleElement);
    dialogElement.appendChild(messageElement);
    dialogElement.appendChild(buttonsElement);
    overlayElement.appendChild(dialogElement);
    document.body.appendChild(overlayElement);

    // Show the dialog
    requestAnimationFrame(() => {
        overlayElement.classList.add("show");
        dialogElement.focus();
    });
}
