import { assert, Str, IDefinition } from "@tripetto/builder";
import { TL10n, TStyles } from "@tripetto/runner";
import { monitorPosition } from "../helpers/position";
import "./builder.scss";

declare const PACKAGE_VERSION: string;

const builderFrames: Window[] = [];

export function builder(
    props: {
        readonly id: number;
        readonly shareUrl: string;
        readonly open?: "share" | "styles" | "l10n" | "notifications" | "connections" | "tracking";
        readonly tier?: "pro" | "legacy";
        readonly results?: boolean;
        readonly data?: string;
        readonly onClose?: () => void;
    },
    baseUrl: string,
    ajaxUrl: string,
    language: string,
    onChange?: (
        type: "runner" | "definition" | "styles" | "l10n",
        data: "autoscroll" | "chat" | "classic" | IDefinition | TStyles | TL10n
    ) => void,
    onRefresh?: () => void,
    fullscreen?: boolean
): void {
    const builderElement = document.createElement("div");
    const fnToggleFullscreen = monitorPosition(
        builderElement,
        "wp-tripetto-builder",
        onChange ? true : false,
        props.open === "styles" ? "400px" : props.open === "l10n" ? "600px" : "640px",
        fullscreen
    );
    const builderFrame = assert(
        document.body.appendChild(builderElement).appendChild(document.createElement("iframe")).contentWindow || undefined
    );
    const openSideBySide = onChange && !fullscreen;

    builderFrame.document.open();
    builderFrame.document.write(
        `<body>
           <script src="${baseUrl}/vendors/tripetto-builder.js?ver=${PACKAGE_VERSION}"></script>
           <script src="${baseUrl}/vendors/tripetto-runner.js?ver=${PACKAGE_VERSION}"></script>
           <script src="${baseUrl}/js/wp-tripetto-builder.js?ver=${PACKAGE_VERSION}"></script>
           <script>
           (function(t){t(t)})(function(t){return typeof WPTripettoBuilder===\"undefined\"?setTimeout(function(){t(t)},1):WPTripettoBuilder.bootstrap(${Str.replace(
               JSON.stringify({ ...props, baseUrl, ajaxUrl, language, sideBySide: onChange ? true : false }),
               "</script>",
               "\\u003C/script>"
           )})});
           </script>
        </body>`
    );
    builderFrame.document.close();

    if (onChange) {
        builderFrames.forEach((frame) => {
            frame.postMessage(
                {
                    tripetto: true,
                    type: "terminate",
                },
                window.location.origin
            );
        });
    }

    builderFrames.push(builderFrame);

    window.addEventListener("message", (msg: MessageEvent) => {
        if (msg.data && msg.data.tripetto && msg.data.target === props.id) {
            switch (msg.data.type) {
                case "ready":
                    builderElement.classList.add(openSideBySide ? "sidebyside-ready" : "ready");
                    break;
                case "fullscreen":
                    fnToggleFullscreen();
                    break;
                case "results":
                    builderElement.classList.remove(openSideBySide ? "sidebyside-ready" : "ready");

                    window.open(`admin.php?page=tripetto-forms&action=results&id=${props.id}`, "_self");
                    break;
                case "definition":
                    if (onChange) {
                        onChange("definition", msg.data.definition);
                    }
                    break;
                case "styles":
                    if (onChange) {
                        onChange("styles", msg.data.styles);
                    }
                    break;
                case "l10n":
                    if (onChange) {
                        onChange("l10n", msg.data.l10n);
                    }
                    break;
                case "runner":
                    if (onChange) {
                        onChange("runner", msg.data.runner);
                    }
                    break;
                case "closing":
                    builderElement.classList.remove("ready");

                    const status = document.getElementById("WPTripettoStatus");

                    if (status) {
                        status.innerHTML =
                            "💾 <b>One moment while your form is being saved...</b><br/><br/>⚠️ Please do not leave this page as you may lose data. You will be redirected automatically when your data is stored.";
                    }
                    break;
                case "close":
                    if (props.onClose) {
                        props.onClose();
                    }

                    if (onChange) {
                        if (openSideBySide) {
                            builderElement.classList.add("sidebyside-close");
                            builderElement.classList.remove("sidebyside-ready");

                            setTimeout(() => builderElement.parentElement?.removeChild(builderElement), 500);
                        } else {
                            builderElement.parentElement?.removeChild(builderElement);
                        }

                        for (let w = 0; w < builderFrames.length; w++) {
                            if (builderFrames[w] === builderFrame) {
                                builderFrames.splice(w, 1);

                                break;
                            }
                        }
                    } else {
                        builderElement.classList.remove("ready");

                        window.open(`admin.php?page=tripetto-forms`, "_self");
                    }
                    break;
                case "refresh":
                    if (onRefresh) {
                        builderElement.parentElement?.removeChild(builderElement);

                        onRefresh();
                    } else {
                        builderElement.classList.remove("ready");

                        location.reload();
                    }
                    break;
            }
        }
    });
}
