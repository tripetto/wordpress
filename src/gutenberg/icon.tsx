import * as React from "react";
import { base } from "./globals";

const icon = () => <img src={base + "/assets/tripetto.png"} width="24" height="24" />;

export default icon;
