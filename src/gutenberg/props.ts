export interface IBlockProps {
    readonly preview?: boolean;
    readonly form?: number;
    readonly pausable?: boolean;
    readonly persistent?: boolean;
    readonly async?: boolean;
    readonly width?: string;
    readonly height?: string;
    readonly placeholder?: string;
    readonly css?: string;
    readonly className?: string;
}
