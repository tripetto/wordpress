declare const tripetto_gutenberg_block: {
    readonly capability_create: boolean;
    readonly capability_edit: boolean;
    readonly capability_run: boolean;
    readonly capability_results: boolean;
};

export const canEdit = tripetto_gutenberg_block.capability_edit || false;
export const canCreate = (canEdit && tripetto_gutenberg_block.capability_create) || false;
export const canRun = tripetto_gutenberg_block.capability_run || false;
export const canResults = tripetto_gutenberg_block.capability_results || false;
