import * as React from "react";
import * as Superagent from "superagent";
import { BlockEditProps } from "@wordpress/blocks";
import { InspectorControls, BlockControls } from "@wordpress/block-editor";
import {
    ToolbarGroup,
    ToolbarDropdownMenu,
    ToolbarButton,
    ToggleControl,
    Button,
    Panel,
    PanelBody,
    PanelRow,
    Placeholder,
    SelectControl,
    TextControl,
    TextareaControl,
    RadioControl,
    Spinner,
} from "@wordpress/components";
import { IDefinition, setAny, each } from "@tripetto/builder";
import { L10n, TL10n, TStyles } from "@tripetto/runner";
import { IBlockProps } from "./props";
import { canCreate, canEdit, canResults, canRun } from "./capabilities";
import { base, url, language } from "./globals";
import { getLocale, getTranslation } from "../builder/helpers/l10n";
import l10n from "./l10n";

declare const PACKAGE_LICENSE: string;

const HELP_URL = "https://tripetto.com/help/articles/how-to-use-the-tripetto-form-builder-in-the-wordpress-gutenberg-editor/";

interface IRunnerController {
    definition: IDefinition;
    styles: TStyles;
    l10n: TL10n;
    readonly restart: () => void;
    readonly destroy: () => void;
}

interface IRunnerNamespace {
    readonly run: (props: {
        readonly license: string;
        readonly element: HTMLElement;
        readonly definition?: IDefinition;
        readonly styles: TStyles;
        readonly l10n: TL10n;
        readonly language: string;
        readonly locale: (locale: "auto" | string) => Promise<L10n.ILocale | undefined>;
        readonly translations: (language: "auto" | string, context: string) => Promise<L10n.TTranslation | L10n.TTranslation[] | undefined>;
        readonly onTouch: () => void;
        readonly onSubmit?: () => boolean;
        readonly onPause?: {
            readonly recipe: "email";
            readonly onPause: () => boolean;
        };
        readonly className?: string;
        readonly customStyle?: React.CSSProperties;
        readonly customCSS?: string;
    }) => Promise<IRunnerController>;
}

interface IBuilderProps {
    readonly id: number;
    readonly runner: "autoscroll" | "chat" | "classic";
    readonly open?: "share" | "styles" | "l10n" | "notifications" | "connections" | "tracking";
    readonly definition?: IDefinition;
    readonly styles: TStyles;
    readonly l10n: TL10n;
    readonly data?: string;
    readonly entries?: number;
    readonly onClose?: () => void;
}

declare const TripettoAutoscroll: IRunnerNamespace | undefined;
declare const TripettoChat: IRunnerNamespace | undefined;
declare const TripettoClassic: IRunnerNamespace | undefined;

declare const WPTripetto: {
    builder: (
        props: IBuilderProps,
        base: string,
        url: string,
        language: string,
        onChange: (
            type: "runner" | "definition" | "styles" | "l10n",
            data: "autoscroll" | "chat" | "classic" | IDefinition | TStyles | TL10n
        ) => void,
        onRefresh?: () => void,
        fullscreen?: boolean
    ) => void;
};

function getRunner(runner: "autoscroll" | "chat" | "classic"): IRunnerNamespace | undefined {
    switch (runner) {
        case "chat":
            return (typeof TripettoChat !== "undefined" && TripettoChat) || undefined;
        case "classic":
            return (typeof TripettoClassic !== "undefined" && TripettoClassic) || undefined;
        default:
            return (typeof TripettoAutoscroll !== "undefined" && TripettoAutoscroll) || undefined;
    }
}

function loadRunner(
    element: HTMLDivElement,
    props: IBuilderProps,
    attributes: {
        pausable?: boolean;
        width?: string;
        height?: string;
        css?: string;
        className?: string;
    },
    controllerRef: React.MutableRefObject<true | IRunnerController | undefined>
): void {
    const runner = getRunner(props.runner);

    controllerRef.current = true;

    if (runner) {
        if (props.data) {
            try {
                const values: {
                    [variable: string]: string;
                } = {};

                each(
                    JSON.parse(atob(props.data)),
                    (
                        variable: {
                            readonly description: string;
                            readonly value: string;
                        },
                        name
                    ) => {
                        values[name] = variable.value;
                    },
                    { keys: true }
                );

                setAny(window, "TRIPETTO_CUSTOM_VARIABLES", values);
            } catch {}
        }

        runner
            .run({
                license: PACKAGE_LICENSE,
                element,
                definition: props.definition,
                styles: props.styles,
                l10n: props.l10n,
                language,
                locale: (locale) => getLocale(base, url, locale),
                translations: (language, context) => getTranslation(base, url, language, context),
                onTouch: () => element.parentElement?.focus(),
                onSubmit: () => true,
                onPause:
                    (attributes.pausable && {
                        recipe: "email",
                        onPause: () => true,
                    }) ||
                    undefined,
                className: attributes.className,
                customCSS: attributes.css,
                customStyle:
                    attributes.width || attributes.height
                        ? {
                              width: attributes.width,
                              height: attributes.height,
                          }
                        : undefined,
            })
            .then((ref) => {
                controllerRef.current = ref;
            });
    } else {
        element.innerHTML = "⚠️ " + l10n.error_loading_preview;
    }
}

const Builder = (props: BlockEditProps<IBlockProps>) => {
    const { attributes, setAttributes } = props;
    const [forms, setForms] = React.useState<
        {
            readonly label: string;
            readonly value: string;
        }[]
    >();
    const [status, setStatus] = React.useState<"loading-forms" | "loading-form" | "ready" | "error">("ready");
    const [locked, setLocked] = React.useState(false);
    const [activeRunner, setActiveRunner] = React.useState<"autoscroll" | "chat" | "classic">("autoscroll");
    const [, doUpdate] = React.useState<{}>();
    const elementRef = React.useRef<HTMLDivElement>(null);
    const controllerRef = React.useRef<true | IRunnerController>();
    const runnerRef = React.useRef<"autoscroll" | "chat" | "classic">();
    const formRef = React.useRef<IBuilderProps>();
    const attributesRef = React.useRef<IBlockProps>({});
    const debounceRef = React.useRef<number>();
    const positionRef = React.useRef<number>();
    const setForm = (form: IBuilderProps | undefined) => {
        formRef.current = form;

        doUpdate({});
    };
    const createForm = (runner: "autoscroll" | "chat" | "classic" = "autoscroll") => {
        setStatus("loading-form");

        Superagent.post(url)
            .type("form")
            .send({
                action: "tripetto_gutenberg_create",
                runner,
            })
            .then((response) => {
                if (response.ok && response.body) {
                    setForm(response.body);
                    setAttributes({
                        form: response.body.id,
                    });
                    setStatus("ready");
                } else {
                    setStatus("error");
                }
            })
            .catch(() => setStatus("error"));
    };

    const reloadRunner = () => {
        if (elementRef.current && formRef.current) {
            runnerRef.current = formRef.current.runner;

            if (controllerRef.current && controllerRef.current !== true) {
                controllerRef.current.destroy();
            }

            attributesRef.current = { ...attributes };

            loadRunner(elementRef.current, formRef.current, attributes, controllerRef);
        }
    };

    const changeRunner = (runner: "autoscroll" | "chat" | "classic") => {
        if (!locked) {
            setActiveRunner(runner);

            if (formRef.current && runnerRef.current !== runner) {
                setLocked(true);

                Superagent.post(url)
                    .type("form")
                    .send({
                        action: "tripetto_runner",
                        id: attributes.form,
                        runner,
                        overwrite: true,
                    })
                    .then((res) => {
                        if (res.ok && formRef.current) {
                            setForm({ ...formRef.current, runner });
                        }

                        setLocked(false);
                    })
                    .catch(() => setLocked(false));
            }
        }
    };

    const openBuilder = (open?: "styles" | "l10n" | "notifications" | "connections" | "tracking" | "fullscreen") => {
        if (formRef.current && controllerRef.current && controllerRef.current !== true && !locked) {
            setLocked(true);

            Superagent.post(url)
                .type("form")
                .send({
                    action: "tripetto_gutenberg_load",
                    id: attributes.form,
                })
                .then((response) => {
                    if (response.ok && response.body) {
                        setForm(response.body);

                        if (controllerRef.current && controllerRef.current !== true && formRef.current) {
                            if (formRef.current.definition) {
                                controllerRef.current.definition = formRef.current.definition;
                            }

                            controllerRef.current.styles = formRef.current.styles;
                            controllerRef.current.l10n = formRef.current.l10n;
                        }

                        WPTripetto.builder(
                            {
                                ...response.body,
                                open: (open !== "fullscreen" && open) || undefined,
                                onClose: () => setLocked(false),
                            },
                            base,
                            url,
                            language,
                            (type, data) => {
                                switch (type) {
                                    case "runner":
                                        if (formRef.current) {
                                            setForm({ ...formRef.current, runner: data as "autoscroll" | "chat" | "classic" });
                                        }
                                        break;
                                    case "definition":
                                        const definition = data as IDefinition;

                                        if (controllerRef.current && controllerRef.current !== true) {
                                            controllerRef.current.definition = definition;
                                        }

                                        if (formRef.current) {
                                            setForm({ ...formRef.current, definition });
                                        }

                                        break;
                                    case "styles":
                                        const styles = data as TStyles;

                                        if (controllerRef.current && controllerRef.current !== true) {
                                            controllerRef.current.styles = styles;
                                        }

                                        if (formRef.current) {
                                            setForm({ ...formRef.current, styles });
                                        }

                                        break;
                                    case "l10n":
                                        const l10n = data as TL10n;

                                        if (controllerRef.current && controllerRef.current !== true) {
                                            controllerRef.current.l10n = l10n;
                                        }

                                        if (formRef.current) {
                                            setForm({ ...formRef.current, l10n });
                                        }

                                        break;
                                }
                            },
                            () => refresh(),
                            open === "fullscreen"
                        );
                    } else {
                        setLocked(false);
                    }
                })
                .catch(() => setLocked(false));
        }
    };

    const refresh = () => {
        if (controllerRef.current && controllerRef.current !== true) {
            controllerRef.current.destroy();
        }

        runnerRef.current = undefined;
        controllerRef.current = undefined;
        formRef.current = undefined;

        setForms(undefined);
        setStatus("ready");
        setLocked(false);
    };

    const detach = () => {
        refresh();

        setAttributes({
            form: undefined,
        });
    };

    const viewResults = () => {
        if (formRef.current && formRef.current.id > 0) {
            window.open(`admin.php?page=tripetto-forms&action=results&id=${formRef.current.id}`, "_blank");
        }
    };

    if (attributes.preview) {
        return <img src={base + "/assets/preview.png"} />;
    }

    React.useEffect(() => {
        if (status === "ready") {
            if (attributes.form && !formRef.current) {
                setStatus("loading-form");

                Superagent.post(url)
                    .type("form")
                    .send({
                        action: "tripetto_gutenberg_load",
                        id: attributes.form,
                    })
                    .then((response) => {
                        if (response.ok && response.body) {
                            setForm(response.body);
                            setStatus("ready");
                        } else {
                            setStatus("error");
                        }
                    })
                    .catch(() => setStatus("error"));
            } else if (!attributes.form && !forms) {
                if (canEdit || canRun) {
                    setStatus("loading-forms");

                    Superagent.post(url)
                        .type("form")
                        .send({
                            action: "tripetto_gutenberg_fetch",
                        })
                        .then((response) => {
                            if (response.ok && response.body && response.body.forms) {
                                setForms(
                                    response.body.forms.map((form: { id: number; name: string; date: string }) => ({
                                        value: `${form.id}`,
                                        label: `${form.name} (${form.date})`,
                                    }))
                                );
                                setStatus("ready");
                            } else {
                                setStatus("error");
                            }
                        })
                        .catch(() => setStatus("error"));
                } else {
                    setForms([]);
                    setStatus("ready");
                }
            }
        }
    });

    React.useLayoutEffect(() => {
        let positionMonitor = 0;

        if (elementRef.current && elementRef.current.parentElement) {
            positionRef.current =
                Array.from(elementRef.current.parentElement.parentElement?.childNodes || []).indexOf(elementRef.current.parentElement) + 1;

            positionMonitor = setInterval(() => {
                if (elementRef.current && elementRef.current.parentElement) {
                    const position =
                        Array.from(elementRef.current.parentElement.parentElement?.childNodes || []).indexOf(
                            elementRef.current.parentElement
                        ) + 1;

                    if (positionRef.current !== position) {
                        positionRef.current = position;

                        reloadRunner();
                    }
                }
            }, 1000) as {} as number;
        }

        if (!controllerRef.current || runnerRef.current !== formRef.current?.runner) {
            reloadRunner();

            if (formRef.current?.runner) {
                setActiveRunner(formRef.current?.runner);
            }
        }

        if (debounceRef.current) {
            clearTimeout(debounceRef.current);
        }

        debounceRef.current = setTimeout(() => {
            if (
                attributesRef.current.pausable !== attributes.pausable ||
                attributesRef.current.width !== attributes.width ||
                attributesRef.current.height !== attributes.height ||
                attributesRef.current.css !== attributes.css ||
                attributesRef.current.className !== attributes.className
            ) {
                reloadRunner();
            }

            debounceRef.current = 0;
        }, 1000) as {} as number;

        () => {
            clearTimeout(debounceRef.current);
            clearInterval(positionMonitor);
        };
    });

    if ((attributes.form && !formRef.current && status !== "error") || status === "loading-form") {
        return (
            <>
                <Spinner />
                {l10n.loading_preview}
            </>
        );
    } else if (!attributes.form || status === "error") {
        return (
            <>
                <Placeholder
                    icon={<img src={base + "/assets/tripetto.png"} width="24" height="24" style={{ marginRight: "12px" }} />}
                    label="Tripetto"
                    isColumnLayout={false}
                    className="wp-tripetto-gutenberg"
                >
                    {status === "error" ? (
                        attributes.form ? (
                            <div style={{ display: "flex", flexDirection: "column", alignItems: "flex-start" }}>
                                ❌ {l10n.error_loading_form}
                                <Button
                                    icon="open-folder"
                                    variant="primary"
                                    onClick={() => {
                                        setForms(undefined);
                                        setAttributes({
                                            form: undefined,
                                        });
                                        setStatus("ready");
                                    }}
                                    style={{
                                        marginTop: "16px",
                                    }}
                                >
                                    {l10n.select_another}
                                </Button>
                            </div>
                        ) : (
                            <div style={{ display: "flex", flexDirection: "column", alignItems: "flex-start" }}>
                                ❌ {l10n.error_loading_forms}
                                <Button
                                    icon="update-alt"
                                    variant="primary"
                                    onClick={() => setStatus("ready")}
                                    style={{
                                        marginTop: "16px",
                                    }}
                                >
                                    {l10n.retry}
                                </Button>
                            </div>
                        )
                    ) : status !== "ready" ? (
                        <>
                            <Spinner />
                            {l10n.loading_forms}
                        </>
                    ) : canEdit || canRun ? (
                        <>
                            {forms?.length || !canCreate ? (
                                <>
                                    <SelectControl
                                        value=""
                                        options={[{ label: "📂 " + l10n.select_form, value: "" }, ...(forms || [])]}
                                        onChange={(form) => {
                                            const id = parseInt(form);

                                            if (id > 0) {
                                                formRef.current = undefined;

                                                setAttributes({
                                                    form: id,
                                                });
                                            }
                                        }}
                                        style={{
                                            height: "36px",
                                            lineHeight: "16px",
                                            maxWidth: "300px",
                                        }}
                                    />
                                </>
                            ) : undefined}

                            {canCreate && (
                                <>
                                    {(forms?.length && <div style={{ margin: "8px" }}>{l10n.or}</div>) || undefined}
                                    <Button icon="plus-alt2" variant="primary" onClick={() => createForm()}>
                                        {l10n.build_form}
                                    </Button>
                                </>
                            )}
                        </>
                    ) : (
                        <>⚠️ {l10n.insufficient_rights}</>
                    )}
                </Placeholder>
                <BlockControls controls={{}}>
                    <ToolbarGroup>
                        <ToolbarButton
                            label={l10n.help}
                            icon="editor-help"
                            onClick={() => {
                                window.open(HELP_URL, "_blank");
                            }}
                        />
                    </ToolbarGroup>
                </BlockControls>
            </>
        );
    }

    return (
        <>
            <InspectorControls>
                <Panel>
                    <PanelBody title={formRef.current?.definition?.name || l10n.unnamed_form}>
                        {canEdit && (
                            <>
                                <PanelRow>
                                    <Button icon="edit" variant="tertiary" disabled={locked} onClick={() => openBuilder("fullscreen")}>
                                        {l10n.edit_form_fullscreen}
                                    </Button>
                                </PanelRow>
                                <PanelRow>
                                    <Button icon="align-pull-right" variant="tertiary" disabled={locked} onClick={() => openBuilder()}>
                                        {l10n.edit_form_side_by_side}
                                    </Button>
                                </PanelRow>
                                <PanelRow>
                                    <Button
                                        icon="admin-appearance"
                                        variant="tertiary"
                                        disabled={locked}
                                        onClick={() => openBuilder("styles")}
                                    >
                                        {l10n.edit_styles}
                                    </Button>
                                </PanelRow>
                                <PanelRow>
                                    <Button icon="translation" variant="tertiary" disabled={locked} onClick={() => openBuilder("l10n")}>
                                        {l10n.edit_translations}
                                    </Button>
                                </PanelRow>
                            </>
                        )}
                        {canResults && (
                            <PanelRow>
                                <Button icon="list-view" variant="tertiary" disabled={locked} onClick={() => viewResults()}>
                                    {l10n.view_results + ((formRef.current?.entries || 0) > 0 ? ` (${formRef.current?.entries || 0})` : "")}
                                </Button>
                            </PanelRow>
                        )}
                        <PanelRow>
                            <Button icon="editor-help" variant="tertiary" onClick={() => window.open(HELP_URL, "_blank")}>
                                {l10n.help}
                            </Button>
                        </PanelRow>
                        <PanelRow>
                            <Button
                                icon="editor-unlink"
                                variant="tertiary"
                                isDestructive={true}
                                disabled={locked || (!canRun && !canEdit)}
                                onClick={() => detach()}
                            >
                                {l10n.detach_form}
                            </Button>
                        </PanelRow>
                    </PanelBody>
                    {canEdit && (
                        <>
                            <PanelBody title={l10n.form_face} initialOpen={false}>
                                <PanelRow>{l10n.form_face_label}</PanelRow>
                                <PanelRow>
                                    <RadioControl
                                        selected={activeRunner}
                                        disabled={locked}
                                        options={[
                                            { label: "Autoscroll", value: "autoscroll" },
                                            { label: "Chat", value: "chat" },
                                            { label: "Classic", value: "classic" },
                                        ]}
                                        onChange={(runner: string) => changeRunner(runner as "autoscroll" | "chat" | "classic")}
                                    />
                                </PanelRow>
                            </PanelBody>
                            <PanelBody title={l10n.automate} initialOpen={false}>
                                <PanelRow>
                                    <Button icon="bell" variant="tertiary" disabled={locked} onClick={() => openBuilder("notifications")}>
                                        {l10n.notifications}
                                    </Button>
                                </PanelRow>
                                <PanelRow>
                                    <Button icon="share" variant="tertiary" disabled={locked} onClick={() => openBuilder("connections")}>
                                        {l10n.connections}
                                    </Button>
                                </PanelRow>
                                <PanelRow>
                                    <Button icon="visibility" variant="tertiary" disabled={locked} onClick={() => openBuilder("tracking")}>
                                        {l10n.tracking}
                                    </Button>
                                </PanelRow>
                            </PanelBody>
                        </>
                    )}
                    <PanelBody
                        title={l10n.options}
                        initialOpen={attributes.pausable || attributes.persistent || attributes.async === false}
                    >
                        <PanelRow>
                            <ToggleControl
                                label={l10n.pausable}
                                help={l10n.pausable_help}
                                checked={attributes.pausable || false}
                                onChange={() =>
                                    setAttributes({
                                        pausable: !attributes.pausable,
                                    })
                                }
                            />
                        </PanelRow>
                        <PanelRow>
                            <ToggleControl
                                label={l10n.persistent}
                                help={l10n.persistent_help}
                                checked={attributes.persistent || false}
                                onChange={() =>
                                    setAttributes({
                                        persistent: !attributes.persistent,
                                    })
                                }
                            />
                        </PanelRow>
                        <PanelRow>
                            <ToggleControl
                                label={l10n.async}
                                help={l10n.async_help}
                                checked={attributes.async === false}
                                onChange={() =>
                                    setAttributes({
                                        async: attributes.async === false ? undefined : false,
                                    })
                                }
                            />
                        </PanelRow>
                    </PanelBody>
                    <PanelBody title={l10n.width} initialOpen={attributes.width ? true : false}>
                        <PanelRow>
                            <TextControl
                                label={l10n.width_label}
                                placeholder="100%"
                                value={attributes.width || ""}
                                onChange={(width) =>
                                    setAttributes({
                                        width: width || undefined,
                                    })
                                }
                            />
                        </PanelRow>
                    </PanelBody>
                    <PanelBody title={l10n.height} initialOpen={attributes.height ? true : false}>
                        <PanelRow>
                            <TextControl
                                label={l10n.height_label}
                                placeholder="auto"
                                value={attributes.height || ""}
                                onChange={(height) =>
                                    setAttributes({
                                        height: height || undefined,
                                    })
                                }
                            />
                        </PanelRow>
                    </PanelBody>
                    <PanelBody title={l10n.placeholder} initialOpen={attributes.placeholder ? true : false}>
                        <PanelRow>
                            <TextareaControl
                                label={l10n.placeholder_label}
                                help={l10n.placeholder_help}
                                value={attributes.placeholder || ""}
                                onChange={(placeholder) =>
                                    setAttributes({
                                        placeholder: placeholder || undefined,
                                    })
                                }
                            />
                        </PanelRow>
                    </PanelBody>
                    <PanelBody title={l10n.css} initialOpen={attributes.css ? true : false}>
                        <PanelRow>
                            <TextareaControl
                                label={l10n.css_label}
                                help={l10n.css_help}
                                placeholder={l10n.css_placeholder}
                                value={attributes.css || ""}
                                onChange={(css) =>
                                    setAttributes({
                                        css: css || undefined,
                                    })
                                }
                            />
                        </PanelRow>
                    </PanelBody>
                </Panel>
            </InspectorControls>
            <BlockControls controls={{}}>
                {canEdit && (
                    <>
                        <ToolbarGroup>
                            <ToolbarDropdownMenu
                                icon="edit"
                                label={l10n.edit_form}
                                controls={[
                                    {
                                        icon: "fullscreen-alt",
                                        title: l10n.edit_form_fullscreen,
                                        isDisabled: locked,
                                        onClick: () => openBuilder("fullscreen"),
                                    },
                                    {
                                        icon: "align-pull-right",
                                        title: l10n.edit_form_side_by_side,
                                        isDisabled: locked,
                                        onClick: () => openBuilder(),
                                    },
                                ]}
                            />
                        </ToolbarGroup>
                        <ToolbarGroup>
                            <ToolbarDropdownMenu
                                icon="cover-image"
                                label={l10n.form_face}
                                controls={[
                                    {
                                        icon: "align-wide",
                                        title: "Autoscroll",
                                        isDisabled: runnerRef.current === "autoscroll" || locked,
                                        onClick: () => changeRunner("autoscroll"),
                                    },
                                    {
                                        icon: "testimonial",
                                        title: "Chat",
                                        isDisabled: runnerRef.current === "chat" || locked,
                                        onClick: () => changeRunner("chat"),
                                    },
                                    {
                                        icon: "menu-alt",
                                        title: "Classic",
                                        isDisabled: runnerRef.current === "classic" || locked,
                                        onClick: () => changeRunner("classic"),
                                    },
                                ]}
                            />
                            <ToolbarButton
                                label={l10n.edit_styles}
                                icon="admin-appearance"
                                disabled={locked}
                                onClick={() => openBuilder("styles")}
                            />
                            <ToolbarButton
                                label={l10n.edit_translations}
                                icon="translation"
                                disabled={locked}
                                onClick={() => openBuilder("l10n")}
                            />
                        </ToolbarGroup>
                        <ToolbarGroup>
                            <ToolbarButton
                                label={l10n.notifications}
                                icon="bell"
                                disabled={locked}
                                onClick={() => openBuilder("notifications")}
                            />
                            <ToolbarButton
                                label={l10n.connections}
                                icon="share"
                                disabled={locked}
                                onClick={() => openBuilder("connections")}
                            />
                            <ToolbarButton
                                label={l10n.tracking}
                                icon="visibility"
                                disabled={locked}
                                onClick={() => openBuilder("tracking")}
                            />
                        </ToolbarGroup>
                    </>
                )}
                {canResults && (
                    <ToolbarGroup>
                        <ToolbarButton
                            label={l10n.view_results + ((formRef.current?.entries || 0) > 0 ? ` (${formRef.current?.entries || 0})` : "")}
                            icon="list-view"
                            disabled={locked}
                            onClick={() => viewResults()}
                        />
                    </ToolbarGroup>
                )}
                <ToolbarGroup>
                    <ToolbarButton
                        label={l10n.restart}
                        icon="update"
                        onClick={() => {
                            if (controllerRef.current && controllerRef.current !== true) {
                                controllerRef.current.restart();
                            }
                        }}
                    />
                    <ToolbarButton
                        label={l10n.help}
                        icon="editor-help"
                        onClick={() => {
                            window.open(HELP_URL, "_blank");
                        }}
                    />
                </ToolbarGroup>
            </BlockControls>
            <div ref={elementRef}></div>
        </>
    );
};

export default Builder;
