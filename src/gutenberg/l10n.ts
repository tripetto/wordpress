declare const tripetto_gutenberg_block: {
    readonly l10n_title: string;
    readonly l10n_description: string;
    readonly l10n_keyword_form: string;
    readonly l10n_keyword_survey: string;
    readonly l10n_loading_preview: string;
    readonly l10n_loading_forms: string;
    readonly l10n_error_loading_preview: string;
    readonly l10n_error_loading_forms: string;
    readonly l10n_error_loading_form: string;
    readonly l10n_insufficient_rights: string;
    readonly l10n_select_form: string;
    readonly l10n_build_form: string;
    readonly l10n_or: string;
    readonly l10n_unnamed_form: string;
    readonly l10n_select_another: string;
    readonly l10n_retry: string;
    readonly l10n_form_face: string;
    readonly l10n_form_face_label: string;
    readonly l10n_edit_form: string;
    readonly l10n_edit_form_fullscreen: string;
    readonly l10n_edit_form_side_by_side: string;
    readonly l10n_edit_styles: string;
    readonly l10n_edit_translations: string;
    readonly l10n_automate: string;
    readonly l10n_notifications: string;
    readonly l10n_connections: string;
    readonly l10n_tracking: string;
    readonly l10n_view_results: string;
    readonly l10n_restart: string;
    readonly l10n_options: string;
    readonly l10n_pausable: string;
    readonly l10n_pausable_help: string;
    readonly l10n_persistent: string;
    readonly l10n_persistent_help: string;
    readonly l10n_async: string;
    readonly l10n_async_help: string;
    readonly l10n_width: string;
    readonly l10n_width_label: string;
    readonly l10n_height: string;
    readonly l10n_height_label: string;
    readonly l10n_placeholder: string;
    readonly l10n_placeholder_label: string;
    readonly l10n_placeholder_help: string;
    readonly l10n_css: string;
    readonly l10n_css_label: string;
    readonly l10n_css_help: string;
    readonly l10n_css_placeholder: string;
    readonly l10n_detach_form: string;
    readonly l10n_help: string;
};

const l10n = {
    title: tripetto_gutenberg_block.l10n_title,
    description: tripetto_gutenberg_block.l10n_description,
    keyword_form: tripetto_gutenberg_block.l10n_keyword_form,
    keyword_survey: tripetto_gutenberg_block.l10n_keyword_survey,
    loading_preview: tripetto_gutenberg_block.l10n_loading_preview,
    loading_forms: tripetto_gutenberg_block.l10n_loading_forms,
    error_loading_preview: tripetto_gutenberg_block.l10n_error_loading_preview,
    error_loading_forms: tripetto_gutenberg_block.l10n_error_loading_forms,
    error_loading_form: tripetto_gutenberg_block.l10n_error_loading_form,
    insufficient_rights: tripetto_gutenberg_block.l10n_insufficient_rights,
    select_form: tripetto_gutenberg_block.l10n_select_form,
    build_form: tripetto_gutenberg_block.l10n_build_form,
    or: tripetto_gutenberg_block.l10n_or,
    unnamed_form: tripetto_gutenberg_block.l10n_unnamed_form,
    select_another: tripetto_gutenberg_block.l10n_select_another,
    retry: tripetto_gutenberg_block.l10n_retry,
    form_face: tripetto_gutenberg_block.l10n_form_face,
    form_face_label: tripetto_gutenberg_block.l10n_form_face_label,
    edit_form: tripetto_gutenberg_block.l10n_edit_form,
    edit_form_fullscreen: tripetto_gutenberg_block.l10n_edit_form_fullscreen,
    edit_form_side_by_side: tripetto_gutenberg_block.l10n_edit_form_side_by_side,
    edit_styles: tripetto_gutenberg_block.l10n_edit_styles,
    edit_translations: tripetto_gutenberg_block.l10n_edit_translations,
    automate: tripetto_gutenberg_block.l10n_automate,
    notifications: tripetto_gutenberg_block.l10n_notifications,
    connections: tripetto_gutenberg_block.l10n_connections,
    tracking: tripetto_gutenberg_block.l10n_tracking,
    view_results: tripetto_gutenberg_block.l10n_view_results,
    restart: tripetto_gutenberg_block.l10n_restart,
    options: tripetto_gutenberg_block.l10n_options,
    pausable: tripetto_gutenberg_block.l10n_pausable,
    pausable_help: tripetto_gutenberg_block.l10n_pausable_help,
    persistent: tripetto_gutenberg_block.l10n_persistent,
    persistent_help: tripetto_gutenberg_block.l10n_persistent_help,
    async: tripetto_gutenberg_block.l10n_async,
    async_help: tripetto_gutenberg_block.l10n_async_help,
    width: tripetto_gutenberg_block.l10n_width,
    width_label: tripetto_gutenberg_block.l10n_width_label,
    height: tripetto_gutenberg_block.l10n_height,
    height_label: tripetto_gutenberg_block.l10n_height_label,
    placeholder: tripetto_gutenberg_block.l10n_placeholder,
    placeholder_label: tripetto_gutenberg_block.l10n_placeholder_label,
    placeholder_help: tripetto_gutenberg_block.l10n_placeholder_help,
    css: tripetto_gutenberg_block.l10n_css,
    css_label: tripetto_gutenberg_block.l10n_css_label,
    css_help: tripetto_gutenberg_block.l10n_css_help,
    css_placeholder: tripetto_gutenberg_block.l10n_css_placeholder,
    detach_form: tripetto_gutenberg_block.l10n_detach_form,
    help: tripetto_gutenberg_block.l10n_help,
};

export default l10n;
