declare const tripetto_gutenberg_block: {
    readonly base: string;
    readonly url: string;
    readonly language: string;
    readonly icon: string;
};

export const { base, url, language, icon } = tripetto_gutenberg_block;
