import { registerBlockType, BlockConfiguration } from "@wordpress/blocks";
import { IBlockProps } from "./props";
import block from "./block.json";
import l10n from "./l10n";
import icon from "./icon";
import edit from "./builder";
import "./gutenberg.scss";

registerBlockType<IBlockProps>("tripetto/form", {
    ...block,
    title: l10n.title,
    description: l10n.description,
    icon,
    edit,
    save: () => {
        return null;
    },
} as {} as BlockConfiguration<IBlockProps>);
