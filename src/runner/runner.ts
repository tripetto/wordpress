import {
    IDefinition,
    ISnapshot,
    TStyles,
    TL10n,
    L10n,
    Instance,
    Export,
    Import,
    castToNumber,
    castToString,
    checksum,
    each,
    findFirst,
    getAny,
    isFunction,
    isObject,
    map,
    powSolve,
    set,
    setAny,
} from "@tripetto/runner";
import { IRunnerAttachments } from "@tripetto/runner-react-hook";
import { CSSProperties } from "react";
import { getLocale, getTranslation } from "../builder/helpers/l10n";
import * as Superagent from "superagent";
import "promise-polyfill/src/polyfill";
import "../builder/helpers/constants";

declare const PACKAGE_LICENSE: string;

interface INamespace {
    readonly run: (props: {
        readonly license: string;
        readonly element?: HTMLElement;
        readonly definition?: IDefinition | Promise<IDefinition | undefined>;
        readonly snapshot?: ISnapshot | Promise<ISnapshot | undefined>;
        readonly styles?: TStyles | Promise<TStyles | undefined>;
        readonly l10n?: TL10n | Promise<TL10n | undefined>;
        readonly language?: string;
        readonly locale: (locale: "auto" | string) => Promise<L10n.ILocale | undefined>;
        readonly translations: (language: "auto" | string, context: string) => Promise<L10n.TTranslation | L10n.TTranslation[] | undefined>;
        readonly onImport?: (instance: Instance) => void;
        readonly onSubmit?: (
            instance: Instance,
            language: string,
            locale: string,
            namespace?: string
        ) => Promise<string | undefined> | boolean | void;
        readonly onPause?: {
            readonly recipe: "email";
            readonly onPause: (
                emailAddress: string,
                snapshot: ISnapshot,
                language: string,
                locale: string,
                namespace: string
            ) => Promise<void> | boolean | void;
        };
        readonly onReload?: () => Promise<IDefinition>;
        readonly onAction?: (
            event: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
            form: {
                readonly fingerprint: string;
                readonly name: string;
            },
            block?: {
                readonly id: string;
                readonly name: string;
            }
        ) => void;
        readonly display?: "page";
        readonly pausable?: boolean;
        readonly persistent?: boolean;
        readonly attachments?: IRunnerAttachments;
        readonly customCSS?: string;
        readonly customStyle?: CSSProperties;
        readonly className?: string;
    }) => void;
}

declare const TripettoAutoscroll: INamespace | undefined;
declare const TripettoChat: INamespace | undefined;
declare const TripettoClassic: INamespace | undefined;

type TLoader =
    | {
          readonly definition: IDefinition;
          readonly styles: TStyles;
          readonly l10n: TL10n;
          readonly snapshot: ISnapshot | undefined;
      }
    | undefined;

function getNamespace(runner: string): INamespace | undefined {
    switch (runner) {
        case "chat":
            return (typeof TripettoChat !== "undefined" && TripettoChat) || undefined;
        case "classic":
            return (typeof TripettoClassic !== "undefined" && TripettoClassic) || undefined;
        default:
            return (typeof TripettoAutoscroll !== "undefined" && TripettoAutoscroll) || undefined;
    }
}

function getRunner(runner: string): Promise<INamespace> {
    return new Promise((resolve: (namespace: INamespace) => void) => {
        const namespace = getNamespace(runner);

        if (!namespace) {
            const handle = setInterval(() => {
                const n = getNamespace(runner);

                if (n) {
                    clearInterval(handle);

                    resolve(n);
                }
            }, 100);
        } else {
            resolve(namespace);
        }
    });
}

function getStyles(styles: TStyles | undefined, pro: boolean | undefined): TStyles | undefined {
    if (styles && !pro) {
        set(styles, "noBranding", false);
    }

    return styles;
}

function getSnapshotToken(): string {
    if (window.location.search) {
        const n = window.location.search.indexOf("tripetto-resume=");

        if (n !== -1) {
            const reference = window.location.search.substr(n + 16, 64);

            if (reference.length === 64) {
                return reference;
            }
        }
    }

    return "";
}

export async function run(
    props: {
        readonly reference: string;
        readonly runner: string;
        readonly element: string;
        readonly uri: string;
        readonly url: string;
        readonly nonce: string;
        readonly version: string;
        readonly language?: string;
        readonly definition?: IDefinition;
        readonly snapshot?: ISnapshot;
        readonly snapshotToken?: string;
        readonly styles?: TStyles;
        readonly l10n?: TL10n;
        readonly fullPage?: boolean;
        readonly pausable?: boolean;
        readonly persistent?: boolean;
        readonly pro?: boolean;
        readonly width?: string;
        readonly height?: string;
        readonly css?: string;
        readonly className?: string;
        readonly data?: string;
        readonly prefill?: (Import.IFieldByKey | Import.IFieldByName)[];
    },
    trackers?: (() => (
        event: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
        form: {
            readonly reference: string;
            readonly runner: string;
            readonly fingerprint: string;
            readonly name: string;
        },
        block?: {
            readonly id: string;
            readonly name: string;
        }
    ) => void)[]
): Promise<void> {
    const url = `${props.uri}/tripetto.php?reference=${encodeURIComponent(props.reference)}`;
    const token = (props.pausable && (props.snapshotToken || getSnapshotToken())) || undefined;
    const nonce = props.nonce || "";
    const [definition, styles, l10n, snapshot] = (() => {
        if (!props.definition || !props.styles || !props.l10n || (!props.snapshot && token)) {
            const loader = new Promise<TLoader>((resolve: (data: TLoader) => void) => {
                const fnFallback = () => {
                    Superagent.post(props.url)
                        .type("form")
                        .send({
                            action: "tripetto_load",
                            reference: props.reference,
                            token: token || "",
                        })
                        .then((response) =>
                            resolve(
                                (response.ok &&
                                    response.body &&
                                    response.body.definition &&
                                    response.body.styles &&
                                    response.body.l10n && {
                                        definition: response.body.definition,
                                        styles: response.body.styles,
                                        l10n: response.body.l10n,
                                        snapshot: response.body.snapshot,
                                    }) ||
                                    undefined
                            )
                        )
                        .catch(() => resolve(undefined));
                };

                Superagent.get(url + (token ? `&token=${encodeURIComponent(token)}` : ""))
                    .send()
                    .then((response) => {
                        if (response.ok && response.body && response.body.definition && response.body.styles && response.body.l10n) {
                            resolve({
                                definition: response.body.definition,
                                styles: response.body.styles,
                                l10n: response.body.l10n,
                                snapshot: response.body.snapshot,
                            });
                        } else {
                            fnFallback();
                        }
                    })
                    .catch(() => fnFallback());
            });

            const definitionLoader =
                props.definition ||
                new Promise((resolve: (definition: IDefinition | undefined) => void) => loader.then((data) => resolve(data?.definition)));
            const stylesLoader =
                getStyles(props.styles, props.pro) ||
                new Promise((resolve: (styles: TStyles | undefined) => void) =>
                    loader.then((data) => resolve(getStyles(data?.styles, props.pro)))
                );
            const l10nLoader =
                props.l10n || new Promise((resolve: (l10n: TL10n | undefined) => void) => loader.then((data) => resolve(data?.l10n)));
            const snapshotLoader =
                props.snapshot ||
                new Promise((resolve: (snapshot: ISnapshot | undefined) => void) => loader.then((data) => resolve(data?.snapshot)));

            return [definitionLoader, stylesLoader, l10nLoader, snapshotLoader] as [
                typeof definitionLoader,
                typeof stylesLoader,
                typeof l10nLoader,
                typeof snapshotLoader
            ];
        }

        return [props.definition, getStyles(props.styles, props.pro), props.l10n, props.snapshot] as [
            typeof props.definition,
            typeof props.styles,
            typeof props.l10n,
            typeof props.snapshot
        ];
    })();

    if (props.data) {
        try {
            const variables = JSON.parse(atob(props.data));

            if (isObject(variables)) {
                const current = getAny(window, "TRIPETTO_CUSTOM_VARIABLES");

                setAny(window, "TRIPETTO_CUSTOM_VARIABLES", isObject(current) ? { ...current, ...variables } : variables);
            }
        } catch {}
    }

    (await getRunner(props.runner)).run({
        license: PACKAGE_LICENSE,
        element: document.getElementById(props.element) || undefined,
        definition,
        snapshot,
        language: props.language,
        l10n,
        styles,
        locale: (locale: "auto" | string) => getLocale(props.uri, props.url, locale),
        translations: (language: "auto" | string, context: string) => getTranslation(props.uri, props.url, language, context),
        display: (props.fullPage && "page") || undefined,
        persistent: props.persistent,
        className: props.className,
        customCSS: props.css,
        customStyle:
            props.width || props.height
                ? {
                      width: props.width,
                      height: props.height,
                  }
                : undefined,
        onImport:
            props.prefill &&
            ((instance) => {
                Import.fields(instance, props.prefill!);
            }),
        onSubmit: (instance: Instance, language: string, locale: string) =>
            new Promise((resolve: (id: string | undefined) => void, reject: (reason?: string) => void) => {
                const exportables = Export.exportables(instance);
                const actionables = Export.actionables(instance);

                /**
                 * If we have no fields or only fields without data, we don't need to
                 * post that data to the server. It's simply a form without data.
                 */
                if (!actionables && !findFirst(exportables.fields, (field) => (field.time ? true : false))) {
                    return resolve("");
                }

                const payloadChecksum = checksum({ exportables, actionables }, true);

                if (payloadChecksum) {
                    // First announce a new submission
                    Superagent.post(props.url)
                        .type("form")
                        .send({
                            action: "tripetto_announce",
                            reference: props.reference,
                            nonce,
                            fingerprint: exportables.fingerprint,
                            checksum: payloadChecksum,
                        })
                        .then((announcement) => {
                            // Then perform work and submit the nonce and payload.
                            if (announcement.status === 200 && announcement.body && announcement.body.id && announcement.body.difficulty) {
                                try {
                                    const id = castToString(announcement.body.id);
                                    const difficulty = castToNumber(announcement.body.difficulty);
                                    const timestamp = castToNumber(announcement.body.timestamp);

                                    Superagent.post(props.url)
                                        .type("form")
                                        .send({
                                            action: "tripetto_submit",
                                            id,
                                            nonce: powSolve(
                                                { exportables, actionables },
                                                difficulty,
                                                id,
                                                16,
                                                1000 * 60 * 5,
                                                timestamp,
                                                "seconds"
                                            ),
                                            language,
                                            locale,
                                            exportables: JSON.stringify(exportables),
                                            actionables: JSON.stringify(actionables),
                                            snapshot: token,
                                        })
                                        .then((result) => {
                                            if (result.ok && result.body && result.body.id) {
                                                resolve(result.body.id);
                                            } else {
                                                reject(
                                                    result.status === 403 ? "rejected" : (result.error && result.error.text) || undefined
                                                );
                                            }
                                        })
                                        .catch((result) => reject((result && result.status === 403 && "rejected") || undefined));
                                } catch {
                                    reject();
                                }
                            } else {
                                reject(
                                    announcement.status === 409 ? "outdated" : (announcement.error && announcement.error.text) || undefined
                                );
                            }
                        })
                        .catch((result) => reject((result && result.status === 409 && "outdated") || undefined));
                } else {
                    reject();
                }
            }),
        onPause:
            (props.pausable && {
                recipe: "email",
                onPause: (emailAddress: string, snapshot: ISnapshot, language: string, locale: string, runner: string) =>
                    new Promise<void>((resolve: () => void, reject: (reason?: string) => void) => {
                        Superagent.post(props.url)
                            .type("form")
                            .send({
                                action: "tripetto_pause",
                                reference: props.reference,
                                nonce,
                                url: window.location.href,
                                emailAddress,
                                snapshot: JSON.stringify(snapshot),
                                language,
                                locale,
                                runner,
                                token,
                            })
                            .then((res) => {
                                if (res.ok) {
                                    resolve();
                                } else {
                                    reject((res.error && res.error.text) || undefined);
                                }
                            })
                            .catch(() => reject());
                    }),
            }) ||
            undefined,
        onReload: () =>
            new Promise((resolve: (definition: IDefinition) => void, reject: () => void) => {
                const fnFallback = () => {
                    Superagent.post(props.url)
                        .type("form")
                        .send({
                            action: "tripetto_load",
                            reference: props.reference,
                            token: token || "",
                        })
                        .then((response) => {
                            if (response.ok && response.body && response.body.definition) {
                                resolve(response.body.definition);
                            } else {
                                reject();
                            }
                        })
                        .catch(() => reject());
                };

                Superagent.get(url)
                    .then((response) => {
                        if (response.ok && response.body && response.body.definition) {
                            resolve(response.body.definition);
                        } else {
                            fnFallback();
                        }
                    })
                    .catch(() => fnFallback());
            }),
        onAction:
            (trackers &&
                (() => {
                    const emitters = map(trackers, (tracker) => {
                        try {
                            if (isFunction(tracker)) {
                                const eventTracker = tracker();

                                if (isFunction(eventTracker)) {
                                    return eventTracker;
                                }
                            }
                        } catch (err) {
                            console.log("Tripetto: Invalid tracker code!");
                            console.log(err);
                        }

                        return undefined;
                    });

                    return (
                        event: "start" | "stage" | "unstage" | "focus" | "blur" | "pause" | "complete",
                        form: {
                            readonly fingerprint: string;
                            readonly name: string;
                        },
                        block?: {
                            readonly id: string;
                            readonly name: string;
                        }
                    ) =>
                        each(emitters, (tracker) => {
                            if (tracker) {
                                try {
                                    tracker(
                                        event,
                                        {
                                            reference: props.reference,
                                            runner: props.runner,
                                            ...form,
                                        },
                                        block
                                    );
                                } catch (err) {
                                    console.log("Tripetto: Error in tracker event handler code!");
                                    console.log(err);
                                }
                            }
                        });
                })()) ||
            undefined,
        attachments: {
            put: (file: File, onProgress?: (percentage: number) => void) =>
                new Promise((resolve: (reference: string) => void, reject: (reason?: string) => void) => {
                    const formData = new FormData();

                    formData.append("action", "tripetto_attachment_upload");
                    formData.append("file", file);
                    formData.append("reference", props.reference);
                    formData.append("nonce", nonce);

                    Superagent.post(props.url)
                        .send(formData)
                        .on("progress", (event: Superagent.ProgressEvent) => {
                            if (event.direction === "upload" && onProgress) {
                                onProgress(event.percent || 0);
                            }
                        })
                        .then((res: Superagent.Response) => {
                            if (res.ok && res.body && res.body.reference) {
                                resolve(res.body.reference);
                            } else {
                                reject(res.status === 413 ? "File is too large." : undefined);
                            }
                        })
                        .catch(() => reject());
                }),
            get: (file: string) =>
                new Promise<Blob>((resolve: (data: Blob) => void, reject: () => void) => {
                    Superagent.post(props.url)
                        .type("form")
                        .send({
                            action: "tripetto_attachment_download",
                            reference: props.reference,
                            nonce,
                            file,
                        })
                        .responseType("blob")
                        .then((res: Superagent.Response) => {
                            if (res.ok) {
                                resolve(res.body);
                            } else {
                                reject();
                            }
                        })
                        .catch(() => reject());
                }),
            delete: (file: string) => {
                return new Promise((resolve: () => void, reject: () => void) => {
                    Superagent.post(props.url)
                        .type("form")
                        .send({
                            action: "tripetto_attachment_unload",
                            reference: props.reference,
                            nonce,
                            file,
                        })
                        .then((res: Superagent.Response) => {
                            if (res.ok) {
                                resolve();
                            } else {
                                reject();
                            }
                        })
                        .catch(() => reject());
                });
            },
        },
    });
}
