import "./elementor.scss";

interface IWidget {
    readonly [index: number]: Element;
    readonly length: number;
}

declare const elementorFrontend:
    | {
          readonly hooks: {
              readonly addAction: (name: string, cb: (widget: IWidget) => void) => void;
          };
      }
    | undefined;

function bootstrap(element: Element | undefined): void {
    if (element) {
        let ref = element.firstElementChild;

        while (ref) {
            if (ref.id.indexOf("tripetto-runner-") === 0) {
                const signature = ref.id.replace(/\-/g, "_");

                // @ts-ignore
                if (typeof window[signature] === "function") {
                    // @ts-ignore
                    typeof window[signature]();
                }

                return;
            }

            bootstrap(ref);

            ref = ref.nextElementSibling;
        }
    }
}

jQuery(window).on("elementor/frontend/init", () => {
    if (typeof elementorFrontend !== "undefined") {
        elementorFrontend.hooks.addAction("frontend/element_ready/tripetto.default", (widget) => bootstrap(widget[0]));
    }
});
