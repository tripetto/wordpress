const fs = require("fs");

fs.mkdirSync("./dist/", { recursive: true });
fs.writeFileSync(".vagrant/Vagrantfile", fs.readFileSync(".vagrant/Vagrantfile", "utf8").replace("do |config|\n", "do |config|\n  config.vm.synced_folder \"../dist/\", \"/var/www/html/wp-content/plugins/tripetto\", owner: \"root\", group: \"root\"\n"), "utf8");
fs.writeFileSync(".vagrant/provision/default.yml", fs.readFileSync(".vagrant/provision/default.yml", "utf8").replace("memory: 1024", "memory: 3192"), "utf8");
