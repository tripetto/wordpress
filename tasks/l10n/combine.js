const fs = require("fs");
const path = require("path");
const prettier = require("prettier");
const tripetto = require("@tripetto/builder");
const package = require("../../package.json");

function combine(inputFolder, outputFolder, prefix, makeRegional) {
    if (fs.existsSync(inputFolder)) {
        const files = fs.readdirSync(inputFolder) || [];

        fs.mkdirSync(outputFolder, { recursive: true });

        prefix = prefix.replace("@tripetto/", "tripetto-");

        files.forEach(function (file) {
            if (fs.statSync(inputFolder + file).isFile()) {
                const isJSON = file.lastIndexOf(".json") === file.length - 5;

                if (isJSON) {
                    const inputFile = JSON.parse(fs.readFileSync(inputFolder + file, "utf8"));

                    if (inputFile && (inputFile[""] || (inputFile.length > 0 && inputFile[0][""]))) {
                        const language = path.basename(file, ".json");
                        const outputFile =
                            outputFolder +
                            (prefix ? prefix + "-" : "") +
                            language +
                            (makeRegional && language.indexOf("_") === -1 ? "_" + language.toUpperCase() : "") +
                            ".json";
                        const inputData =
                            typeof inputFile === "object" && "length" in inputFile && inputFile.length > 0 ? inputFile : [inputFile];

                        if (fs.existsSync(outputFile)) {
                            const length = inputData.length;
                            let outputData = JSON.parse(fs.readFileSync(outputFile, "utf8"));
                            let merged = 0;

                            for (let i = 0; i < inputData.length; i++) {
                                if (JSON.stringify(outputData).indexOf(JSON.stringify(inputData[i])) === -1) {
                                    merged++;

                                    if (typeof outputData === "object" && "length" in outputData && outputData.length > 0) {
                                        outputData.push(inputData[i]);
                                    } else {
                                        outputData = [outputData, inputData[i]];
                                    }
                                }
                            }

                            if (merged > 0) {
                                fs.writeFileSync(
                                    outputFile,
                                    prettier.format(JSON.stringify(outputData), {
                                        parser: "json",
                                    }),
                                    "utf8"
                                );

                                console.log(`combine: ${inputFolder + file} -> ${outputFile} (merged ${merged} out of ${length})`);
                            } else {
                                console.log(`combine: ${inputFolder + file} -> ${outputFile} (skipped, already merged)`);
                            }
                        } else {
                            fs.writeFileSync(
                                outputFile,
                                prettier.format(JSON.stringify(inputData.length === 1 ? inputData[0] : inputData), {
                                    parser: "json",
                                }),
                                "utf8"
                            );

                            console.log(`combine: ${inputFolder + file} -> ${outputFile} (created)`);
                        }
                    }
                }
            }
        });
    }
}

combine(`./node_modules/@tripetto/builder/translations/`, "./dist/languages/", "tripetto", true);

tripetto.each(
    tripetto.filter(Object.keys(package.devDependencies), (pkg) => /^\@tripetto\/runner-/.test(pkg)),
    (pkg) => {
        combine(`./node_modules/${pkg}/translations/`, "./dist/languages/", "tripetto", true);
        combine(`./node_modules/${pkg}/builder/translations/`, "./dist/languages/", "tripetto", true);
    }
);

tripetto.each(
    tripetto.filter(Object.keys(package.devDependencies), (pkg) => /^\@tripetto\/runner-/.test(pkg)),
    (pkg) => {
        combine(`./node_modules/${pkg}/runner/translations/`, `./dist/languages/`, pkg, false);
    }
);
