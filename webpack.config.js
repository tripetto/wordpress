const webpack = require("webpack");
const webpackCopy = require("copy-webpack-plugin");
const webpackTerser = require("terser-webpack-plugin");
const webpackReplace = require("replace-in-file-webpack-plugin");
const webpackExtract = require("mini-css-extract-plugin");
const webpackShell = require("webpack-shell-plugin-next");
const svgToMiniDataURI = require("mini-svg-data-uri");
const path = require("path");
const pkg = require("./package.json");
const production = process.argv.includes("production");
const analyzer = require("webpack-bundle-analyzer").BundleAnalyzerPlugin;

require("dotenv").config();

const config = (type) => {
    return {
        target: ["web", "es5"],
        entry: `./src/${type}/${type}.ts`,
        output: {
            filename: `wp-tripetto${type !== "admin" ? "-" + type : ""}.js`,
            path: path.resolve(__dirname, "dist", "js"),
            ...(type !== "gutenberg" && type !== "elementor"
                ? {
                      library: {
                          name: `WPTripetto${type !== "admin" ? type.substr(0, 1).toUpperCase() + type.substr(1) : ""}`,
                          type: "umd",
                          umdNamedDefine: true,
                      },
                  }
                : undefined),
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: "ts-loader",
                    options: {
                        configFile: "tsconfig.json",
                        compilerOptions: {
                            noEmit: false,
                            noEmitHelpers: type !== "gutenberg" && type !== "elementor",
                            importHelpers: type !== "gutenberg" && type !== "elementor",
                        },
                    },
                },
                {
                    test: /\.svg$/,
                    use: [
                        {
                            loader: "url-loader",
                            options: {
                                generator: (content) => svgToMiniDataURI(content.toString()),
                            },
                        },
                        {
                            loader: "image-webpack-loader",
                        },
                    ],
                },
                ...(type === "admin" || type === "gutenberg" || type === "elementor"
                    ? [
                          {
                              test: /\.s[ac]ss$/i,
                              use: [
                                  {
                                      loader: webpackExtract.loader,
                                  },
                                  {
                                      loader: "css-loader",
                                      options: {
                                          esModule: false,
                                          url: {
                                              filter: (url) => !url.includes("/fonts/"),
                                          },
                                      },
                                  },
                                  {
                                      loader: "sass-loader",
                                  },
                              ],
                          },
                      ]
                    : []),
            ],
        },
        resolve: {
            extensions: [".ts", ".tsx", ".js"],
        },
        externals: {
            "@tripetto/builder": "Tripetto",
            "@tripetto/runner": "TripettoRunner",
            "@tripetto/runner-autoscroll": "TripettoAutoscroll",
            "@tripetto/runner-chat": "TripettoChat",
            "@tripetto/runner-classic": "TripettoClassic",
            ...(type === "gutenberg"
                ? {
                      react: "React",
                      "@wordpress/blocks": "wp.blocks",
                      "@wordpress/block-editor": "wp.blockEditor",
                      "@wordpress/components": "wp.components",
                      "@wordpress/i18n": "wp.i18n",
                  }
                : undefined),
            ...(type === "elementor"
                ? {
                      jquery: "jQuery",
                  }
                : undefined),
        },
        performance: {
            hints: false,
        },
        optimization: {
            minimizer: [
                new webpackTerser({
                    terserOptions: {
                        format: {
                            preamble: `/*! ${pkg.title} ${pkg.version} */`,
                            comments: false,
                        },
                    },
                    extractComments: false,
                }),
            ],
        },
        plugins: [
            new webpack.DefinePlugin({
                PACKAGE_NAME: JSON.stringify(pkg.name),
                PACKAGE_VERSION: JSON.stringify(pkg.version),
                PACKAGE_LICENSE: JSON.stringify(process.env.TRIPETTO_LICENSE || ""),
            }),
            ...(type === "admin"
                ? [
                      new webpackShell({
                          onBuildStart: {
                              scripts: ["npm run l10n"],
                              blocking: true,
                              parallel: false,
                          },
                      }),
                      new webpackExtract({
                          filename: `../css/wp-tripetto.css`,
                      }),
                      new webpackCopy({
                          patterns: [
                              {
                                  from: "./src/**/*.+(php|json|txt)",
                                  to({ absoluteFilename }) {
                                      return path.join(
                                          __dirname,
                                          "dist",
                                          absoluteFilename.substr(absoluteFilename.replace("\\src\\", "/src/").lastIndexOf("/src/") + 5)
                                      );
                                  },
                              },
                              {
                                  from: "./dependencies/freemius",
                                  to: "../freemius",
                                  globOptions: {
                                      ignore: [
                                          "**/.git*",
                                          "**/.github/**",
                                          "**/*.pot",
                                          "**/*.yml",
                                          "**/composer.json",
                                          "**/gulpfile.js",
                                          "**/LICENSE.txt",
                                          "**/package.json",
                                          "**/README.md",
                                      ],
                                  },
                              },
                              {
                                  from: "./src/assets/",
                                  to: "../assets/",
                              },
                              {
                                  from: "./translations/",
                                  to: "../languages/",
                              },
                              {
                                  from: "./src/fonts/**/*.+(woff|woff2|ttf)",
                                  to({ absoluteFilename }) {
                                      return path.join(
                                          __dirname,
                                          "dist",
                                          absoluteFilename.substr(absoluteFilename.replace("\\src\\", "/src/").lastIndexOf("/src/") + 5)
                                      );
                                  },
                              },
                              {
                                  from: "node_modules/@tripetto/builder/fonts/",
                                  to: "../fonts/",
                              },
                              {
                                  from: "node_modules/@tripetto/builder/locales/",
                                  to: "../locales/",
                              },
                              {
                                  from: "node_modules/@tripetto/builder/runtime/tripetto-umd.js",
                                  to: "../vendors/tripetto-builder.js",
                              },
                              {
                                  from: "node_modules/@tripetto/runner/dist/umd/index.js",
                                  to: "../vendors/tripetto-runner.js",
                              },
                              {
                                  from: "node_modules/@tripetto/runner-autoscroll/runner/index.js",
                                  to: "../vendors/tripetto-runner-autoscroll.js",
                              },
                              {
                                  from: "node_modules/@tripetto/runner-chat/runner/index.js",
                                  to: "../vendors/tripetto-runner-chat.js",
                              },
                              {
                                  from: "node_modules/@tripetto/runner-classic/runner/index.js",
                                  to: "../vendors/tripetto-runner-classic.js",
                              },
                              {
                                  from: "node_modules/@tripetto/runner-autoscroll/builder/index.js",
                                  to: "../vendors/tripetto-builder-autoscroll.js",
                              },
                              {
                                  from: "node_modules/@tripetto/runner-chat/builder/index.js",
                                  to: "../vendors/tripetto-builder-chat.js",
                              },
                              {
                                  from: "node_modules/@tripetto/runner-classic/builder/index.js",
                                  to: "../vendors/tripetto-builder-classic.js",
                              },
                              {
                                  from: "./src/admin/templates/**/thumbnail.jpg",
                                  to({ absoluteFilename }) {
                                      return path.join(
                                          __dirname,
                                          "dist",
                                          absoluteFilename.substr(absoluteFilename.replace("\\src\\", "/src/").lastIndexOf("/src/") + 5)
                                      );
                                  },
                              },
                          ],
                      }),
                      new webpackReplace([
                          {
                              dir: "dist",
                              files: ["plugin.php", "readme.txt", "lib/database.php"],
                              rules: [
                                  {
                                      search: "{{ NAME }}",
                                      replace: "Tripetto",
                                  },
                                  {
                                      search: "{{ TITLE }}",
                                      replace: pkg.title,
                                  },
                                  {
                                      search: "{{ DESCRIPTION }}",
                                      replace: pkg.description,
                                  },
                                  {
                                      search: /\{\{\sVERSION\s\}\}/gi,
                                      replace: pkg.version,
                                  },
                                  {
                                      search: "{{ HOMEPAGE }}",
                                      replace: pkg.homepage,
                                  },
                                  {
                                      search: "{{ KEYWORDS }}",
                                      replace: (pkg.keywords || []).join(", "),
                                  },
                                  {
                                      search: `"secret_key" => ""`,
                                      replace: `"secret_key" => '${process.env.FREEMIUS_SECRET_KEY || ""}'`,
                                  },
                              ],
                          },
                      ]),
                  ]
                : []),
            ...(type === "gutenberg" || type === "elementor"
                ? [
                      new webpackExtract({
                          filename: `../css/wp-tripetto-${type}.css`,
                      }),
                  ]
                : []),
            ...(production
                ? [
                      new analyzer({
                          analyzerMode: "static",
                          reportFilename: `../../reports/bundle-${type}.html`,
                          openAnalyzer: false,
                      }),
                  ]
                : []),
        ],
    };
};

module.exports = () => {
    return [config("admin"), config("builder"), config("runner"), config("gutenberg"), config("elementor")];
};
