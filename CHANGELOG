✔️ New feature
⚡ Improvement
🐛 Bugfix
❌ Deprecated or removed feature

**VERSION 8.0.10 (05-03-2025)**
🐛 Fixed a security issue in the results list

**VERSION 8.0.9 (30-01-2025)**
⚡ Automatic generation of an index file in the upload folder of Tripetto to prevent directory listing on servers that have enabled directory listing

**VERSION 8.0.8 (27-01-2025)**
⚡ Tested compatibility with WordPress 6.7.1
🐛 Fixed a security issue in the upload block

**VERSION 8.0.7 (26-11-2024)**
🐛 Fixed a security issue in the upload block

**VERSION 8.0.5 (05-11-2024)**
⚡ Tested compatibility with WordPress 6.7

**VERSION 8.0.3 (06-06-2024)**
🐛 Fixed a performance issue in the multi-select dropdown block

**VERSION 8.0.1 (19-03-2024)**
✔️ Added a signature block (see https://tripetto.com/blog/product-update-new-signature-block/)
⚡ Made domain validation (URL validation) less strict so that local domains, IP addresses, and relative paths are now allowed
⚡ Added the alias of a block to the tracking functionality
⚡ Added a live preview of uploaded images in the result panel
⚡ Tested compatibility with WordPress 6.5
🐛 Fixed a bug in the email address field of the notification panel

**VERSION 7.2.2 (15-01-2024)**
⚡ Tested compatibility with WordPress 6.4.2
🐛 Fixed a bug in the calculator block when calculating scores for individual rows in a matrix block
🐛 Fixed missing translations for certain blocks in the builder

**VERSION 7.2.1 (14-11-2023)**
🐛 Fixed a bug in the locking/unlocking functionality (when used to mark fields as read-only)

**VERSION 7.2.0 (06-11-2023)**
✔️ Added a setting to hide sections after a (required) block that fails validation (this was the default behavior prior to version 7.1.0)
⚡ Improved the zoom functionality of the builder when working with very large forms
⚡ Tested compatibility with WordPress 6.4
🐛 Fixed an issue where it was not possible to complete a form in some specific cases
🐛 Fixed a backward compatibility issue in the hidden-field block

**VERSION 7.1.0 (12-10-2023)**
✔️ New version of the Autoscroll runner that is capable of showing blocks (questions) past a required block that is left unanswered (this also greatly improves the progress bar behavior)
✔️ Added a display value that will be used when recalling values in a form (this ensures that not the optional alias is shown, but the display-friendly value or name)
✔️ Added hyperlink support to the ranking block
⚡ Tested compatibility with WordPress 6.3.1
⚡ Improved compatibility of the Gutenberg block with older WordPress versions
🐛 Fixed a bug in the Autoscroll runner where the welcome or closing screen of a form could become unresponsive
🐛 Fixed Firefox drag-and-drop issue in ranking block

**VERSION 7.0.2 (24-07-2023)**
🐛 Fixed a bug in the Autoscroll runner where a form was not shown in some cases due to problems with loading images used in the form

**VERSION 7.0.1 (05-07-2023)**
✔️ Added a ranking block (see https://tripetto.com/blog/product-update-the-often-requested-ranking-block/)
⚡ Add file name as querystring parameter to file upload URLs (allows to extract the original file name of an upload)
⚡ Improved protection for data values that are automatically generated (these values can no longer be changed with, for example, a setter block)

**VERSION 6.0.2 (30-05-2023)**
🐛 Fixed a bug where file uploads could not be downloaded
🐛 Fixed a bug in the mailer block

**VERSION 6.0.0 (23-05-2023)**
✔️ Forms-in-forms support (subforms) that allows for improved organization and structuring of large forms (only available in the pro version)
✔️ Allow the use of `mailto:` and `tel:` hyperlinks in buttons (works in the multiple choice block, picture choice block, and in closing messages)
✔️ Added technical documentation for using hooks and filters in Tripetto (https://tripetto.com/sdk/docs/applications/wordpress/api/)
⚡ Tested compatibility with WordPress 6.2
⚡ Update help URLs to the new Tripetto website

**VERSION 5.7.7 (03-03-2023)**
🐛 Fixed a bug in the text condition evaluation in the following blocks: calculator, evaluate, hidden field, text (single line), text (multiple lines), and variable

**VERSION 5.7.6 (23-12-2022)**
🐛 Fixed a small bug in the multiple choice block
🐛 Fixed a bug in the form duplication function

**VERSION 5.7.5 (21-12-2022)**
🐛 Fixed a bug in the Classic runner where the height of the form was incorrect

**VERSION 5.7.4 (22-11-2022)**
⚡ Tested compatibility with WordPress 6.1.1
🐛 Fixed a timezone-related bug in the date/time selection control in the builder

**VERSION 5.7.3 (17-11-2022)**
🐛 Fixed a bug in the pause function of the Autoscroll runner

**VERSION 5.7.2 (07-11-2022)**
🐛 Fixed a bug in the multi-select dropdown block where condition labels were missing

**VERSION 5.7.1 (28-10-2022)**
⚡ Added a dataset parameter to the `tripetto_webhook` filter
🐛 Fixed a bug where the preview panel stopped working
🐛 Fixed a bug where an empty option in a dropdown block could be selected as the default option (when no placeholder was defined)

**VERSION 5.7.0 (14-10-2022)**
⚡ Improved the WordPress hooks and filters
🐛 Fixed a bug where prefilling a dropdown block using a query string sometimes didn't work as expected
🐛 Fixed missing translations for the multi-select dropdown block

**VERSION 5.6.2 (06-10-2022)**
🐛 Fixed a bug where the plugin could not be activated in certain conditions

**VERSION 5.6.1 (23-09-2022)**
🐛 Fixed a bug where scores for the matrix block were not calculated properly

**VERSION 5.6.0 (21-09-2022)**
✔️ Added color picker to the form styles editor
✔️ Added color setting for multiple-choice, picture-choice, and yes/no buttons
⚡ Added a message when closing the form builder while there is unsaved data
⚡ Tested compatibility with WordPress 6.0.2
🐛 Fixed a bug where HTML characters were not displayed correctly on the results page

**VERSION 5.5.0 (02-08-2022)**
✔️ Added multi-select dropdown block
✔️ Added the ability to add branches without sections (right-click or hold the `+` button in the builder and select `Insert branch` to directly add a new branch)
⚡ Added right-click mouse support in the builder (you can now right-click on any item to reveal the context menu with additional options)
🐛 Fixed a bug where tracking events would not emit when using a global Google Analytics tracker code

**VERSION 5.4.0 (27-05-2022)**
⚡ Added more alignment options to the multiple-choice block
⚡ Tested compatibility with WordPress 6.0
⚡ Improved the `tripetto_submit` hook so it better handles errors in the custom hook code
🐛 Fixed a bug where the CSV export returned a cached version when a certain server-side cache configuration is enabled

**VERSION 5.3.2 (28-02-2022)**
🐛 Fixed a bug where assets for the Gutenberg block were loaded when not needed

**VERSION 5.3.1 (25-02-2022)**
⚡ Migrate Integromat to Make
⚡ Tested compatibility with WordPress 5.9.1
🐛 Fixed a bug in the attachment download service

**VERSION 5.3.0 (17-02-2022)**
✔️ Added a build new form page that includes a gallery of form templates to choose from
✔️ Added an option to store a concatenated text with all the selected options in the dataset (available for the checkboxes, multiple-choice, and picture-choice blocks)
✔️ Added Google Tag Manager support for form tracking
✔️ Added an option to configure trackers to use global tracker codes when embedding forms
✔️ Added the possibility to retrieve a result overview page by result reference (using the `reference` query string parameter)
✔️ Added a WordPress filter hook `tripetto_prefill` that allows programmatically prefilling data in forms
⚡ Improved the exportability feature to make it clearer what this feature does
⚡ Tested compatibility with WordPress 5.9
🐛 Fixed submission issues when using Safari with response data that contains combining diacritical marks
🐛 Fixed a bug where icons in buttons would turn the wrong color when hovering over the button
🐛 Fixed a bug in the required feature of the matrix block

**VERSION 5.2.0 (22-11-2021)**
✔️ Added limits feature (minimum/maximum text length) to multi-line text
🐛 Fixed an XSS vulnerability when uploading malicious SVG files (thanks to thiennv/Patchstack for reporting this issue)

**VERSION 5.1.5 (03-11-2021)**
⚡ Update dashboard

**VERSION 5.1.4 (21-10-2021)**
⚡ Improved retry mechanism and error handling when saving forms (reduces issues with unstable hosts)
⚡ Improved chat button visibility for small devices

**VERSION 5.1.3 (18-10-2021)**
🐛 Fixed a bug in the conflict detection component

**VERSION 5.1.2 (14-10-2021)**
🐛 Fixed a bug where the builder stopped working when an evaluate block was added after a cluster without any nodes

**VERSION 5.1.1 (23-09-2021)**
⚡ Improved the onboarding tutorial with instruction videos about using the Gutenberg block and the Elementor widget

**VERSION 5.1.0 (23-09-2021)**
✔️ Added a widget for Elementor (see https://tripetto.com/blog/another-update-tripetto-as-elementor-widget/)
🐛 Fixed a bug where the builder could not load on hosts where mbstring support for PHP was not enabled

**VERSION 5.0.1 (16-09-2021)**
✔️ Added a full-featured Gutenberg block for Tripetto (see https://tripetto.com/blog/major-update-tripetto-as-gutenberg-block/)
✔️ Added the possibility to customize the columns of the result list
⚡ Added protection to detect conflicts when working with multiple users (or browser tabs/windows) on the same form
⚡ Improved the header bar of the list views
⚡ Added a style setting to the runners to customize the font size for small screens (mobile devices)
⚡ Made the `tripetto_create_forms` capability dependent on the `tripetto_edit_forms` capability (so a user needs both capabilities to be able to create new forms)
🐛 Fixed a bug where pausing the form with an invalid email address would freeze the form
🐛 Fixed a bug where the builder could not load when a malformed custom tracking code was supplied

**VERSION 4.2.4 (07-09-2021)**
🐛 Fixed a bug in the address validation of the email block
🐛 Fixed a bug in the calculator where an outcome was not always available immediately
🐛 Fixed a bug where duplicate fields were shown in the classic runner
🐛 Fixed a bug in the download handler of uploaded files
🐛 Fixed a bug where the headers of CSV files were missing when using PHP <= 5.6
🐛 Fixed a bug in the custom WordPress hook (`tripetto_submit`)

**VERSION 4.2.3 (04-08-2021)**
🐛 Fixed missing information in the dashboard

**VERSION 4.2.2 (03-08-2021)**
🐛 Fixed a bug in the email notification service (messages were not sent in some cases)

**VERSION 4.2.1 (29-07-2021)**
✔️ Added a WordPress filter hook `tripetto_webhook` that allows altering data that is sent to the custom webhook
🐛 Fixed a bug in the WordPress action hook implementation where the wrong data was supplied to the hook

**VERSION 4.2.0 (29-07-2021)**
✔️ Added tracking support for Google Analytics, Facebook Pixel or a custom tracker code (see https://tripetto.com/blog/new-feature-form-activity-tracking-with-google-analytics-and-facebook-pixel/)
✔️ Added Zapier feature to the connections panel
✔️ Added Integromat feature to the connections panel
✔️ Added Pably Connect feature to the connections panel
✔️ Added a WordPress filter hook `tripetto_styles` that allows altering style settings of forms
✔️ Added French translation (🙏 Benjamin COUEDEL)
⚡ Improved the automate menu (there are now separate panels for managing notifications, connections and tracking)
⚡ Improved trackpad support for the form builder (now supports panning in all directions)
⚡ Improved test function for Slack notifications and webhooks (fields of the form are now submitted as test data)
🐛 Fixed an issue where settings in the automate panels would not be saved

**VERSION 4.1.1 (23-07-2021)**
✔️ Added limiting feature to limit the number of selected options/answers (available for the checkboxes, multiple choice, and picture choice blocks)
✔️ Added randomization feature (available for the dropdown, checkboxes, multiple choice, picture choice, and radio buttons blocks)
✔️ Added counter slot that counts the number of selected options/answers (available for the checkboxes, multiple choice, and picture choice blocks)
🐛 Fixed a compatibility issue with Happy Addons for Elementor (see https://wordpress.org/support/topic/add_menu_page-breaking-other-plugins/)
🐛 Fixed a bug where recalling values of other blocks did not work properly for specific cases
🐛 Fixed a bug where administrators in multisite installations could not access Tripetto
🐛 Fixed a bug where style changes in the chat form were not applied to the live preview panel

**VERSION 4.1.0 (15-07-2021)**
✔️ Added WordPress variables to the hidden field block (see https://tripetto.com/blog/new-feature-instantly-use-wordpress-variables-in-your-forms/)
🐛 Fixed a bug where the builder screen was shown behind the WordPress menu
🐛 Fixed a bug where branch iterations would sometimes not display correctly
🐛 Fixed a bug where conflicts occurred when the same form was used multiple times on the same page
🐛 Fixed a bug where activation of both the free and pro version of the plugin could result in a plugin activation error

**VERSION 4.0.4 (08-07-2021)**
🐛 Fixed a bug where some users could not activate the plugin properly

**VERSION 4.0.3 (06-07-2021)**
🐛 Fixed a compatibility issue with WordPress 5.0 or lower
🐛 Fixed a bug in the pause and resume function

**VERSION 4.0.1 (01-07-2021)**
✔️ Implemented roles and capabilities support
✔️ Added a settings page to manage global settings for Tripetto
✔️ Added a setting to disable the SPAM-protection system
✔️ Added an IP address allowlist for the SPAM-protection system
✔️ Added a setting to set the sender address for emails sent by Tripetto
✔️ Added an onboarding wizard to help configuring and using the plugin
✔️ Added new branch mode for checking if none of the conditions match (logical NOT)
✔️ Added automatic video playback and pausing
✔️ Added a new constant to the calculator to find the current branch index number
⚡ Improved the webhook (it now allows URLs with a username and password in it)
⚡ Renamed the premium plan to pro
⚡ Improved block type selection when adding a new block
⚡ Added a style setting to remove the asterisk for required blocks (only applies to the autoscroll and classic runner)
⚡ Improved variables support in URLs so you can use a variable to specify the protocol and domain part of an URL
⚡ Improved the multiple choice and picture choice blocks so you can select a hyperlink target for URL choices
⚡ Improved dashboard with help articles and video tutorials
🐛 Fixed a typo in the mailer block

**VERSION 3.5.1 (31-05-2021)**
⚡ Improved the URL block
🐛 Fixed a data regression bug causing some forms to not work properly

**VERSION 3.5.0 (27-05-2021)**
✔️ Added settings to the automate panel to make uploaded files in forms directly accessible for webhooks and Slack/email recipients
🐛 Fixed a bug in the closing message of the classic runner
🐛 Fixed a bug in the capitalize function (see https://gitlab.com/tripetto/studio/-/issues/40)

**VERSION 3.4.3 (20-05-2021)**
✔️ Added count occurrences function to the calculator block
✔️ Added concatenate option to the set value block
✔️ Added an option to disable scrolling in the autoscroll runner
✔️ Implemented Trusted Types support (see https://web.dev/trusted-types/)
✔️ Added German translation for the runners (form faces)
✔️ Added French translation for the runners (form faces)
✔️ Added Spanish translation for the runners (form faces)
✔️ Added Portuguese translation for the runners (form faces)
✔️ Added Indonesian translation (🙏 https://gitlab.com/hisamafahri)
✔️ Added additional alias options to the yes/no block
⚡ Improved performance for very large forms
⚡ Improved the live preview of the classic runner
⚡ Improved usability of the set value block
⚡ Improved the icons on the dashboard page
⚡ Improved the size of the plugin (reduced the plugin ZIP file by 1.38Mb)
🐛 Fixed a bug in the number field where the formatting was not applied
🐛 Fixed a bug in the calculator feature of the number block
🐛 Fixed a bug in the classic runner where sections with no blocks (or only invisible blocks) caused paginated forms to stop working
🐛 Fixed a bug with the submit button in the multiple choice and picture choice block
🐛 Fixed a bug in the translation system where the wrong translation was used
🐛 Fixed the date/time in the list of forms and results
🐛 Fixed an accessibility issue with the dropdown lists in the runners (form faces)

**VERSION 3.3.1 (17-02-2021)**
✔️ Added an import/export function to allow easy adding of multiple items at once (for example, dropdown options, multiple choice buttons, text suggestions, etc.)
✔️ Added a custom variable block that allows the use of custom variables
✔️ Added a set value block that allows to set field values or other variables
✔️ Added suggestions support in the single line text block so you can specify a list of pre-defined options for the text input
✔️ Added a score feature to blocks so you can calculate scores directly without the need of a separate calculator block (this works for the blocks checkbox, checkboxes, dropdown, matrix, multiple choice, picture choice, radiobuttons, scale, single line text and yes/no)
✔️ Added a calculator option to the number block, so you can make successive calculations directly without the need of a separate calculator block
✔️ Added a prefill setting to the number, single line text and multiple lines text blocks so you can set an initial value for those fields (if you want to set an initial value for other blocks, use the new set value block)
✔️ Added a minimum required text length option to the single line text block
⚡ Enabled a scrollbar for menus with lots of options (you can grab the scrollbar to quickly scroll through all items in the menu)
⚡ Changed the initial behavior of a subcalculation within a calculator block: The initial value is now set to the last answer (ANS) of the parent calculation instead of an empty value (please check your form if you are using subcalculations as this change might break things for you)
⚡ Improved the calculator behavior when using variables that have no value yet as input (this will only lead to an invalid calculation when that variable is used as initial input, for multiplication, or for division, otherwise the variable input will be considered 0)
🐛 Fixed a keyboard bug in MacOS Safari where the text cursor sometimes jumped to end of a line (https://gitlab.com/tripetto/studio/-/issues/28)
🐛 Fixed the mobile keyboard for numeric fields with decimal precision
🐛 Fixed a bug where the label of a node was not displayed properly (https://gitlab.com/tripetto/studio/-/issues/31)
🐛 Fixed a bug in the allowed file types setting of the file upload block
🐛 Fixed a bug in the closing message where sometimes the wrong message was shown
🐛 Fixed a bug in the radio button block in the chat runner
🐛 Fixed a query string bug in Internet Explorer
🐛 Fixed a bug where downloads would not work when wp-admin lives in a sub folder
🐛 Fixed a bug where in some specific cases a form could not be submitted
🐛 Fixed a compatibility bug with PHP <= 5.6

**VERSION 3.2.0 (12-01-2021)**
✔️ Added a calculator block that allows (advanced) calculations inside forms (see https://tripetto.com/calculator/)
✔️ Added a stop block that can be used to prevent completion of a form
⚡ Improved condition logic (you can now use values of other blocks)
⚡ Improved switching between block types (more block settings are now retained)
⚡ Improved markdown support for checkboxes and radio buttons
⚡ Implemented variables support in dropdown options, multiple choice items and picture choice items
⚡ Implemented an option to set readable labels for boolean (true/false) values
🐛 Fixed an alignment bug in the mobile view of the Autoscroll runner
🐛 Fixed the usage of variables inside image/video URLs
🐛 Fixed a bug where the identifier of a form response was not available in the closing message of nested branches
🐛 Fixed a bug where an illegal custom font name would lead to a runner fault
🐛 Fixed a bug in the file upload condition block

**VERSION 3.1.13 (15-12-2020)**
⚡ Tested compatibility with WordPress 5.6 and PHP 8
🐛 Fixed an issue with headers already sent

**VERSION 3.1.12 (09-12-2020)**
🐛 Fixed a bug in the mail notification service

**VERSION 3.1.11 (07-12-2020)**
🐛 Fixed a bug in the result viewer

**VERSION 3.1.10 (03-12-2020)**
🐛 Fixed a bug in duplicate form function
🐛 Fixed a bug in redirect to another URL after form completion

**VERSION 3.1.8 (23-11-2020)**
🐛 Fixed a problem with (Cloudflare) caching (added a `Cache-Control` header)

**VERSION 3.1.7 (18-11-2020)**
✔️ Added shortcode attribute `placeholder` to define a loader message (this message is shown while the form is loading)
⚡ Improved the shortcode editor so the loader message can be managed
🐛 Fixed a bug where a form could not load due to insufficient permissions of the plugin's own AJAX handler

**VERSION 3.1.6 (17-11-2020)**
⚡ Improved form loading performance (especially on slow-hosted websites)

**VERSION 3.1.5 (13-11-2020)**
✔️ Added shortcode attribute `async` to control the loading method of forms inside pages
🐛 Fixed a bug in branch iteration logic
🐛 Fixed a bug in scale block

**VERSION 3.1.4 (04-11-2020)**
🐛 Fixed a bug in the translation editor
🐛 Fixed a bug in the SPAM-protection system on 32-bit legacy systems

**VERSION 3.1.3 (21-10-2020)**
⚡ Improved compatibility with cache plugins
⚡ Improved error messages
🐛 Fixed a bug where the Tripetto branding was still visible in the email messages sent by the plugin
🐛 Fixed a bug where database migrations (from old plugin version a newer one) sometimes resulted in an error
🐛 Fixed a bug related to font ligatures

**VERSION 3.1.2 (14-10-2020)**
🐛 Fixed a bug where the Hindi keyboard on iOS could not be used properly

**VERSION 3.1.1 (13-10-2020)**
🐛 Fixed compatibility with 32-bit legacy systems (fixes a problem with the SPAM-protection system not working correctly on PHP without 64-bit support)

**VERSION 3.1.0 (10-10-2020)**
✔️ Added picture choice block
✔️ Added scale block
⚡ Added more shapes to the rating block
⚡ Improved rating block conditions
⚡ Improved URL block (if the protocol prefix is missing, it is added automatically on blur)
⚡ Allow relative URLs for images and fonts
⚡ Switched to RFC5322 for email validation (which is more strict)
⚡ Auto-focus is now set to the selected button/option whenever possible
🐛 Fixed a bug in the web font loader

**VERSION 3.0.7 (06-10-2020)**
🐛 Fixed bug in chat window positioning
🐛 Fixed compatibility with MySQL versions lower than 5.6.5

**VERSION 3.0.6 (05-10-2020)**
🐛 Fixed bug in mobile responsiveness (see https://gitlab.com/tripetto/wordpress/-/issues/88)
🐛 Fixed compatibility with PHP 5.6

**VERSION 3.0.5 (02-10-2020)**
🐛 Improved compatibility with W3 Total Cache
🐛 Improved compatibility with Autoptimize
🐛 Fixed a bug where the font `Fira Sans` could not be used in Firefox
🐛 Added missing label in translation settings for the autoscroll runner

**VERSION 3.0.4 (30-09-2020)**
✔️ Added a style option to set the opacity of the background image in the runners
⚡ Improved the multiple choice buttons (they now wrap to the next line when necessary)
⚡ Automatically remove empty checkboxes, multiple choice buttons and radio buttons when in live or test mode (preview mode still shows placeholders for empty options)
🐛 Fixed a bug in the SPAM-protection system (this solves submission errors as reported by some of our users)
🐛 Fixed the z-index of the chat runner in inline mode
🐛 Fixed a compatibility issue with the WP AMP plugin
🐛 Fixed a bug where the Tripetto branding was not removed for free premium forms

**VERSION 3.0.3 (18-09-2020)**
🐛 Fixed bug in migration component

**VERSION 3.0.2 (18-09-2020)**
✔️ Added autoscroll runner
✔️ Added chat runner
✔️ Added classic runner
✔️ Added custom welcome message support
✔️ Added custom closing message support (supports alternative closing messages per branch)
✔️ Added URL redirect support
✔️ Added pause and resume support
✔️ Added standalone mode (allows forms to run standalone without the need of using a shortcode on a page; your WordPress instance is now a full-blown survey tool!)
✔️ Added date/time block
✔️ Added telephone block
✔️ Added error action block
✔️ Added evaluate condition block
✔️ Added regex condition block
✔️ Added translation support for static labels/texts in forms
✔️ Added identification number of each result to the results list
✔️ Added shortcode editor for easy configuration of shortcodes
✔️ Added persistent mode (this allows forms to maintain their state/session between page navigation)
✔️ Added dutch plugin translation
✔️ Added links to help articles
✔️ Added custom hooks to catch form submissions (`tripetto_submit`) and pause requests (`tripetto_pause`)
⚡ Upgraded builder to latest and greatest version (lots of improvements under the hood there; too much to list here)
⚡ Migrated rolling collector to autoscroll runner
⚡ Migrated classic bootstrap collector to classic runner
⚡ Improved mailer block (add reply-to header support and option to include all data)
⚡ Improved styling options to allow way more customizations
⚡ Improved SPAM-protection (no need for CAPTCHAs in Tripetto, we have a different mechanism to fight form spamming)
⚡ Improved automation tests
⚡ Improved shortcodes for better support of multiple forms on a single page
⚡ Improved inline behavior of forms (no more style conflicts)
⚡ You can now customize the styles of all your forms (in previous versions only premium forms could be customized)
⚡ Changed the CSV delimiter to semicolon instead of comma (better for Excel and the behavior now aligns with the Tripetto Studio)
⚡ Changed the CSV sorting to descending (the behavior now aligns with the Tripetto Studio)
⚡ Renamed entries to results (to align the terms used with Tripetto Studio)
🐛 Fixed a bug in the validation of empty number fields
🐛 Fixed a bug while filling out number fields in Firefox browser
🐛 Fixed the unwanted behavior of the first radio button getting selected for required radio button questions
🐛 Fixed a bug on Android devices having difficulty with showing the soft-keyboard while filling out the form
🐛 Fixed the unwanted ability to create 'infinite loops' inside your form, resulting in a freezing form
🐛 Fixed a bug where partial or broken definitions could be saved to the database
❌ Removed the single free premium form (you now always need a paid license to use premium features)
❌ Removed the feedback-for-a-premium-license form (thanks to everyone who filled it in; you made Tripetto better!)

**VERSION 2.2.5 (30-04-2020)**
🐛 Fixed a bug where large data sets could not be saved to the database

**VERSION 2.2.3 (08-10-2019)**
🐛 Fixed a bug in the hidden field block

**VERSION 2.2.1 (10-09-2019)**
🐛 Fixed a bug in the markdown parser
🐛 Fixed a bug where the collector sometimes didn't load on certain pages
🐛 Fixed a bug where Wordfence mistakenly thought a script was malicious

**VERSION 2.1.3 (04-09-2019)**
🐛 Fixed a bug where the plugin would not work with MySQL version 5.5 (or older)

**VERSION 2.1.2 (28-08-2019)**
🐛 Fixed a bug where the wrong plugin directory was used

**VERSION 2.0.4 (27-08-2019)**
⚡ Removed jQuery dependency

**VERSION 2.0.3 (21-08-2019)**
🐛 Fixed issue #77 (https://gitlab.com/tripetto/wordpress/issues/77)
🐛 Fixed a bug in the file upload handling

**VERSION 2.0.1 (09-08-2019)**
✔️ Added a brand new collector with a standard UI based on Bootstrap (for those who want to create a more traditional form)
✔️ Added a toggle to the header bar in the edit screen to switch between the Rolling UI or the new Standard UI
✔️ Added a toggle to the header bar in the edit screen to switch between edit mode and test mode (in edit mode all blocks are displayed in the preview pane, test mode runs the form with all logic enabled so you can test it)
✔️ Added edit buttons in the preview pane to quickly open the properties of a block
✔️ Automatically scroll blocks into view when they are edited
⚡ Improved the header bar in the editor screen to incorporate all the new options (moved the device toggle buttons to a separate dropdown)
⚡ Improved the shortcode so it is not necessary anymore to specify a height for the form (you still can specify a fixed height if you want)
⚡ Installed the latest and greatest version of the Rolling UI collector
🐛 Fixed a bug that caused problems when 2 or more forms were placed on the same page

**VERSION 1.4.2 (31-07-2019)**
🐛 Fixed a bug with the sender's name in email notifications ([#76](https://gitlab.com/tripetto/wordpress/issues/76))

**VERSION 1.4.0 (25-07-2019)**
✔️ Added an option to generate random values with the hidden field block
✔️ Added the option to align the first block of the collector at the top instead of the center of the screen
⚡ Moved some collector options (navigation bar, enumerators, etc.) to the styles section of the settings panel
⚡ Upgraded to a new version of the rolling collector with improved performance and also some bug fixes

**VERSION 1.3.0 (17-07-2019)**
✔️ Added hidden field block (this allows the use of hidden fields in forms and also logic based on the value of those hidden fields)

**VERSION 1.2.1 (16-07-2019)**
✔️ Added the option to include the form data in confirmation mails
✔️ Added the "Send an email"-block to send emails from within forms
🐛 Fixed bug in the editor position when WP menu is collapsed
🐛 Fixed a bug in keyboard navigation in the collector
🐛 Fixed a bug where focus sometimes was set on the wrong field in the settings panel of the editor

**VERSION 1.1.11 (10-07-2019)**
✔️ Added multiple checkboxes block
✔️ Added radiobuttons block
✔️ Added the possibility to hide the title of a block
🐛 Fixed problem with the style of the checkbox block
⚡ Renamed `Next` button to `Ok` button

**VERSION 1.1.8 (09-07-2019)**
✔️ Added single checkbox block
✔️ Added ability to change the labels of the `Next` and `Complete` buttons in the form style panel
✔️ Added uninstall script to remove the data tables when the plugin is deleted
🐛 Automatically make blocks required when the required feature is enabled (this saves an additional click by the user)
🐛 Fixed a bug in the positioning of the empty message of the collector

**VERSION 1.1.5 (02-07-2019)**
✔️ Confirmation dialogs when a form or entry is about to be deleted
🐛 Fixed a bug in the Zapier-integration
⚡ Unlocked all premium functionality for one form without the need of having a paid license

**VERSION 1.0.0 (14-06-2019)**
👶 A new plugin is born, Tripetto is now in the WordPress Plugin Directory!
