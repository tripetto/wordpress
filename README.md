# 📦 Tripetto WordPress Plugin
[![Read the docs](https://badgen.net/badge/icon/docs/cyan?icon=wiki&label)](https://tripetto.com/sdk/docs/applications/wordpress/introduction/)
[![Source code](https://badgen.net/badge/icon/source/black?icon=gitlab&label)](https://gitlab.com/tripetto/wordpress/)
[![Follow us on Twitter](https://badgen.net/badge/icon/@tripetto?icon=twitter&label)](https://twitter.com/tripetto)

The Tripetto WordPress plugin is a complete and powerful plugin to create forms and surveys directly in your WordPress website. The plugin runs a stand-alone, full-blown form builder and stores all data right inside your WordPress environment without any dependencies on outside infrastructure.

## 🧑‍💻 Setting up your local dev environment
We use the [VCCW](http://vccw.cc/) development environment for WordPress. Make sure [Node.js](https://nodejs.org/en/), [VirtualBox](https://www.virtualbox.org/) and [Vagrant](http://www.vagrantup.com/) are installed. Then perform the following steps:
1. On Windows machines add the mapping `192.168.33.10 vccw.test` to the `hosts` file.
2. Run `npm i`;
3. Make sure you have a local SSH key (if not create one, instructions [here](https://docs.gitlab.com/ee/ssh/));
4. If you are on Windows run `npm run setup:windows`, if you use macOS run `setup:macos`.

Whenever a password is requested for the user `vagrant`, enter `vagrant` as password.

## 🐞 Enable WP debug for logging
1. Add the following constant to your testing environment’s wp-config.php file (located at `.vagrant/wordpress`) i.e. before `That is all, stop editing! Happy publishing.`.
When set to true, the log is saved to `wp-content/debug.log`:

```
define( 'WP_DEBUG_LOG', true );
```

## 🧪 Enable Freemius dev mode for [testing](https://freemius.com/help/documentation/wordpress-sdk/testing/) and disabling e-mail activation
1. Add the following constants to your testing environment’s wp-config.php file (located at `.vagrant/wordpress`) i.e. before `That is all, stop editing! Happy publishing.`:

```
define( 'WP_FS__DEV_MODE', true );
define( 'WP_FS__tripetto_SECRET_KEY',  /* Private key here */);
define( 'WP_FS__SKIP_EMAIL_ACTIVATION', true );
```

## 🛠️ Working on this project
1. Make sure to start the WordPress environment: `npm start` (the website http://vccw.test should be up and running now);
2. Start webpack for building the application: `npm test`.
3. Start mailcatcher for catching outgoing e-mail:
    1. Run `npm run mailcatcher`
    2. Open mailcatcher in browser `http://vccw.test:1080/`
4. Log-in to WordPress:

```
- URL: http://vccw.test/wp-admin
- Username: `admin`
- Password: `admin`
```

5. Activate Tripetto plugin (required only once)

    1. While logged in to WordPress navigate to `Plugins` and activate the plugin
    2. You will be redirected to the main plugin page


## 🔨 Changing the PHP version
1. Run `npm run ssh`
2. Run `curl https://raw.githubusercontent.com/vccw-team/change-php-version/master/run.sh | bash -s -- 7.4` (change `7.4` to the desired PHP version)

## 🧰 Optional: Installing phpMyAdmin for database access
1. Download latest package at `https://www.phpmyadmin.net/`
2. Unzip file and rename folder to `phpMyAdmin`
3. Copy folder to `.vagrant/wordpress`

Log-in to PhpMyAdmin:

```
- URL: http://vccw.test/phpMyAdmin
- Username: `wordpress`
- Password: `wordpress`
```

## 🧰 Optional: Enable Multisite
1. Add the following constants to your testing environment’s wp-config.php file (located at `.vagrant/wordpress`) i.e. before `That is all, stop editing! Happy publishing.`:

```
define('WP_ALLOW_MULTISITE', true);
```
2. Login to the admin page and go to `Tools` -> `Network Setup` and follow the instructions.

## 📇 WordPress Plugin Directory
The Tripetto plugin is listed in the WordPress Plugin Directory on https://wordpress.org/plugins/tripetto/

### Subversion
WordPress uses Subversion (SVN) as version control system. After a plugin is reviewed and approved by WordPress, a SVN repository is created and access is granted to the plugin owner.

The SVN repository for this plugin is: https://plugins.svn.wordpress.org/tripetto

WordPress documentation :
1. [Using Subversion with the WordPress Plugin Directory](https://developer.wordpress.org/plugins/wordpress-org/how-to-use-subversion/)
2. [How the readme.txt works](https://developer.wordpress.org/plugins/wordpress-org/how-your-readme-txt-works/)
3. [How plugin assets (header images and icons) work](https://developer.wordpress.org/plugins/wordpress-org/plugin-assets/)

### How to update trunk
Currently `/trunk` contains our latest version.

1. To update the `/trunk` directory you have to install SVN on your computer. If you use Chocolatey, type: `choco install svn`. Try `svn --version` to see if SVN is up and running, otherwise reboot and try again.

2. Navigate to your preferred local repository location, e.g. `c:\repositories\wordpress\tripetto`. Please note this location is different from the plugin's development repository which is on GitLab! If you connect for the first time, type `svn co https://plugins.svn.wordpress.org/tripetto`. If you've connected before, type `svn up`. You are now in sync with the remote repository.

3. Make any of the following changes:
   * Update images in the `/assets` folder
   * Update a new version of the plugin in the `/trunk` folder

4. SVN is different from Git, but still it knows which files are updated. Type `svn status` to check your changes. VS Code has a nice extension where you can view the differences of your changed files. It's called `SVN`. 🙈

5. Always check if there are files not under source control (`svn status` will show them with a question mark `?`). Add them using `svn add <file>`.

6. Commit your changes via `svn ci -m "Type a proper commit message here"`.

### How to create a tag
After you've updated `/trunk` with a new version, it's time to create a tag of that version.

1. Copy the trunk version via `svn cp trunk tags/x.x.x` where x.x.x stands for the new version.

2. Commit your changes via `svn ci -m "Tag x.x.x"`

### Delete old versions
WordPress encourages removing older tags and keeping only the latest version of each major release (see https://developer.wordpress.org/plugins/wordpress-org/how-to-use-subversion/#delete-old-versions). To do so, run the following command:

```bash
svn delete https://plugins.svn.wordpress.org/tripetto/tags/<tag> -m "Delete old version"
```

After that run `svn update` to pull the changes into the local repository.

## ✨ Contributors
- [Hisam A Fahri](https://gitlab.com/hisamafahri) (Indonesian translation)
- [Krzysztof Kamiński](https://gitlab.com/kriskaminski) (Polish translation)

## 👋 About us
If you want to learn more about Tripetto or contribute in any way, visit us at [tripetto.com](https://tripetto.com/).
